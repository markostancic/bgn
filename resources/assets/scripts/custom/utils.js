/* eslint-disable */
/**
 * Returns random number from provided min and max values
 * @param min {number}
 * @param max {number}
 * @returns {number}
 */
function random_numbers( min, max ) {
	min = Math.ceil( min );
	max = Math.floor( max );
	return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
}

/**
 * Showing element with css class or jquery methods fadeIn and slideIn
 *
 * @param element {HTMLElement} HTML Element from Document
 * @param elClass {string}      Class to control, default is show
 */
function showElement( element, elClass = 'show' ) {
	element.setAttribute( 'aria-expanded', 'true' );
// Get element to open from btn's data-open attribute
	const modal             = document.querySelector( element.dataset.open ),
	      type_of_animation = element.dataset.hasOwnProperty( 'function' ) ? element.dataset.function : 'fade';

	if ( modal == undefined ) {
		return;
	}

	// If element to open has class modal, add class overflow to body
	// Overflow class is used in css to apply overflow hidden on body
	// to prevent scrolling while modal is openned
	if ( modal.classList.contains( 'modal' ) || modal.classList.contains( 'navbar-collapse' ) ) {
		document.body.classList.add( 'overflow' );
	}
	switch ( type_of_animation ) {
		case "fade":
			$( modal ).fadeIn();
			break;
		case "slide":
			$( modal ).slideDown();
			break;
		case "class":
			modal.classList.add( elClass );
			break;
	}
}

/**
 * Hiding element with css class or jquery methods fadeIn and slideIn
 *
 * @param element {HTMLElement} HTML Element from Document
 * @param elClass {string}      Class to control, default is show
 */
function hideElement( element, elClass = 'show' ) {
	element.setAttribute( 'aria-expanded', 'false' );
	// Get element to open from btn's data-open attribute
	const modal             = document.querySelector( element.dataset.close ),
	      type_of_animation = element.dataset.hasOwnProperty( 'function' ) ? element.dataset.function : 'fade';

	if ( modal == undefined ) {
		return;
	}

	// If element to open has class modal, add class overflow to body
	// Overflow class is used in css to apply overflow hidden on body
	// to prevent scrolling while modal is openned
	if ( modal.classList.contains( 'modal' ) || modal.classList.contains( 'navbar-collapse' ) ) {
		document.body.classList.remove( 'overflow' );
	}
	switch ( type_of_animation ) {
		case "fade":
			$( modal ).fadeOut();
			break;
		case "slide":
			$( modal ).slideUp();
			break;
		case "class":
			modal.classList.remove( elClass );
			break;
	}
}

/**
 * Toggles element with css class or jquery methods fadeIn and slideIn
 *
 * @param element {HTMLElement} HTML Element from Document
 * @param elClass {string}      Class to control, default is show
 */
function toggleElement( element, elClass = 'show' ) {
	element.setAttribute( 'aria-expanded', element.getAttribute( 'aria-expanded' ) === 'true' ? 'false' : 'true' );
	// Get element to open from btn's data-open attribute
	const modal             = document.querySelector( element.dataset.togle ),
	      type_of_animation = element.dataset.hasOwnProperty( 'function' ) ? element.dataset.function : 'fade';

	if ( modal == undefined ) {
		return;
	}

	// If element to open has class modal, add class overflow to body
	// Overflow class is used in css to apply overflow hidden on body
	// to prevent scrolling while modal is openned
	if ( modal.classList.contains( 'modal' ) || modal.classList.contains( 'navbar-collapse' ) ) {
		document.body.classList.toggle( 'overflow' );
	}
	switch ( type_of_animation ) {
		case "fade":
			$( modal ).fadeToggle();
			break;
		case "slide":
			$( modal ).slideToggle();
			break;
		case "class":
			modal.classList.toggle( elClass );
			break;
	}
}

/**
 * Scroll element up or down based on attributes
 *
 * @param item_to_scroll
 * @param direction
 */
function scroll_item( item_to_scroll, direction ) {
	let amount_to_scroll = item_to_scroll.scrollTop;

	switch ( direction ) {
		case 'up':
			amount_to_scroll += 100;
			break;
		case 'down':
			amount_to_scroll -= 100;
			break;
		default:
			amount_to_scroll += 100;
	}
	$( item_to_scroll ).animate(
		{
			scrollTop: amount_to_scroll
		}, 500 )
}

export {
	random_numbers,
	showElement,
	hideElement,
	toggleElement,
	scroll_item
}