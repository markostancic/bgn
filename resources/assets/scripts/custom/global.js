/* eslint-disable */
import "./header";
import "./form";
import { defer_images, deffer_carousel, defer_background }      from './img-defer';
import { showElement, hideElement, toggleElement, scroll_item } from './utils';
import Carousel                                                 from 'bootstrap/js/src/carousel';

window.addEventListener( 'load', () => {
	defer_images();
	deffer_carousel();
	defer_background();


	// Init all Bootstrap carousels
	document.querySelectorAll( '.carousel' ).forEach( carousel => {
		new Carousel( carousel );
	} );

	// Basic variables and functions
	let screen_width = screen.width;

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '[data-open]' ) ) return;
		showElement( e.target.closest( '[data-open]' ) );
	} );

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '[data-close]' ) ) return;
		hideElement( e.target.closest( '[data-close]' ) );
	} );

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '[data-togle]' ) ) return;
		toggleElement( e.target.closest( '[data-togle]' ) );
	} );

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '[data-scroll]' ) ) return;
		const closest_element = e.target.closest( '[data-scroll]' );
		scroll_item( document.querySelector( closest_element.dataset.target ), closest_element.dataset.scroll );
	} );

	const nightout_container = document.querySelector( '#nightout_content' );
	if ( nightout_container && window.matchMedia( '(hover: hover)' ).matches ) {
		nightout_container.addEventListener( 'wheel', e => {
			if ( !e.target.closest( '#nightout_content' ) ) return;
			e.preventDefault();
			if ( e.deltaY > 0 ) {
				nightout_container.scrollTop += 100;
			} else {
				nightout_container.scrollTop -= 100;
			}
		} );
	}

	// Cta modal button position control
	const sidebar = document.querySelector( '.homepage_sidebar' )
	if ( window.matchMedia( '(min-width: 992px)' ).matches ) {
		if ( sidebar ) {
			const cta_modal    = document.querySelector( '.cta-modal' ),
			      cta_position = sidebar.getBoundingClientRect();

			cta_modal.style.left = cta_position.right + 'px';
			cta_modal.classList.add( 'show' )

			window.addEventListener( 'resize', () => {
				cta_modal.style.left = sidebar.getBoundingClientRect().right + 'px';
			} );
		}


		// Control classes on sidebar elements when hovering
		document.addEventListener( 'mouseover', e => {
			if ( !e.target.closest( '.sidebar_card' ) ) return;
			const closest_element  = e.target.closest( '.sidebar_card' ),
			      previous_element = closest_element.previousElementSibling,
			      next_element     = closest_element.nextElementSibling;

			closest_element.classList.add( 'active' );

			if ( previous_element ) previous_element.classList.add( 'active-sibling-prev' );

			if ( next_element ) next_element.classList.add( 'active-sibling-next' );
		} );
		document.addEventListener( 'mouseout', e => {
			if ( !e.target.closest( '.sidebar_card' ) ) return;

			document.querySelector( '.active' ).classList.remove( 'active' );
			document.querySelectorAll( '.active-sibling-prev, .active-sibling-next' ).forEach( item => item.classList.remove( 'active-sibling-prev', 'active-sibling-next' ) );
		} );
	}
} );
