/* eslint-disable */
let btn = $('.button_date-click-temp');
btn.on('click', function (e) {
    let $this = $(this);
    let $date = $this.data('date');
    $.ajax({
        beforeSend: function (request) {
            $('body').find('.date_picker_filter_content').empty();
            $('.date_picker_filter_content').css('display','flex').addClass('show');
        },
        dataType: 'json',
        url: '/wp-json/lokali/zbirna?date='+$date,
        type: 'GET',
        success: function (data) {
            if (data.length) {
                $.each(data, function (i, item) {
                    var html = '<div class="lokali_card">';
                    html += '<div class="lokali_card_image">';
                    html += '<a href="'+item.link+'">';
                    html += '<img src="' + item.featured_image + '" class="img-fluid">';
                    html += '</a></div>';
                    html += '<div class="lokali_card_text">';
                    html += '<a href="'+item.link+'">';
                    html += '<h2 class="lokali_card_heading">'+item.title+'</h2>';
                    html += '<p class="info">'+item.datum+'</p>';
                    html += '</a>';
                    html += '<a href="'+item.link+'" class="button yellow">Rezerviši online</a>';
                    html += ' <a href="'+item.telefon+'" class="button blue">';
                    html += '<img src="'+item.img_phone+'" class="img-fluid">'+item.telefon_view+'';
                    html += '</a>';
                    html += '</div></div>';
                    $('body').find('.date_picker_filter_content').append(html);
                });
            } else {
              var $html = '<p>Nema rezultata za vašu pretragu.</p>';
                $('body').find('.date_picker_filter_content').html($html);
            }
        },
    });
}); // end load
