/* eslint-disable */
window.addEventListener('load', () => {
  // Control mega menu and dropdown's
  const header = document.querySelector('#header'),
    mega_menu = document.querySelector('.mega-menu');


    document.addEventListener('mouseover', e => {
      if (!e.target.closest('.menu-item-has-children') && !e.target.closest('.mega-menu-dropdown')) return;

      const closest_element = e.target.closest('.menu-item-has-children') || e.target.closest('.mega-menu-dropdown');
      header.classList.add('open');
      if (closest_element.classList.contains('mega-menu-dropdown')) {
        const menu_link = closest_element.querySelector('a');
        document.querySelector(menu_link.hash).classList.add('show');
      }
    });

    document.addEventListener('mouseout', e => {
      if (!e.target.closest('.menu-item-has-children') && !e.target.closest('.mega-menu-dropdown') && !e.target.closest('.mega-menu')) return;

      if (e.target.closest('.menu-item-has-children')) {
        header.classList.remove('open');
        mega_menu.classList.remove('show');
      } else {
        if (document.querySelector('.mega-menu:hover')) {
          header.classList.add('open');
          mega_menu.classList.add('show');
        } else {
          mega_menu.classList.remove('show');
          header.classList.remove('open');
        }
      }
    });

  // Make copy of secondary header and append to megamenu
  const secondary_header = header.querySelector('.secondary-header').cloneNode(true);
  secondary_header.classList.add('cloned');
  mega_menu.append(secondary_header);

  const menu_item_parent = header.querySelectorAll('#header .menu-item-has-children, #header .mega-menu-dropdown');
  if (window.matchMedia('(max-width: 991px)').matches) {
    document.addEventListener('click', e => {
      if (!e.target.closest('#main-navigation .menu-item-has-children') && !e.target.closest('.mega-menu-dropdown') && !e.target.closest('.navbar-toggler')) return;
      const closest_element = e.target;
      menu_item_parent.forEach(item => {
        item.classList === closest_element.classList ? closest_element.classList.toggle('active') : item.classList.remove('active');
      });

    });
  }


// Making secondary header fixed after scrolling
  const main_header = header.querySelector('.main-header');

  function makeHeaderSticky() {
    const scroll_from_top = window.scrollY,
      navbar_collapse = document.querySelector('.navbar-collapse.show');

    if (!navbar_collapse) {
      if (scroll_from_top >= main_header.offsetHeight) {
        header.classList.add('scrolled');
      } else if (scroll_from_top < main_header.offsetHeight) {
        header.classList.remove('scrolled');
      }
    }

    requestAnimationFrame(makeHeaderSticky);
  }

  makeHeaderSticky();
});
