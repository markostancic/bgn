/* eslint-disable */
let label = $('.custom_checkbox_container .custom_checkbox_temp');
label.on('click', function (e) {
    let $this = $(this);
    let term_id = $this.attr('id');
    $.ajax({
        beforeSend: function (request) {
            $('body').find('.date_picker_filter_content').empty();
            $('.loader').css('display', 'flex');
            $('.date_picker_filter_content').css('display','flex').addClass('show');
          $('.clubbing_temp').hide();
          $('.date_temp').hide();
        },
        dataType: 'json',
        url: '/wp-json/lokali/svi?term_id=' + term_id,
        type: 'GET',
        success: function (data) {
            if (data.length) {
                $.each(data, function (i, item) {
                    var html = '<div class="lokali_card">';
                    html += '<div class="lokali_card_image">';
                    html += '<a href="'+item.link+'">';
                    html += '<img src="' + item.featured_image + '" class="img-fluid">';
                    html += '</a></div>';
                    html += '<div class="lokali_card_text">';
                    html += '<a href="'+item.link+'">';
                    html += '<h2 class="lokali_card_heading">'+item.title+'</h2>';
                    html += '<p class="info">'+item.title+'</p>';
                    html += '</a>';
                    html += '<a href="'+item.link+'" class="button yellow">Rezerviši online</a>';
                    html += ' <a href="'+item.telefon+'" class="button blue">';
                    html += '<img src="" class="img-fluid">'+item.telefon_view+'';
                    html += '</a>';
                    html += '</div></div>';
                    $('body').find('.date_picker_filter_content').append(html);
                    $('.loader').fadeOut();
                });
            } else {
                $('.loader').hide();
                $('#load-more-bugs').hide();
            }

            $('#load-more-bugs').removeClass('loading');
        },
    });
}); // end load
