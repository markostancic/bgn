/* eslint-disable */
// import { random_numbers } from './utils';
function random_numbers( min, max ) {
	min = Math.ceil( min );
	max = Math.floor( max );
	return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
}

export function datePickerControl(filter_container) {
	// const filter_container = document.querySelector( '.date_picker_filter' );
	// if ( !filter_container ) return;

	const static_results = filter_container.querySelectorAll( '.date_picker_filter_content' )

	function hideDatePickerResults() {
		document.querySelector( '.date_picker_filter_content.show' ).classList.remove( 'show' );
		let random_num = random_numbers( 0, static_results.length - 1 );
		static_results[ random_num ].classList.add( 'show' );
	}

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '.button_date-click ' ) ) return;
		const closestElement = e.target.closest( '.button_date-click' ),
		      active_day_btn = filter_container.querySelector( '.button_date-click.active' );

		if ( !closestElement.classList.contains( 'active' ) ) {
			closestElement.classList.add( 'active' );
			active_day_btn.classList.remove( 'active' );
		}
	} );

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '.button_date-click ' ) ) return;
		hideDatePickerResults();
	} );

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '.date_picker_type_lokal_radio label' ) ) return;
		hideDatePickerResults();
	} );

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '.custom_checkbox_container label' ) ) return;
		const date_picker_check = filter_container.querySelector( '.date_picker_boxes' );
		date_picker_check.classList.add( 'active' );
		setTimeout( () => {
			date_picker_check.classList.remove( 'active' );
			hideDatePickerResults();
			filter_container.querySelector( '.date_picker_advanced_search' ).classList.remove( 'show' );
		}, 250 );
	} );
}
