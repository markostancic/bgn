/* eslint-disable */
let pull_page = 1;
$('#load-more-bugs').on('click', function (e) {
  let jsonFlag = true;
  pull_page++;
  console.log(pull_page);
  $.ajax({
    beforeSend: function (request) {
      $('#load-more-bugs').addClass('loading');
    },
    dataType: 'json',
    url: '/wp-json/kategorije/postovi?page=' + pull_page,
    type: 'GET',
    success: function (data) {
      if (data.length) {
        $.each(data, function (i, item) {
          var html = '<div class="col-md-12 feedback-card">';
          html += '<div class="row">';
          html += '<div class="col-md-9 content-card">';
          if (item.message) {
            html += '<p class="message">' + item.message + '</p>';
          }
          html += '<div class="info"><div class="dropdown-divider"></div>';
          html += '<span class="name">';
          html += item.title;
          html += '</span>';
          if (item.country) {
            html += '<span class="country">' + item.country + '</span>';
          }
          html += '</div></div></div></div>';
          $('body').find('.content-list-bugs').append(html);
        });
      } else {
        $('#load-more-bugs').hide();
      }

      $('#load-more-bugs').removeClass('loading');
    },
  });
}); // end load
