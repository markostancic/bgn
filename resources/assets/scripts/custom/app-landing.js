/* eslint-disable */
window.addEventListener('load', () => {
  // app landing page gradient
  const gradient = document.querySelector('.gradient');
  if (gradient) {
    const gradient_client_rect = gradient.getBoundingClientRect(),
      gradients_height = gradient_client_rect.height,
      gradients_position_top = gradient_client_rect.top,
      viewport_height = window.innerHeight;
    let colors = [],
      is_element_visible = false;

    function changeBackground() {
      let gr_client_rect = gradient.getBoundingClientRect(),
        gr_top = -gr_client_rect.top,
        adjusted_scroll_top = gr_top + (viewport_height / 1.8),
        scrolled = (adjusted_scroll_top / gradients_height) * 100;
      if (scrolled > 0 && scrolled <= 100) {
        if (scrolled > 65) {
          colors[0] = 255 - (scrolled * 0.5) / 2.5;
          colors[1] = 170 - (scrolled * 3) / 2.5;
          colors[2] = 80 + (scrolled * 1.475) / 2.5;
        } else if (scrolled >= 35 && scrolled <= 65) {
          colors[0] = 255 - (scrolled * 0.4);
          colors[1] = 208 - (scrolled * 1.1) / 1.4;
          colors[2] = 80 + (scrolled * 1.1) / 2.5;
        } else if (scrolled <= 0) {
          colors[0] = 0;
          colors[1] = 173;
          colors[2] = 238;
        } else {
          colors[0] = scrolled * 7.286;
          colors[1] = 173 + scrolled;
          colors[2] = 238 - (scrolled * 4.514);
        }
        gradient.style.backgroundColor = `rgb(${colors})`;
      }
      requestAnimationFrame(changeBackground);
    }

    changeBackground();

    const options = {
        rootMargin: '-300px 0px 0px 100px',
      },
      observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
          is_element_visible = entry.isIntersecting;
        })
      }, options);

    // observer.observe( gradient );
  }
});
