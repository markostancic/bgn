/* eslint-disable */
/* export class Form {
	constructor( formElement, disableAutoComplete = true ) {
		this.formElement         = formElement;
		this.disableAutoComplete = disableAutoComplete;
		this.fields_invalid      = 0;
	}

	init() {
		if ( this.disableAutoComplete ) {
			this.formElement.setAttribute( 'autocomplete', 'off' );
		}
	}

	emailValidation( mail_input ) {
		const is_valid = mail_input.value.match( /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,5})+$/ );

		if ( !is_valid ) {
			const input_parent = mail_input.closest( '.form-group' ) || mail_input;
			input_parent.classList.add( 'invalid' );
		}
	}

	telephoneValidation( telephone_input ) {
		const is_valid = telephone_input.value.match( /^[+]*[(]?[0-9]{1,4}[)]?[-\s./0-9]*$/ );

		if ( !is_valid ) {
			const input_parent = telephone_input.closest( '.form-group' ) || telephone_input;
			input_parent.classList.add( 'invalid' );
		}
	}

	lengthValidation( input_element, min_length ) {
		const is_valid = input_element.value.length <= min_length;

		if ( !is_valid ) {
			const input_parent = input_element.closest( '.form-group' ) || input_element;
			input_parent.classList.add( 'invalid' );
		}
	}

	requiredFieldsValidation() {
		const form_elements = this.formElement.elements;

		for ( let i = 0; i < form_elements.length; i++ ) {
			form_elements[ i ].closest( '.form-group' ).classList.remove( 'invalid' );

			if ( ( form_elements[ i ].dataset.required == 'true' || form_elements[ i ].ariaRequired == 'true' || form_elements[ i ].required == 'true' ) && form_elements[ i ].value === '' ) {
				form_elements[ i ].parentElement.classList.add( 'invalid' );
				this.fields_invalid++;
			}
		}
	}

	submitControl() {
		if ( !this.fields_invalid ) {
			this.formElement.onsubmit = e => e.preventDefault();
		}
	}
} */

window.addEventListener( 'load', () => {
	// Form validation
	let all_inputs    = $( '.form .form-input' );
	let name_input    = $( '#fullname' );
	let email_input   = $( '#email' );
	let subject_input = $( '#subject' );
	let submit_btn    = $( '.form .submit' );
	let i             = 0;

	function check_validity( element ) {
		let i = 0;
		for ( ; i < element.length; i++ ) {
			if ( $( element[ i ].checkValidity() ) ) {
				$( element[ i ] ).addClass( 'error' );
				$( element ).removeClass( 'valid' );
			} else {
				$( element ).removeClass( 'error' );
				$( element ).addClass( 'valid' );
			}
		}
	}

	function check_input_length( element, input_length ) {
		if ( $( element ).val().length < input_length ) {
			$( element ).addClass( 'error' );
			$( element ).removeClass( 'valid' );
		} else {
			$( element ).removeClass( 'error' );
			$( element ).addClass( 'valid' );
		}
	}

	function check_email( mail ) {
		if ( !/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,5})+$/.test( $( mail ).val() ) ) {
			$( mail ).addClass( 'error' );
			$( mail ).removeClass( 'valid' );
		} else {
			$( mail ).removeClass( 'error' );
			$( mail ).addClass( 'valid' );
		}
	}

	$( name_input ).keyup( function () {
		check_input_length( this, 3 );
	} );
	$( subject_input ).keyup( function () {
		check_input_length( this, 4 );
	} );
	$( email_input ).keyup( function () {
		check_email( this );
	} );

	$( submit_btn ).click( function () {
		check_validity( all_inputs );
	} );

	// Set inputs required attribute for html 5 validation
	for ( ; i < all_inputs.length; i++ ) {
		all_inputs[ i ].setAttribute( 'required', true );
	}
} );