window.addEventListener( 'load', () => {
	// Add required attribute for lightbox if there is any
	let wp_gallery = $( '.wp-block-gallery' );
	if ( wp_gallery.length > 0 ) {
		let wp_gallery_links = wp_gallery.map( function ( index ) {
			return $( wp_gallery[ index ] ).find( 'a' );
		} );
		wp_gallery_links.each( ( index ) => {
			$( wp_gallery_links[ index ] ).attr( 'data-lightbox', `roadtrip${ index }` );
		} );
	}
} );