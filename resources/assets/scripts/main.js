// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import './custom/global';
import './custom/lightbox';
import './custom/custom-lightbox';
import './custom/load_more';
import './custom/search_lokali';
import './custom/search_lokali-category';
import './custom/search_lokali_grupna';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
    common,
  // Home page
    home,
  // About Us page, note the change from about-us to aboutUs.
    aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());



