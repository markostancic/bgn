<?php

use function App\asset_path;
use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 *
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__ . '/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__) . '/config/assets.php',
            'theme' => require dirname(__DIR__) . '/config/theme.php',
            'view' => require dirname(__DIR__) . '/config/view.php',
        ]);
    }, true);

/**
 * Register Menu locations
 */
add_action('after_setup_theme', 'register_custom_nav_menus');
function register_custom_nav_menus()
{
    register_nav_menus([
        'primary_navigation' => 'Primary Navigation',
        'menu_1' => 'First menu footer',
        'menu_2' => 'Second menu footer',
        'menu_3' => 'Third menu footer',
        'menu_4' => 'Fourth menu footer',
        'menu_5' => 'Fifth menu footer',
        'menu_6' => 'Servis kolona 1 menu 1',
        'menu_7' => 'Servis kolona 1 menu 2',
        'menu_8' => 'Servis kolona 1 menu 3',
        'menu_9' => 'Servis kolona 2 menu 1',
        'menu_10' => 'Servis kolona 3 menu 1',
        'menu_11' => 'Servis kolona 4 menu 1',
        'menu_12' => 'Servis kolona 4 menu 2',
    ]);
}

/**
 * Register sidebar and footer widget
 */
function register_custom_widgets()
{
    register_sidebar([
        'name' => 'Footer widget',
        'id' => 'footer_widget',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<p class="custom-heading-2">',
        'after_title' => '</h2>',
    ]);
    register_sidebar(
        [
            'name' => __('Sidebar', 'beogradnocu'),
            'id' => 'custom-sidebar',
            'description' => __('Sidebar', 'beogradnocu'),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ]
    );
}

add_action('widgets_init', 'register_custom_widgets');

/**
 * Defining ACF Options Page
 */
if (function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'page_title' => __('Podešavanje teme', 'beogradnocu'),
        'menu_title' => __('Beogradnoću', 'beogradnocu'),
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => get_template_directory_uri() . '/icon.png',
    ]);

    acf_add_options_sub_page([
        'page_title' => __('Podešavanje Header-a', 'beogradnocu'),
        'menu_title' => __('Header', 'beogradnocu'),
        'parent_slug' => 'theme-general-settings',
    ]);

    acf_add_options_sub_page([
        'page_title' => __('Podešavanje Footer-a', 'beogradnocu'),
        'menu_title' => __('Footer', 'beogradnocu'),
        'parent_slug' => 'theme-general-settings',
    ]);
}

/**
 * Modifier for default wp search form
 *
 * @param array $args
 *
 * @return false|mixed|string|void
 */
function get_custom_search_form($args = [])
{
    do_action('pre_get_search_form');

    $echo = true;

    if (!is_array($args)) {
        $echo = (bool) $args;

        // Set an empty array and allow default arguments to take over.
        $args = [];
    }

    // Defaults are to echo and to output no custom label on the form.
    $defaults = [
        'echo' => $echo,
        'aria_label' => '',
    ];

    $args = wp_parse_args($args, $defaults);

    /**
     * Filters the array of arguments used when generating the search form.
     */
    $args = apply_filters('search_form_args', $args);

    $format = current_theme_supports('html5', 'search-form') ? 'html5' : 'xhtml';

    /**
     * Filters the HTML format of the search form.
     */
    $format = apply_filters('search_form_format', $format);

    $search_form_template = locate_template('searchform.php');
    if ('' != $search_form_template) {
        ob_start();
        require $search_form_template;
        $form = ob_get_clean();
    } else {
        // Build a string containing an aria-label to use for the search form.
        if (isset($args['aria_label']) && $args['aria_label']) {
            $aria_label = 'aria-label="' . esc_attr($args['aria_label']) . '" ';
        } else {
            /*
             * If there's no custom aria-label, we can set a default here. At the
             * moment it's empty as there's uncertainty about what the default should be.
             */
            $aria_label = '';
        }
        if ('html5' == $format) {
            $form = '<div class="search-form"><form role="search" ' . $aria_label . 'method="get" class="search-form" action="' . esc_url(home_url('/')) . '">
				<label>
					<span class="screen-reader-text">' . _x('Search for:', 'label') . '</span>
					<input type="search" class="search-field" placeholder="' . esc_attr_x('Search &hellip;', 'placeholder') . '" value="' . get_search_query() . '" name="s" />
				</label>
				<input type="submit" class="search-submit" value="' . esc_attr_x('Search', 'submit button') . '" />
			</form></div>';
        } else {
            $form = '<div class="search-form"><form role="search" ' . $aria_label . 'method="get" id="searchform" class="searchform" action="' . esc_url(home_url('/')) . '">
				<div>
					<label class="screen-reader-text" for="s">' . _x('Search for:', 'label') . '</label>
					<input type="text" value="' . get_search_query() . '" name="s" id="s" />
					<input type="submit" id="searchsubmit" value="' . esc_attr_x('Search', 'submit button') . '" />
				</div>
			</form></div>';
        }
    }

    /**
     * Filters the HTML output of the search form.
     */
    $result = apply_filters('get_search_form', $form);

    if (null === $result) {
        $result = $form;
    }

    if (isset($args['echo']) && $args['echo']) {
        echo $result;
    } else {
        return $result;
    }
}

/*
 * Custom shortcodes
 */

function baner_cards()
{
    $content = '';
    if (have_rows('shortcode_cards')):
        while (have_rows('shortcode_cards')):
            the_row();
            $image = get_sub_field('ikonica');
            $text = get_sub_field('tekst');
            $btn = get_sub_field('dugme');
            $btn_link = $btn['url'];
            $btn_title = $btn['title'];
            $content .= <<<Content
            <div class="about shortcode_banner_section">
            <div class="main-content">
            <div class="custom-card custom-flex-row">
                <img src="" data-srcset="$image" alt="">
	            <div class="text">
		            <p>$text</p>
		            <a href="$btn_link" class="button yellow">$btn_title</a>
	            </div>
            </div>
            </div>
              </div>
Content;
        endwhile;
    endif;

    return $content;
}

add_shortcode('baner_cards_shortcode', 'baner_cards');

function enterier_gallery()
{
    $_content = '';
    $title = __('Enterijer i galerija', 'beogradnocu');
    if (have_rows('galerija_fotografija__enterijer')):
        $_content .= '<div class="enterier-gallery">';
        $_content .= '<h2>' . $title . '</h2>';
        while (have_rows('galerija_fotografija__enterijer')):
            the_row();
            $image = get_sub_field('slika')['sizes']['galerija-group'];
            $_content .= '<div class="gallery-item">';
            $_content .= '<img src="' . $image . '" alt="">';
            $_content .= '</div>';
        endwhile;
        $_content .= '</div>';
    endif;
    $container = <<<Content
        $_content
Content;

    return $container;
}

add_shortcode('enterier_gallery', 'enterier_gallery');

add_action('rest_api_init', function () {
    register_rest_route('kategorije', '/postovi', [
        'methods' => 'GET',
        'callback' => 'postovi',
    ]);
     register_rest_route('kalendar', '/galerije', [
        'methods' => 'GET',
        'callback' => 'galerije',
    ]);
    register_rest_route('lokali', 'svi', [
        'methods' => 'GET',
        'callback' => 'lokali',
    ]);
    register_rest_route('lokali', '/zbirna', [
        'methods' => 'GET',
        'callback' => 'lokali_zbirna',
    ]);
});

function postovi($request)
{
    $posts_data = [];
    $paged = $request->get_param('page');
    $paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
    $posts = new WP_Query([
        'post_type' => 'post',
        'status' => 'published',
        'posts_per_page' => 5,
        'paged' => $paged,
    ]);

    if ($posts->have_posts()) {
        while ($posts->have_posts()) {
            $posts->the_post();

            $posts_data[] = [
                'id' => get_the_ID(),
                'title' => get_the_title(),
                'link' => get_permalink(),
            ];
        }
    }
    wp_reset_postdata();
    return $posts_data;
}

function lokali($request)
{
    $posts_data = [];
    $cat = $request->get_param('term_id');
    $post_types = ['kafane_beograd', 'klubovi_beograd', 'splavovi_beograd', 'restorani_beograd', 'barovi_beograd'];

    $img_phone = asset_path('images/beograd_nocu__general_header_rezervacije.svg');
    $posts = new WP_Query([
        'post_type' => $post_types,
        'status' => 'published',
        'posts_per_page' => -1,
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy' => 'deo_grada',
                'field' => 'id',
                'terms' => $cat,
            ],
        ],
    ]);

    if ($posts->have_posts()) {
        while ($posts->have_posts()) {
            $posts->the_post();
            $featured_img_url = get_the_post_thumbnail_url($posts->ID, 'lokali');
            $telefon = get_field('telefon_kod', 'option');
            $telefon_view = get_field('telefon_prikaz', 'option');

            $posts_data[] = [
                'id' => get_the_ID(),
                'title' => get_the_title(),
                'link' => get_permalink(),
                'img_phone' => $img_phone,
                'featured_image' => $featured_img_url,
                'telefon' => $telefon,
                'telefon_view' => $telefon_view,
            ];
        }
    }
    wp_reset_postdata();
    return $posts_data;
}

function lokali_zbirna($request)
{
    $post_types = ['kafane_beograd', 'klubovi_beograd', 'splavovi_beograd', 'restorani_beograd', 'barovi_beograd'];
    $posts_data = [];
    $date = $request->get_param('date');
    $img_phone = asset_path('images/beograd_nocu__general_header_rezervacije.svg');
    $posts = new WP_Query([
        'post_type' => $post_types,
        'status' => 'published',
        'posts_per_page' => -1,
        'meta_query' => [
            [
                'key' => 'program_%_datum',
                'value' => $date,
                'compare' => '=',
                'type' => 'numeric',
            ],
        ],
    ]);

    if ($posts->have_posts()) {
        while ($posts->have_posts()) {
            $posts->the_post();
            $featured_img_url = get_the_post_thumbnail_url($posts->ID, 'lokali');
            $telefon = get_field('telefon_kod', 'option');
            $telefon_view = get_field('telefon_prikaz', 'option');
            $datum_repeat = [];
            $arr = [];
            if (have_rows('program', $posts->ID)):
                while (have_rows('program', $posts->ID)):
                    the_row();
                    $datum = strtotime(get_sub_field('datum'));
                    $cur_date = strtotime($date);
                    if ($cur_date === $datum) {
                        $tekst_programa[] = get_sub_field('tekst_programa');
                    }

                endwhile;
            endif;

            $posts_data[] = [
                'id' => get_the_ID(),
                'title' => get_the_title(),
                'link' => get_permalink(),
                'img_phone' => $img_phone,
                'featured_image' => $featured_img_url,
                'telefon' => $telefon,
                'telefon_view' => $telefon_view,
                'datum' => $tekst_programa,

            ];
        }
    }
    wp_reset_postdata();
    return $posts_data;
}

function my_posts_where($where)
{
    $where = str_replace("meta_key = 'program_", "meta_key LIKE 'program_", $where);
    return $where;
}

add_filter('posts_where', 'my_posts_where');

/**
 * Disabling Gutenberg
 */
add_filter('use_block_editor_for_post', '__return_false');

/**
 * Get all terms for current post by ID from parameter set input
 */
function getSpecificTaxTerms($tax)
{
    global $post;
    $args = ['hide_empty=0'];
    $term_list = '';
    $terms = wp_get_post_terms($post->ID, $tax);
    if (!empty($terms) && !is_wp_error($terms)) {
        $count = count($terms);
        $i = 0;
        foreach ($terms as $term) {
            $i++;
            $term_list .= '<a href="' . esc_url(get_term_link($term->term_id)) . '" class="tag" >' . $term->name . '</a>';
        }
        echo $term_list;
    }
}

/**
 * Quick edit && Bulk edit
 */
/**
 * Defining variable for post type where columns are gonna be added ( slug or name of CPT ).
 */
$post_type_lokali = 'lokali';
$post_type_desavanja = 'desavanja';
$post_type_event = 'event';
$post_type_novagodina = 'nova_godina';

/**
 * New column
 */
add_filter("manage_{$post_type_lokali}_posts_columns", 'add_prioritet_column');
add_filter("manage_{$post_type_desavanja}_posts_columns", 'add_prioritet_column');
add_filter("manage_{$post_type_event}_posts_columns", 'add_prioritet_column');
add_filter("manage_{$post_type_novagodina}_posts_columns", 'add_prioritet_column');

/**
 * manage_{POST TYPE NAME}_posts_columns
 */
function add_prioritet_column($column_array)
{
    // I want to add my column at the beginning, so I use array_slice()
    // in other cases $column_array['prioritet'] = 'prioritet' will be enough
    $column_array = array_slice($column_array, 0, 2, true)
     + ['prioritet' => 'Prioritet']// our new column for prioritet
     + array_slice($column_array, 1, null, true);

    return $column_array;
}

/**
 * Populate our new columns with data
 */
add_action("manage_{$post_type_lokali}_posts_custom_column", 'populate_column', 10, 2);
add_action("manage_{$post_type_desavanja}_posts_custom_column", 'populate_column', 10, 2);
add_action("manage_{$post_type_event}_posts_custom_column", 'populate_column', 10, 2);
add_action("manage_{$post_type_novagodina}_posts_custom_column", 'populate_column', 10, 2);
function populate_column($column_name, $id)
{
    // if you have to populate more that one columns, use switch()
    if ('prioritet' == $column_name) {
        $prioritet = get_post_meta($id, 'prioritet', true);

        if ($prioritet) {
            echo esc_html($prioritet);
        } else {
            esc_html_e('Nije uneto.', 'beogradnocu');
        }
    }
}

/**
 * quick_edit_custom_box allows to add HTML in Quick Edit
 * Please note: it files for EACH column, so it is similar to manage_posts_custom_column
 */
add_action('quick_edit_custom_box', 'column_quick_edit_fields', 10, 2);

function column_quick_edit_fields($column_name, $post_type)
{

    // you can check post type as well but is seems not required because your columns are added for specific CPT anyway
    if ('prioritet' != $column_name) {
        return;
    }

    // you can also print Nonce here, do not do it ouside the switch() because it will be printed many times
    wp_nonce_field('prioritet_q_edit_nonce', 'prioritet_nonce');
    // please note: the <fieldset> classes could be:
    // inline-edit-col-left, inline-edit-col-center, inline-edit-col-right
    // each class for each column, all columns are float:left,
    // so, if you want a left column, use clear:both element before
    // the best way to use classes here is to look in browser "inspect element" at the other fields

    // for the FIRST column only, it opens <fieldset> element, all our fields will be there
    ?>

    <fieldset class="inline-edit-col-right">
        <div class="inline-ed¬it-col">
            <div class="inline-edit-group wp-clearfix">
                <label class="alignleft">
                    <span class="title">Prioritet</span>
                    <span class="input-text-wrap"><input type="text" name="prioritet" value=""></span>
                </label>

    <?php
}

/**
 * Quick Edit Save
 */
add_action('save_post', 'column_quick_edit_save');

function column_quick_edit_save($post_id)
{

    // check user capabilities
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    // check nonce
    if (!wp_verify_nonce($_POST['prioritet_nonce'], 'prioritet_q_edit_nonce')) {
        return;
    }

    // update the price
    if (isset($_POST['prioritet'])) {
        update_post_meta($post_id, 'prioritet', $_POST['prioritet']);
    }
}

add_action('admin_enqueue_scripts', 'column_enqueue_quick_edit_population');
function column_enqueue_quick_edit_population($pagehook)
{

    // do nothing if we are not on the target pages
    if ('edit.php' != $pagehook) {
        return;
    }
    wp_enqueue_script('populatequickedit', App\asset_path('scripts/populate.js'), ['jquery']);
}


/**
 * Bind predefined input for bulk edit box
 */
add_action('bulk_edit_custom_box', 'column_quick_edit_fields', 10, 2);

/**
 * Action to hook all rest routes
 */
add_action('rest_api_init', function () {

    // Defining rest api route for quick & bulk edit
    register_rest_route('bulk', '/edit', [
        'methods' => 'POST',
        'callback' => 'bulkedit',
    ]);
});

/**
 * Function to save bulk selected posts via rest api
 */
function bulkedit(WP_REST_Request $request)
{
    // for each post ID
    foreach ($_POST['post_ids'] as $id) {

        /**
         * if price is empty, maybe we shouldn't change it
         */
        if (!empty($_POST['prioritet'])) {
            update_post_meta($id, 'prioritet', $_POST['prioritet']);
        }
    }

    return $request;
    die();
}

add_filter("manage_edit-{$post_type_lokali}_sortable_columns", 'manage_sortable_columns');
add_filter("manage_edit-{$post_type_desavanja}_sortable_columns", 'manage_sortable_columns');
add_filter("manage_edit-{$post_type_event}_sortable_columns", 'manage_sortable_columns');
add_filter("manage_edit-{$post_type_novagodina}_sortable_columns", 'manage_sortable_columns');
function manage_sortable_columns($sortable_columns)
{
    /**
     * In this scenario, I already have a column with an
     * ID (or index) of 'prioritet column'. Both column
     * indexes MUST match.
     * $sortable_columns['prioritet'] = 'prioritet';
     * The value of the array item (after the =) is the
     * identifier of the column data.
     */
    $sortable_columns['prioritet'] = 'Prioritet';

    return $sortable_columns;
}

add_action('pre_get_posts', 'manage_wp_posts_pre_get_posts', 1);
function manage_wp_posts_pre_get_posts($query)
{
    /**
     * We only want our code to run in the main WP query
     * AND if an orderby query variable is designated.
     */
    if ($query->is_main_query() && ($orderby = $query->get('orderby'))) {
        switch ($orderby) {
            // If we're ordering by 'prioritet'
            case 'prioritet':
                // set our query's meta_key, which is used for custom fields
                $query->set('meta_key', 'prioritet');
                /**
                 * Tell the query to order by our custom field/meta_key's
                 * value, in this film rating's case: PG, PG-13, R, etc.
                 *
                 * If your meta value are numbers, change 'meta_value'
                 * to 'meta_value_num'.
                 */
                $query->set('orderby', 'prioritet');
                break;
        }
    }
}

/**
 * Removing comment section from administration
 */
add_action('admin_init', 'my_remove_admin_menus');
function my_remove_admin_menus()
{
    remove_menu_page('edit-comments.php');
}

/**
 * Calculating month for calendar
 */
function month($m)
{
    $months = [
        '1' => __('Januar', 'beogradnocu'),
        '2' => __('Februar', 'beogradnocu'),
        '3' => __('Mart', 'beogradnocu'),
        '4' => __('April', 'beogradnocu'),
        '5' => __('Maj', 'beogradnocu'),
        '6' => __('Jun', 'beogradnocu'),
        '7' => __('Jul', 'beogradnocu'),
        '8' => __('Avgust', 'beogradnocu'),
        '9' => __('Septembar', 'beogradnocu'),
        '10' => __('Oktobar', 'beogradnocu'),
        '11' => __('Novembar', 'beogradnocu'),
        '12' => __('Decembar', 'beogradnocu'),
    ];

    return $months[$m];
}

/**
 * Calculating month for calendar
 */
function monthshort($m)
{
    $months = [
        '1' => __('Jan', 'beogradnocu'),
        '2' => __('Feb', 'beogradnocu'),
        '3' => __('Mart', 'beogradnocu'),
        '4' => __('Apr', 'beogradnocu'),
        '5' => __('Maj', 'beogradnocu'),
        '6' => __('Jun', 'beogradnocu'),
        '7' => __('Jul', 'beogradnocu'),
        '8' => __('Avg', 'beogradnocu'),
        '9' => __('Sep', 'beogradnocu'),
        '10' => __('Okt', 'beogradnocu'),
        '11' => __('Nov', 'beogradnocu'),
        '12' => __('Dec', 'beogradnocu'),
    ];

    return $months[$m];
}

/**
 * Calculating days for calendar
 */
function day($d)
{
    $days = [
        'Mon' => __('Ponedeljak', 'beogradnocu'),
        'Tue' => __('Utorak', 'beogradnocu'),
        'Wed' => __('Sreda', 'beogradnocu'),
        'Thu' => __('Četvrtak', 'beogradnocu'),
        'Fri' => __('Petak ', 'beogradnocu'),
        'Sat' => __('Subota', 'beogradnocu'),
        'Sun' => __('Nedelja', 'beogradnocu'),
    ];
    if ($d > 6) {
        $d = $d - 7;
    }
    ;

    return $days[$d];
}

/**
 * Calculating days for calendar
 */
function days($d)
{
    $days = [
        'Mon' => __('Pon', 'beogradnocu'),
        'Tue' => __('Uto', 'beogradnocu'),
        'Wed' => __('Sre', 'beogradnocu'),
        'Thu' => __('Čet', 'beogradnocu'),
        'Fri' => __('Pet ', 'beogradnocu'),
        'Sat' => __('Sub', 'beogradnocu'),
        'Sun' => __('Ned', 'beogradnocu'),
    ];
    if ($d > 6) {
        $d = $d - 7;
    }
    ;

    return $days[$d];
}

/**
 * Calculating if cpt has event for calendar
 */
function haveday($dateg, $lokal)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "dogadjaji_bgn";
    $table_name2 = $wpdb->prefix . "dogadjaji_bgn_ns";
    // Selektovanje po datumima
    $lokal = '"' . $lokal . '"';
    // Preuzimanje i reorder za dati dan
    $query1 = 'SELECT * FROM ' . $table_name . ' WHERE date="' . $dateg . '" AND tip_lokala IN (' . $lokal . ') LIMIT 1'; // OR ('
    $result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);
    if (!$result1) {
        $query11 = "SELECT * FROM " . $table_name2 . " WHERE ";
        $query11 .= "(tip_repetition = 'd' OR danu_nedelji = '" . date('N', strtotime($dateg)) . "' AND tip_repetition = 'w') AND tip_lokala IN ($lokal) AND start_date >=$dateg LIMIT 1";
        $result11 = $wpdb->get_results($query11, ARRAY_A);
    }
    if ($result1 or $result11) {
        $reorg = 'da';
    } else {
        $reorg = 'no';
    }

    return $reorg;
}

/**
 * Defining image sizes globally
 */
add_action('after_setup_theme', 'custom_img_size');
function custom_img_size()
{
    add_image_size('lokali', 380, 275, true);
    add_image_size('lokali_logo', 200, 200, true);
    add_image_size('preporuka_sidebar', 350, 350, true);
    add_image_size('lokali_sidebar', 50, 50, true);
    add_image_size('tekstovi_sidebar', 350, 206, true);
    add_image_size('single', 350, 485, true);
    add_image_size('single_povezan', 255, 150, true);
    add_image_size('single_post', 730, 490, true);
    add_image_size('galerija-top', 225, 133, ['left', 'top']);
    add_image_size('galerija-group', 350, 207, ['left', 'top']);
    add_image_size('magazin_small', 255, 170, true);
    add_image_size('magazin_medium', 580, 387, true);
}

function get_webp_image_type($images, $sizes = '', $source_counter = 5)
{
    $counter = 0;
    foreach ($images as $image => $size):
        $webp_image = ABSPATH . str_replace(['.jpg', '.png'], [".webp", ".webp"], $image);
        clearstatcache();
        if (file_exists($webp_image)):
        ?>
		            <source <?php echo !empty($sizes) ? 'media="' . $sizes[$counter] . '"' : ''; ?>
		            <?php echo $source_counter > 4 ? 'data-srcset' : 'srcset' ?>="<?php echo str_replace(['.jpg', '.png'], [".webp", ".webp"], $image); ?>"
		            class="<?php echo $source_counter > 4 ? 'defer' : ''; ?>" type="image/webp">
		        <?php
endif;
    $counter++;
    endforeach;
}

// Dequeue styles and scripts
function dequeue_scripts_styles()
{
    wp_dequeue_style('dashicons');
    wp_dequeue_style('contact-form-7');
    wp_dequeue_style('wp-block-library');

    if (is_front_page()):
        wp_dequeue_script('contact-form-7');
    endif;
}

add_action('wp_enqueue_scripts', 'dequeue_scripts_styles', 100);

// Adding defer attribute to scripts
function add_defer_attribute($tag, $handle)
{
    $scripts_to_defer = [
        'sage/main.js',
        'carrier',
        'products',
        'single-reference',
        'home',
        'contact-form-7',
        'jquery-migrate',
        'front-page',
    ];

    foreach ($scripts_to_defer as $defer_script) {
        if ($defer_script === $handle) {
            return str_replace(' src', ' defer="defer" src', $tag);
        }
    }

    return $tag;
}

if (!is_user_logged_in()) {
    add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);
}

function custom_length_excerpt($word_count_limit)
{
    $content = wp_strip_all_tags(get_the_content(), true);

    return wp_trim_words($content, $word_count_limit);
}

// Removing emojis and other wordpress unused scripts and styles
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);
//remove_action('wp_head', [ $sitepress, 'meta_generator_tag', 20 ]);

// Remove query strings from styles and scripts
function remove_css_js_ver($src)
{
    if (strpos($src, '?ver=')) {
        $src = remove_query_arg('ver', $src);
    }

    return $src;
}

add_action('init', 'remove_css_js_ver');
add_filter('wpcf7_autop_or_not', '__return_false');

function my_custom_switcher()
{
    $languages = icl_get_languages('skip_missing=1&orderby=code&order=desc');
    echo '<div class="yl-menu-switcher">
            <div class="ylms-' . ICL_LANGUAGE_CODE . '">';
    foreach ($languages as $l) {
        if (!$l['active']) {
            $langs[] = '<a href="' . $l['url'] . '">' . $l['language_code'] . '</a>';
        } else {
            $langs[] = strtoupper($l['language_code']);
        }
    }
    echo '</div></div>';
}

function change_wp_search_size($query)
{
    if ($query->is_search) {
        $query->set('posts_per_page', -1);
        $query->set('post_type', ['kafane_beograd', 'klubovi_beograd', 'splavovi_beograd', 'restorani_beograd', 'barovi_beograd', 'striptiz_beograd']);
    }
    return $query;
}

if (!is_admin()) {
    add_filter('pre_get_posts', 'change_wp_search_size');
}

if (!function_exists('array_group_by')) {
    /**
     * Groups an array by a given key.
     *
     * Groups an array into arrays by a given key, or set of keys, shared between all array members.
     *
     * Based on {@author Jake Zatecky}'s {@link https://github.com/jakezatecky/array_group_by array_group_by()} function.
     * This variant allows $key to be closures.
     *
     * @param array $array   The array to have grouping performed on.
     * @param mixed $key,... The key to group or split by. Can be a _string_,
     *                       an _integer_, a _float_, or a _callable_.
     *
     *                       If the key is a callback, it must return
     *                       a valid key from the array.
     *
     *                       If the key is _NULL_, the iterated element is skipped.
     *
     *                       ```
     *                       string|int callback ( mixed $item )
     *                       ```
     *
     * @return array|null Returns a multidimensional array or `null` if `$key` is invalid.
     */
    function array_group_by(array $array, $key)
    {
        if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key)) {
            trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
            return null;
        }

        $func = (!is_string($key) && is_callable($key) ? $key : null);
        $_key = $key;

        // Load the new array, splitting by the target key
        $grouped = [];
        foreach ($array as $value) {
            $key = null;

            if (is_callable($func)) {
                $key = call_user_func($func, $value);
            } elseif (is_object($value) && property_exists($value, $_key)) {
                $key = $value->{$_key};
            } elseif (isset($value[$_key])) {
                $key = $value[$_key];
            }

            if ($key === null) {
                continue;
            }

            $grouped[$key][] = $value;
        }

        // Recursively build a nested grouping if more parameters are supplied
        // Each grouped array value is grouped according to the next sequential key
        if (func_num_args() > 2) {
            $args = func_get_args();

            foreach ($grouped as $key => $value) {
                $params = array_merge([$value], array_slice($args, 2, func_num_args()));
                $grouped[$key] = call_user_func_array('array_group_by', $params);
            }
        }

        return $grouped;
    }
}


function wpa_course_post_link($post_link, $id = 0)
{
    $post = get_post($id);
    if (is_object($post)) {
        $terms = wp_get_object_terms($post->ID, 'izvodjaci');
        if ($terms) {
            return str_replace('%izvodjaci%', $terms[0]->slug, $post_link);
        }
    }
    return $post_link;
}
add_filter('post_type_link', 'wpa_course_post_link', 1, 3);




add_action('rest_api_init', function () {
    register_rest_route('load', '/calendar', [
        'methods' => 'GET',
        'callback' => 'load_calendar',
    ]);
});

function load_calendar($request)
{
    $posts_data = [];
    $posts = [];
    $results = [];
    (int) $month = $request->get_param('month');
    (int) $year = $request->get_param('year');
    $meseci = ['Januar','Februar','Mart','April','Maj','Jun','Jul','Avgust','Septembar','Oktobar','Novembar','Decembar'];


    if ($month > 12) {
        $year++;
    } elseif ($month < 1) {
        $year--;
    }
    $montName = date("F", strtotime('00-' . $month . '-01'));

    $query_date = '01-' . $month . '-' . $year;
    $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $first_day = date('Y-m-01', strtotime($query_date));
    $last_day_test = date('Y-m-31', strtotime($query_date));
    $first_day_named = date('l', strtotime($first_day));

    $day_of_week = (int) date('N', strtotime($first_day_named)) - 1;
    $days = [];

    for ($day = 1; $day <= $day_of_week; $day++):
        $days[] = '';
    endfor;

    for ($day = 1; $day <= $daysInMonth; $day++):
        $days[] = $day;

        $current_date = $day . '-' . $month . '-' . $year;
        $current_date = date('d-m-Y', strtotime($current_date));
        $date_2 = date('Ymd', strtotime($current_date));
        $posts = new WP_Query([
            'post_type' => 'galerija',
            'status' => 'published',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => 'datum_galerije',
                    'value' => $date_2,
                    'compare' => '=',
                    'type' => 'DATE',
                ),
            ),
        ]);


        if ($posts->have_posts()) {
            while ($posts->have_posts()) {
                $posts->the_post();
                $lokal = get_field('lokal', get_the_ID());
                $title_lokala = $lokal->post_title;
                $link_lokala = get_permalink($lokal->ID);
                $datum_galerije = date("d-m-Y", strtotime(get_field('datum_galerije', get_the_ID())));
                $success = 0;
                $active = '';
                if($datum_galerije === $current_date) {
                    $success = 1;
                    $active = 'active';
                }

                $results[] = [
                    'title_lokala' => $title_lokala,
                    'link_lokala' => $link_lokala,
                    'datum_galerije' => $datum_galerije,
                    'date' => $current_date,
                    'success' => $success,
                    'active' => $active
                ];
            }
        }
        wp_reset_postdata();

    endfor;

    $posts_data[] = [
        'month' => $month,
        'year' => $year,
        'next_year' => $year + 1,
        'previous_year' => $year - 1,
        'first_day' => $first_day,
        'last_day' => $last_day,
        'days_in_month' => $daysInMonth,
        'month_name' => $montName,
        'icl_month'=> $meseci[$month-1],
        'day' => $day_of_week,
        'days' => $days,
        'days_count' => count($days),
        'posts' => $results,
    ];

    // Last day of the month.
    $last_day = (int) date('Y-m-t', strtotime($query_date));

    $date_1 = date('Ymd', strtotime($first_day));
    $date_2 = date('Ymd', strtotime($last_day_test));

    return $posts_data;
}

function galerije($request)
{
    $p_data = [];
    $date = $request->get_param('date');
    $posts = new WP_Query([
            'post_type' => 'galerija',
            'status' => 'published',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => 'datum_galerije',
                    'value' => date('Ymd',strtotime($date)),
                    'compare' => '=',
                    'type' => 'DATE',
                ),
            ),
        ]);

    if ($posts->have_posts()) {
        while ($posts->have_posts()) {
            $posts->the_post();


           $gal_date = get_field('datum_galerije');
           $date = strtotime($gal_date);
           $gal_date = date('d.m.Y',$date);
           $gallery = get_field( 'galerija' );
           $lokal = get_field('lokal',$posts->ID);
           $lokal_url = get_permalink($lokal->ID);
           $lokal_title = get_the_title($lokal->ID);

            $p_data[] = [
                'id' => get_the_ID(),
                'title' => get_the_title(),
                'link' => get_permalink(),
                'image' => $gallery[0]['url_do_fotografije'],
                'date' => $gal_date,
                'galerija' => $gallery,
                'num_photos'=> count($gallery),
                'title_lokala'=>$lokal_title,
                'url_lokala'=>$lokal_url
            ];
        }
    }
    wp_reset_postdata();
    return $p_data;
}
