<?php
/*
Template Name: XML Lista Lokala
*/

header("Content-type: text/xml");
echo "<?xml version='1.0' encoding='UTF-8'?>";
echo '<Objects>';
$listaposttype = array(
  'klubovi_beograd',
  'splavovi_beograd',
  'kafane_beograd',
  'barovi_beograd',
  'restorani_beograd',
  'striptiz_beograd',
  'desavanja'
);

foreach ($listaposttype as $posttypet) {
  query_posts("post_type=$posttypet&orderby=title&order=ASC&posts_per_page=-1");
  if (have_posts()) :
    echo '<' . $posttypet . '>';
    while (have_posts()) : the_post();
      echo '<' . $posttypet . '-item>'; // otvaranje pojedinacnog lokala
      echo '<Name>' . htmlspecialchars(get_the_title()) . '</Name>';
      $url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
      echo '<Image>' . $url . '</Image>';
      echo '<About>' . htmlspecialchars(get_the_content()) . '</About>';

      $listastolota = get_post_meta($post->ID, 'beogradnocu_tipsedenja_event', true);
      $listaarray   = explode(',', $listastolota);
      foreach ($listaarray as $pozicija) {
        echo '<Table>' . $pozicija . '</Table>';
      }

      echo '</' . $posttypet . '-item>'; // zatvaranje pojedinacnog lokala
    endwhile;
    echo '</' . $posttypet . '>';
  endif;
}
echo "</Objects>";
