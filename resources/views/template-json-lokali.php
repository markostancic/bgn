<?php

header('Content-Type: application/json');
/*
Template Name: JSON Lokala
*/
$table_name1 = $wpdb->prefix . "pixs_gallery";
$table_name2 = $wpdb->prefix . "pixs_gallery_pic";
$table_name3 = $wpdb->prefix . "posts";
$table_name4 = $wpdb->prefix . "postmeta";
$table_name5 = $wpdb->prefix . "dogadjaji_bgn";
$table_name6 = $wpdb->prefix . "dogadjaji_bgn_ns";

if (isset($_REQUEST['lokalid'])) {
    $lokalId = $_REQUEST['lokalid'];
}
if (isset($_REQUEST['tiplokala'])) {
    $tipLokala = $_REQUEST['tiplokala'];
}
if (isset($_REQUEST['galleryid'])) {
	$galleryid = $_REQUEST['galleryid'];
}

if (array_key_exists('programi', $_REQUEST)) {
    $today = date('Y-m-d H:i:s');
	$today = date('Y-m-d H:i:s', strtotime($today));
	$today_print = $today;
	
	$days = array(
		date('Y-m-d', strtotime($today)),
		date('Y-m-d', strtotime($today . ' + 1 day')),
		date('Y-m-d', strtotime($today . ' + 2 day')),
		date('Y-m-d', strtotime($today . ' + 3 day')),
		date('Y-m-d', strtotime($today . ' + 4 day')),
		date('Y-m-d', strtotime($today . ' + 5 day')),
		date('Y-m-d', strtotime($today . ' + 6 day'))
	);	
	
	$program = array(
		'today' => $today_print,
		'program' => array()
	);
	
	$tipoviLokala = array(		
		'klubovi',
		'splavovi',		
		'kafane',		
		'barovi',
		'kafei',
		'restorani',		
		'striptizbarovi'
	);
		
	foreach($days as $date){
		$daydata = array(
			'date' => $date,
		);		
		foreach($tipoviLokala as $kategorija) {			
			$poTipu[$kategorija] = array();
			query_posts(
				array(
					'post_type'=> array(
						$kategorija
					), 
					'orderby'=>'title', 
					'order'=>'ASC', 
					'posts_per_page'=>'-1'
				));			
			if (have_posts()) : 
				while (have_posts()) : the_post();							
					$localid = $post->ID;					
					$postdata = get_post($lokalId, ARRAY_A);					
					$lokaldata['name'] = $postdata['post_title'];
					$lokaldata['id_lokal'] = $localid;
					$query1 = 'SELECT * FROM ' . $table_name5 . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date . '"';
					$result1= $wpdb->get_results($query1, ARRAY_A);
					$query11 = 'SELECT * FROM ' . $table_name6 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date)) . '" AND tip_repetition = "w") ORDER BY priority DESC'; //(danu_nedelji = "'. date('N', strtotime($date1)) . '" )
					if (!$result1) {
						$result1 = $wpdb->get_results($query11, ARRAY_A);
					}
					if ($result1) {
						$result = $result1[0];
						$dogtitle = '';						
						$url_event = wp_get_attachment_url(get_post_thumbnail_id($result['id_dogadjaja']));
						$dogadjaj = get_post($result['id_dogadjaja'], ARRAY_A);                    											
						if (function_exists('icl_object_id') & ICL_LANGUAGE_CODE != $deflang) {
							global $sitepress;
							$deflang = $sitepress->get_default_language();
							$polje2 = ICL_LANGUAGE_CODE . '_beogradnocu_title_event';
							$dogtitle = get_post_meta($result['id_dogadjaja'], $polje2, true);
							if ($dogtitle == '') {
								$dogtitle = $dogadjaj['post_title'];
							}
						} else {
							$dogtitle = $dogadjaj['post_title'];
						}
						$lokaldata['dogadjaj'] = $dogtitle;						
						$lokaldata['priority'] = $result['priority'];
												
						$beogradnocu_meta = get_post_meta($localid);				
						// adresa
						if (isset($beogradnocu_meta['beogradnocu_ulica'])) {
							$adresa = $beogradnocu_meta['beogradnocu_ulica']['0'];
						}
						// radnovreme
						if (isset($beogradnocu_meta['beogradnocu_radno_vreme'])) {
							$radnovreme = $beogradnocu_meta['beogradnocu_radno_vreme']['0'];
						}
						// kapacitetlokala
						if (isset($beogradnocu_meta['beogradnocu_kapacitet'])) {
							$kapacitet = $beogradnocu_meta['beogradnocu_kapacitet']['0'];
						}

						// muzickizanr
						$muzika = get_the_terms($localid, 'muzikazanr');
						if(!empty($muzika) && $muzika && !is_wp_error($muzika)){
							$musarray = array();
							foreach ( $muzika as $term ) {
								array_push($musarray, $term->name);
							}
							$muzickizanr = join(', ', $musarray);
						}
						elseif (isset($beogradnocu_meta['beogradnocu_zanr'])) {
							$muzickizanr = $beogradnocu_meta['beogradnocu_zanr']['0'];
						}
						
						if(isset($beogradnocu_meta['beogradnocu_google_location'])) {
							$geolocation = $beogradnocu_meta['beogradnocu_google_location']['0'];
						}							
															
						if (strpos($url_event, 'https://www.beogradnocu.com') !== FALSE) {
							$url = $url_event;
						} else {
							$url = wp_get_attachment_url(get_post_thumbnail_id($localid));
						}
						if (strpos($url, 'https://www.beogradnocu.com') === FALSE) {
							$url = "";
						}
						$listaLokala = array(
							'name' => $post->post_title,
							'id' => $localid,
							'url' => $post->guid,
							'image' => $url,							
							'adresa' => $adresa,
							'radnovreme' => $radnovreme,
							'kapacitetlokala' => $kapacitet,
							'muzickizanr' => $muzickizanr,
							'geolocation' => $geolocation,
						);
						$lokaldata['data'] = $listaLokala;						
						
						array_push($poTipu[$kategorija], $lokaldata);
					}							
				endwhile;				
			endif;		
			array_push($daydata['dogadjaji'][$kategorija] = $poTipu[$kategorija]);			
		}				
		array_push($program['program'], $daydata);		
	}	
	wp_send_json($program);	
}

if (array_key_exists('programprioritet', $_REQUEST)) {
    $today = date('Y-m-d H:i:s');
	$today = date('Y-m-d H:i:s', strtotime($today));
	$today_print = $today;
	
	$days = array(
		date('Y-m-d', strtotime($today)),
		date('Y-m-d', strtotime($today . ' + 1 day')),
		date('Y-m-d', strtotime($today . ' + 2 day')),
		date('Y-m-d', strtotime($today . ' + 3 day')),
		date('Y-m-d', strtotime($today . ' + 4 day')),
		date('Y-m-d', strtotime($today . ' + 5 day')),
		date('Y-m-d', strtotime($today . ' + 6 day'))
	);	
	
	$baner = array(
		'image' => 'https://www.beogradnocu.com/wp-content/uploads/2014/08/baner2020.jpg',
		'visible' => false,
	);
	
	$program = array(
		'today' => $today_print,
		'baner' => $baner,
		'program' => array()
	);
	
	$tipoviLokala = array(		
		'klubovi',
		'splavovi',		
		'kafane',		
		'barovi',
		'kafei',
		'restorani',		
		'striptizbarovi'
	);
		
	foreach($days as $date){
		$daydata = array(
			'date' => $date,
			'lokali' => array()
		);		
		foreach($tipoviLokala as $kategorija) {			
			query_posts(
				array(
					'post_type'=> array(
						$kategorija
					), 
					'orderby'=>'title', 
					'order'=>'ASC', 
					'posts_per_page'=>'-1'
				));			
			if (have_posts()) : 
				while (have_posts()) : the_post();							
					$localid = $post->ID;					
					$postdata = get_post($localid, ARRAY_A);					
					$lokaldata['name'] = $postdata['post_title'];
					$lokaldata['id_lokal'] = $localid;
					$query1 = 'SELECT * FROM ' . $table_name5 . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date . '"';
					$result1= $wpdb->get_results($query1, ARRAY_A);
					$query11 = 'SELECT * FROM ' . $table_name6 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date)) . '" AND tip_repetition = "w") ORDER BY priority DESC'; //(danu_nedelji = "'. date('N', strtotime($date1)) . '" )
					if (!$result1) {
						$result1 = $wpdb->get_results($query11, ARRAY_A);
					}
					if ($result1) {
						$result = $result1[0];
						$dogtitle = '';						
						$url_event = wp_get_attachment_url(get_post_thumbnail_id($result['id_dogadjaja']));
						$dogadjaj = get_post($result['id_dogadjaja'], ARRAY_A);                    											
						if (function_exists('icl_object_id') & ICL_LANGUAGE_CODE != $deflang) {
							global $sitepress;
							$deflang = $sitepress->get_default_language();
							$polje2 = ICL_LANGUAGE_CODE . '_beogradnocu_title_event';
							$dogtitle = get_post_meta($result['id_dogadjaja'], $polje2, true);
							if ($dogtitle == '') {
								$dogtitle = $dogadjaj['post_title'];
							}
						} else {
							$dogtitle = $dogadjaj['post_title'];
						}
						$lokaldata['dogadjaj'] = $dogtitle;						
						$lokaldata['priority'] = $result['priority'];
												
						$beogradnocu_meta = get_post_meta($localid);				
						// adresa
						if (isset($beogradnocu_meta['beogradnocu_ulica'])) {
							$adresa = $beogradnocu_meta['beogradnocu_ulica']['0'];
						}
						// radnovreme
						if (isset($beogradnocu_meta['beogradnocu_radno_vreme'])) {
							$radnovreme = $beogradnocu_meta['beogradnocu_radno_vreme']['0'];
						}
						// kapacitetlokala
						if (isset($beogradnocu_meta['beogradnocu_kapacitet'])) {
							$kapacitet = $beogradnocu_meta['beogradnocu_kapacitet']['0'];
						}

						// muzickizanr
						$muzika = get_the_terms($localid, 'muzikazanr');
						if(!empty($muzika) && $muzika && !is_wp_error($muzika)){
							$musarray = array();
							foreach ( $muzika as $term ) {
								array_push($musarray, $term->name);
							}
							$muzickizanr = join(', ', $musarray);
						}
						elseif (isset($beogradnocu_meta['beogradnocu_zanr'])) {
							$muzickizanr = $beogradnocu_meta['beogradnocu_zanr']['0'];
						}
						
						if(isset($beogradnocu_meta['beogradnocu_google_location'])) {
							$geolocation = $beogradnocu_meta['beogradnocu_google_location']['0'];
						}							
															
						if (strpos($url_event, 'https://www.beogradnocu.com') !== FALSE) {
							$url = $url_event;
						} else {
							$url = wp_get_attachment_url(get_post_thumbnail_id($localid));
						}
						if (strpos($url, 'https://www.beogradnocu.com') === FALSE) {
							$url = "";
						}
																		
						$filter = wp_get_post_terms($result['id_dogadjaja'], 'program-lokala',  array("fields" => "names"));
						if(!empty($filter) && $filter && !is_wp_error($filter)){
							$musarray = array();
							foreach ( $filter as $term ) {
								array_push($musarray, $term->name);
							}
							$filteri = join(', ', $musarray);
						}
						
						$listaLokala = array(
							'name' => $post->post_title,
							'id' => $localid,
							'url' => $post->guid,
							'image' => $url,							
							'adresa' => $adresa,
							'radnovreme' => $radnovreme,
							'kapacitetlokala' => $kapacitet,
							'muzickizanr' => $muzickizanr,
							'geolocation' => $geolocation,
							'filter' => $filter,								
						);
						$lokaldata['data'] = $listaLokala;						
												
						array_push($daydata['lokali'], $lokaldata);	
					}							
				endwhile;				
			endif;							
		}				
		array_push($program['program'], $daydata);		
	}	
	wp_send_json($program);	
}

if (array_key_exists('program', $_REQUEST)) {
    $today = date('Y-m-d H:i:s');
	$today = date('Y-m-d H:i:s', strtotime($today));
	$today_print = $today;
	
	$days = array(
		date('Y-m-d', strtotime($today)),
		date('Y-m-d', strtotime($today . ' + 1 day')),
		date('Y-m-d', strtotime($today . ' + 2 day')),
		date('Y-m-d', strtotime($today . ' + 3 day')),
		date('Y-m-d', strtotime($today . ' + 4 day')),
		date('Y-m-d', strtotime($today . ' + 5 day')),
		date('Y-m-d', strtotime($today . ' + 6 day'))
	);
	$program = array(
		'today' => $today,
		'program' => array()
	);
	foreach($days as $date){
		$daydata = array(
			'date' => $date,
			'dogadjaji' => array()
		);
			
		query_posts(
			array(
			'post_type'=> array(
				'klubovi',
				'splavovi',
				'kafane',
				'barovi',
				'kafei',
				'restorani',
				'striptizbarovi'
			), 
			'orderby'=>'title', 
			'order'=>'ASC', 
			'posts_per_page'=>'-1'
		));
		if (have_posts()) : 
			while (have_posts()) : the_post();							
				$localid = $post->ID;
				$postdata = get_post($lokalId, ARRAY_A);				
				$url = wp_get_attachment_url(get_post_thumbnail_id($postdata['ID']));
				$lokaldata['name'] = $postdata['post_title'];
				$lokaldata['id_lokal'] = $localid;
				$lokaldata['image'] = $url;
				$query1 = 'SELECT * FROM ' . $table_name5 . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date . '"';
				$result1= $wpdb->get_results($query1, ARRAY_A);
				$query11 = 'SELECT * FROM ' . $table_name6 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date)) . '" AND tip_repetition = "w") ORDER BY priority DESC'; //(danu_nedelji = "'. date('N', strtotime($date1)) . '" )
				if (!$result1) {
					$result1 = $wpdb->get_results($query11, ARRAY_A);
				}
				if ($result1) {
					$result = $result1[0];
					$dogtitle = '';
					$dogadjaj = get_post($result['id_dogadjaja'], ARRAY_A);                    					
					if (function_exists('icl_object_id') & ICL_LANGUAGE_CODE != $deflang) {
						global $sitepress;
						$deflang = $sitepress->get_default_language();
						$polje2 = ICL_LANGUAGE_CODE . '_beogradnocu_title_event';
						$dogtitle = get_post_meta($result['id_dogadjaja'], $polje2, true);
						if ($dogtitle == '') {
							$dogtitle = $dogadjaj['post_title'];
						}
					} else {
						$dogtitle = $dogadjaj['post_title'];
					}
					$lokaldata['dogadjaj'] = $dogtitle;					
					array_push($daydata['dogadjaji'], $lokaldata);
				}							
			endwhile;
			array_push($program['program'], $daydata);
		endif;
	}	
		
	wp_send_json($program);	
	//echo json_encode($program);
}

if(empty($_REQUEST)){
	query_posts(
	array(
		'post_type'=> array(
			'klubovi',
			'splavovi',
			'kafane',
			'barovi',
			'kafei',
			'restorani',
			'striptizbarovi'
		), 
		'orderby'=>'title', 
		'order'=>'ASC', 
		'posts_per_page'=>'-1'
	));
    $listaLokala = array();
    if (have_posts()) : 
        while (have_posts()) : the_post();
            $beogradnocu_prikazilokal = get_post_meta($post->ID, 'beogradnocu_prikazivanjelokala',TRUE);
            if($beogradnocu_prikazilokal === ''){
                $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                $listaLokala[] = array(
                    'name' => $post->post_title,
                    'id' => $post->ID,
                    'image' => $url,
					'tip_lokala' => $post->post_type
                );
            }
        endwhile;
    endif;
	
	wp_send_json($listaLokala);
    //echo json_encode($listaLokala);
}

if (isset($tipLokala)) {
    query_posts("post_type=$tipLokala&orderby=title&order=ASC&posts_per_page=-1");
    $listaLokala = array();
	if (have_posts()) : 
        while (have_posts()) : the_post();						
            $beogradnocu_prikazilokal = get_post_meta($post->ID, 'beogradnocu_prikazivanjelokala',TRUE);			
            if($beogradnocu_prikazilokal === '') {
				$beogradnocu_meta = get_post_meta($post->ID);				
				// adresa
				if (isset($beogradnocu_meta['beogradnocu_ulica'])) {
					$adresa = $beogradnocu_meta['beogradnocu_ulica']['0'];
				}
				// radnovreme
				if (isset($beogradnocu_meta['beogradnocu_radno_vreme'])) {
					$radnovreme = $beogradnocu_meta['beogradnocu_radno_vreme']['0'];
				}
				// kapacitetlokala
				if (isset($beogradnocu_meta['beogradnocu_kapacitet'])) {
					$kapacitet = $beogradnocu_meta['beogradnocu_kapacitet']['0'];
				}

				// muzickizanr
				$muzika = get_the_terms($post->ID, 'muzikazanr');
				if(!empty($muzika) && $muzika && !is_wp_error($muzika)){
					$musarray = array();
					foreach ( $muzika as $term ) {
						array_push($musarray, $term->name);
					}
					$muzickizanr = join(', ', $musarray);
				}
				elseif (isset($beogradnocu_meta['beogradnocu_zanr'])) {
					$muzickizanr = $beogradnocu_meta['beogradnocu_zanr']['0'];
				}
				
				if(isset($beogradnocu_meta['beogradnocu_google_location'])) {
					$geolocation = $beogradnocu_meta['beogradnocu_google_location']['0'];
				}							
				
                $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                $listaLokala[] = array(
                    'name' => $post->post_title,
                    'id' => $post->ID,
					'url' => $post->guid,
                    'image' => $url,
				    'adresa' => $adresa,
					'radnovreme' => $radnovreme,
					'kapacitetlokala' => $kapacitet,
					'muzickizanr' => $muzickizanr,
					'geolocation' => $geolocation,
                );
            }
        endwhile;
    endif;
		
	wp_send_json($listaLokala);
    //echo json_encode($listaLokala);
}

if (isset($lokalId)) {
    $postdata = get_post($lokalId, ARRAY_A);

	if($postdata == null) {
		echo "{}";
		return;
	}
	
    $lokaldata = array();
    $lokaldata['about'] = htmlspecialchars($postdata['post_content']);
    $lokaldata['name'] = $postdata['post_title'];
    $lokaldata['post_date'] = $postdata['post_date'];
    $lokaldata['post_date_gmt'] = $postdata['post_date_gmt'];
    $lokaldata['post_modified'] = $postdata['post_modified'];
    $lokaldata['post_modified_gmt'] = $postdata['post_modified_gmt'];
    $lokaldata['menu_order'] = $postdata['menu_order'];
	$lokaldata['gallery'] = array();
		
	if($lokaldata['name'] == "") {
		echo "{}";
		return;
	}

    $listastolota = get_post_meta($lokalId); // , 'beogradnocu_tipsedenja_event', true

	$location = $postdata['post_title'];//get_post_meta($lokalId, 'beogradnocu_listagalerijazalokal',TRUE);

	$query1 = 'SELECT id, name, date, photocount, previewpic FROM '.$table_name1.' WHERE location LIKE "%'.$location.'" ORDER BY date DESC LIMIT 50';		
	
	$result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);		

	if($result1){		
		foreach($result1 as $fe){
			$gallerylist = array(
				'id_gallery' => $fe['id'],
				'name' => $fe['name'],
				'date' => $fe['date'],
				'photocount' => $fe['photocount'],
				'previewpic' => $fe['previewpic']
			);
			array_push($lokaldata['gallery'], $gallerylist);
		}
	}
	
    // pozicije lokala
    $lokaldata['pozicije'] = array();
    if (isset($listastolota['beogradnocu_tipsedenja_event'])) {
        $listaarray = explode(',', $listastolota['beogradnocu_tipsedenja_event'][0]);
        foreach ($listaarray as $pozicija) {
            array_push($lokaldata['pozicije'], $pozicija);
        }
    }

    // adresa
    if (isset($listastolota['beogradnocu_ulica'])) {
        $lokaldata['adresa'] = $listastolota['beogradnocu_ulica']['0'];
    }
    // radnovreme
    if (isset($listastolota['beogradnocu_radno_vreme'])) {
        $lokaldata['radnovreme'] = $listastolota['beogradnocu_radno_vreme']['0'];
    }
    // kapacitetlokala
    if (isset($listastolota['beogradnocu_kapacitet'])) {
        $lokaldata['kapacitetlokala'] = $listastolota['beogradnocu_kapacitet']['0'];
    }

    // muzickizanr
    $muzika = get_the_terms($lokalId, 'muzikazanr');
    if(!empty($muzika) && $muzika && !is_wp_error($muzika)){
        $musarray = array();
        foreach ( $muzika as $term ) {
            array_push($musarray, $term->name);
        }
        $lokaldata['muzickizanr'] = join(', ', $musarray);
    }
    elseif (isset($listastolota['beogradnocu_zanr'])) {
        $lokaldata['muzickizanr'] = $listastolota['beogradnocu_zanr']['0'];
    }


    // deo grada
    $lokacija = get_the_terms($lokalId, 'deograda');
    if(!empty($lokacija) && $lokacija && !is_wp_error($lokacija)){
        $musarray = array();
        foreach ( $lokacija as $term ) {
            array_push($musarray, $term->name);
        }
        $lokaldata['deograda'] = join(', ', $musarray);
    }

    // tipkuhinje
    $kuhinja = get_the_terms($lokalId, 'tiprestorana');
    if(!empty($kuhinja) && $kuhinja && !is_wp_error($kuhinja)){
        $musarray = array();
        foreach ( $kuhinja as $term ) {
            array_push($musarray, $term->name);
        }
        $lokaldata['tipkuhinje'] = join(', ', $musarray);
    }


    $lokaldata['program'] = array();
    // za program lokala
    if (function_exists('icl_object_id')) {
        global $sitepress;
        $deflang = $sitepress->get_default_language();
        $posttypes = get_post_type($lokalId);
        $localid = icl_object_id($lokalId, $posttypes, true, $deflang);
    } else {
        $localid = $lokalId;
    }

    global $wpdb;
    $table_name = $wpdb->prefix . "dogadjaji_bgn";
    $table_name2 = $wpdb->prefix . "dogadjaji_bgn_ns";

    $startline = date_i18n('N');

    $today = date_i18n('Y-n-d');
    $posttypes = get_post_type($lokalId);
    if ($posttypes === 'putovanja' or $posttypes === 'desavanja') {
        $query1 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date > "' . $today . '"';
        $result = $wpdb->get_results($query1, ARRAY_A);
        if (isset($result) and !empty($result)) {
            foreach ($result as $v) {
                $dogadjaj = get_post($v['id_dogadjaja'], ARRAY_A);

                if (function_exists('icl_object_id') & ICL_LANGUAGE_CODE != $deflang) {
                    global $sitepress;
                    $deflang = $sitepress->get_default_language();
                    $polje2 = ICL_LANGUAGE_CODE . '_beogradnocu_title_event';
                    $dogtitle = get_post_meta($v['id_dogadjaja'], $polje2, true);
                    if (!$dogtitle) {
                        $dogtitle = $dogadjaj['post_title'];
                    }
                } else {
                    $dogtitle = $dogadjaj['post_title'];
                }

                $lokaldata['program'][] = array(
                    'date' => $v['date'],
                    'name' => $dogtitle
                );
            }
        }

        
    }
    else{


            $date1 = $today;
            $date2 = date('Y-m-d', strtotime($today . ' + 1 day'));
            $date3 = date('Y-m-d', strtotime($today . ' + 2 day'));
            $date4 = date('Y-m-d', strtotime($today . ' + 3 day'));
            $date5 = date('Y-m-d', strtotime($today . ' + 4 day'));
            $date6 = date('Y-m-d', strtotime($today . ' + 5 day'));
            $date7 = date('Y-m-d', strtotime($today . ' + 6 day'));


            // Selektovanje po datumima


            $query1 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date1 . '" LIMIT 1';
            $result1 = $wpdb->get_results($query1, ARRAY_A);
            $query11 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date1)) . '" AND tip_repetition = "w") ORDER BY priority DESC LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date1)) . '" )
            if (!$result1) {
                $result1 = $wpdb->get_results($query11, ARRAY_A);
            }
            if ($result1) {
                $result[1] = $result1[0];
            }


            $query2 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date2 . '" LIMIT 1';
            $result2 = $wpdb->get_results($query2, ARRAY_A);
            $query22 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date2)) . '" AND tip_repetition = "w") ORDER BY priority DESC LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date1)) . '" )
            if (!$result2) {
                $result2 = $wpdb->get_results($query22, ARRAY_A);
            }
            if ($result2) {
                $result[2] = $result2[0];
            }


            $query3 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date3 . '" LIMIT 1';
            $result3 = $wpdb->get_results($query3, ARRAY_A);
            $query33 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date3)) . '" AND tip_repetition = "w") ORDER BY priority DESC LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date3)) . '" )
            if (!$result3) {
                $result3 = $wpdb->get_results($query33, ARRAY_A);
            }
            if ($result3) {
                $result[3] = $result3[0];
            }


            $query4 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date4 . '" LIMIT 1';
            $result4 = $wpdb->get_results($query4, ARRAY_A);
            $query44 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date4)) . '" AND tip_repetition = "w") ORDER BY priority DESC LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date4)) . '" )
            if (!$result4) {
                $result4 = $wpdb->get_results($query44, ARRAY_A);
            }
            if ($result4) {
                $result[4] = $result4[0];
            }


            $query5 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date5 . '" LIMIT 1';
            $result5 = $wpdb->get_results($query5, ARRAY_A);
            $query55 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date5)) . '" AND tip_repetition = "w") ORDER BY priority DESC LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date5)) . '" )
            if (!$result5) {
                $result5 = $wpdb->get_results($query55, ARRAY_A);
            }
            if ($result5) {
                $result[5] = $result5[0];
            }


            $query6 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date6 . '" LIMIT 1';
            $result6 = $wpdb->get_results($query6, ARRAY_A);
            $query66 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date6)) . '" AND tip_repetition = "w") LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date6)) . '" )
            if (!$result6) {
                $result6 = $wpdb->get_results($query66, ARRAY_A);
            }
            if ($result6) {
                $result[6] = $result6[0];
            }


            $query7 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date7 . '" LIMIT 1';
            $result7 = $wpdb->get_results($query7, ARRAY_A);
            $query77 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date7)) . '" AND tip_repetition = "w") LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date7)) . '" )
            if (!$result7) {
                $result7 = $wpdb->get_results($query77, ARRAY_A);
            }
            if ($result7) {
                $result[7] = $result7[0];
            }

            if (isset($result)) {
                foreach ($result as $k => $v) {
                    $dogadjaj = get_post($v['id_dogadjaja'], ARRAY_A);
                    switch ($k) {
                        case 1:
                            $todate = date('d.m.Y', strtotime($today));//$daprint1;
                            break;
                        case 2:
                            $todate = date('d.m.Y', strtotime($today . ' + 1 day'));//$daprint2;
                            break;
                        case 3:
                            $todate = date('d.m.Y', strtotime($today . ' + 2 day'));//$daprint3;
                            break;
                        case 4:
                            $todate = date('d.m.Y', strtotime($today . ' + 3 day'));//$daprint4;
                            break;
                        case 5:
                            $todate = date('d.m.Y', strtotime($today . ' + 4 day'));//$daprint5;
                            break;
                        case 6:
                            $todate = date('d.m.Y', strtotime($today . ' + 5 day'));//$daprint6;
                            break;
                        case 7:
                            $todate = date('d.m.Y', strtotime($today . ' + 6 day'));//$daprint7;
                            break;
                    }
                    if (function_exists('icl_object_id') & ICL_LANGUAGE_CODE != $deflang) {
                        global $sitepress;
                        $deflang = $sitepress->get_default_language();
                        $polje2 = ICL_LANGUAGE_CODE . '_beogradnocu_title_event';
                        $dogtitle = get_post_meta($v['id_dogadjaja'], $polje2, true);
                        if ($dogtitle == '') {
                            $dogtitle = $dogadjaj['post_title'];
                        }
                    } else {
                        $dogtitle = $dogadjaj['post_title'];
                    }

                    $lokaldata['program'][] = array(
                        'date' => $todate,
                        'name' => $dogtitle
                    );


                }
            }

    }
	
	wp_send_json($lokaldata);
    //echo json_encode($lokaldata);    
}

if (isset($galleryid)) {
	$gallerylist = array(
		'pictures' => array()
	);
	$query1 = 'SELECT t1.source FROM '.$table_name2.' AS t1, '.$table_name1.' AS t2 WHERE t1.gid = t2.gid AND t2.id = "'.$galleryid.'"';
	$result1 = $wpdb->get_results($query1, ARRAY_A);
	if($result1){
		foreach($result1 as $fe){
			$source = $fe['source'];
			array_push($gallerylist['pictures'], $source);
		}
	} 	
	wp_send_json($gallerylist);
	//echo json_encode($gallerylist);
}

if (array_key_exists('allgalleries', $_REQUEST)) {	
	$galleries = array(
		'klubovi' => array(),
		'splavovi' => array(),
		'kafane' => array(),
		'barovi' => array(),
		'kafei' => array(),
		'restorani' => array(),
		'striptizbarovi' => array()
	);	
	$query1 = 'SELECT t1.id, t1.name, t1.date, t1.photocount, t1.previewpic FROM '.$table_name1.' AS t1, '.$table_name3.' AS t2 WHERE t1.location = t2.post_title AND t2.post_type = "klubovi" ORDER BY date DESC LIMIT 50';
	$result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);
	
	if($result1){
		foreach($result1 as $fe){
			$gallerylist = array(
				'id_gallery' => $fe['id'],
				'name' => $fe['name'],
				'date' => $fe['date'],
				'photocount' => $fe['photocount'],
				'previewpic' => $fe['previewpic']
			);
			array_push($galleries['klubovi'], $gallerylist);
		}
	}
	
	$query1 = 'SELECT t1.id, t1.name, t1.date, t1.photocount, t1.previewpic FROM '.$table_name1.' AS t1, '.$table_name3.' AS t2 WHERE t1.location = t2.post_title AND t2.post_type = "splavovi" ORDER BY date DESC LIMIT 50';
	$result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);
	
	if($result1){
		foreach($result1 as $fe){
			$gallerylist = array(
				'id_gallery' => $fe['id'],
				'name' => $fe['name'],
				'date' => $fe['date'],
				'photocount' => $fe['photocount'],
				'previewpic' => $fe['previewpic']
			);
			array_push($galleries['splavovi'], $gallerylist);
		}
	}
	
	$query1 = 'SELECT t1.id, t1.name, t1.date, t1.photocount, t1.previewpic FROM '.$table_name1.' AS t1, '.$table_name3.' AS t2 WHERE t1.location = t2.post_title AND t2.post_type = "kafane" ORDER BY date DESC LIMIT 50';
	$result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);
	
	if($result1){
		foreach($result1 as $fe){
			$gallerylist = array(
				'id_gallery' => $fe['id'],
				'name' => $fe['name'],
				'date' => $fe['date'],
				'photocount' => $fe['photocount'],
				'previewpic' => $fe['previewpic']
			);
			array_push($galleries['kafane'], $gallerylist);
		}
	}
	
	$query1 = 'SELECT t1.id, t1.name, t1.date, t1.photocount, t1.previewpic FROM '.$table_name1.' AS t1, '.$table_name3.' AS t2 WHERE t1.location = t2.post_title AND t2.post_type = "barovi" ORDER BY date DESC LIMIT 50';
	$result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);
	
	if($result1){
		foreach($result1 as $fe){
			$gallerylist = array(
				'id_gallery' => $fe['id'],
				'name' => $fe['name'],
				'date' => $fe['date'],
				'photocount' => $fe['photocount'],
				'previewpic' => $fe['previewpic']
			);
			array_push($galleries['barovi'], $gallerylist);
		}
	}
	
	$query1 = 'SELECT t1.id, t1.name, t1.date, t1.photocount, t1.previewpic FROM '.$table_name1.' AS t1, '.$table_name3.' AS t2 WHERE t1.location = t2.post_title AND t2.post_type = "kafei" ORDER BY date DESC LIMIT 50';
	$result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);
	
	if($result1){
		foreach($result1 as $fe){
			$gallerylist = array(
				'id_gallery' => $fe['id'],
				'name' => $fe['name'],
				'date' => $fe['date'],
				'photocount' => $fe['photocount'],
				'previewpic' => $fe['previewpic']
			);
			array_push($galleries['kafei'], $gallerylist);
		}
	}
	
	$query1 = 'SELECT t1.id, t1.name, t1.date, t1.photocount, t1.previewpic FROM '.$table_name1.' AS t1, '.$table_name3.' AS t2 WHERE t1.location = t2.post_title AND t2.post_type = "restorani" ORDER BY date DESC LIMIT 50';
	$result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);
	
	if($result1){
		foreach($result1 as $fe){
			$gallerylist = array(
				'id_gallery' => $fe['id'],
				'name' => $fe['name'],
				'date' => $fe['date'],
				'photocount' => $fe['photocount'],
				'previewpic' => $fe['previewpic']
			);
			array_push($galleries['restorani'], $gallerylist);
		}
	}
	
	$query1 = 'SELECT t1.id, t1.name, t1.date, t1.photocount, t1.previewpic FROM '.$table_name1.' AS t1, '.$table_name3.' AS t2 WHERE t1.location = t2.post_title AND t2.post_type = "striptizbarovi" ORDER BY date DESC LIMIT 50';
	$result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);
	
	if($result1){
		foreach($result1 as $fe){
			$gallerylist = array(
				'id_gallery' => $fe['id'],
				'name' => $fe['name'],
				'date' => $fe['date'],
				'photocount' => $fe['photocount'],
				'previewpic' => $fe['previewpic']
			);
			array_push($galleries['striptizbarovi'], $gallerylist);
		}
	}
	
	wp_send_json($galleries);	
//	echo json_encode($galleries);
}