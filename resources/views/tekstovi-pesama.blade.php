{{--
  Template Name: Tekstovi pesama
--}}

@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      @include('partials.breadcrumb')
      <div class="col-12 col-lg-8 custom-mb-big">
        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = [
          'post_type' => 'tekst_pesme',
          'posts_per_page' => 30,
          'orderby' => 'title',
          'order' => 'asc',
          'paged' => $paged
        ];

        $custom_query = new WP_Query($args);
        if ($custom_query->have_posts()) :
        $count = 0;
        while ($custom_query->have_posts()) : $custom_query->the_post();
        $genres = get_the_terms(get_the_ID(), 'zanr');
        $post_image = get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : get_template_directory_uri() . '/assets/images/img_holder.jpg';
        if ($count > 0) : ?>
        <div class="magazin-post custom-flex-row">
          <div class="post-image">
            <a href="{{ get_permalink() }}">
              <img src="" data-srcset="{{ $post_image }}" alt="" class="img-fluid defer">
            </a>
          </div>
          <div class="post-info">
            <a href="{{ get_permalink() }}" class="custom_card_link custom-flex-row">
              <h2 class="post-title">{!! get_the_title() !!}</h2>
              <p class="post-date">{{ get_the_date() }}</p>
            </a>
            @if($genres)
              <a href="{{ get_term_link($genres[0]->term_id) }}" class="button post-category">{!! $genres[0]->name !!}</a>
            @endif
          </div>
        </div>
        <?php else : ?>
        <div class="featured-post">
          <div class="post-image">
            <a href="{{ get_permalink() }}">
              <img src="{{ $post_image }}" alt="" class="img-fluid">
            </a>
          </div>
          <div class="post-info">
            <h2 class="post-title">
              <a href="#">{!! get_the_title() !!}</a>
            </h2>
            <p class="post-date">{{ get_the_date() }}</p>
            @if($genres)
              <div class="post-categories">
                @foreach($genres as $genre)
                  <a href="{{ get_term_link($genre->term_id) }}" class="button post-category">{!! $genre->name !!}</a>
                @endforeach
              </div>
            @endif
          </div>
        </div>
        <?php endif;
        $count++;
        endwhile;
        wp_reset_postdata();
        endif;
        ?>
        @include('partials.pagination')
      </div>
      @include('partials.sidebar')
    </div>
  </div>
  @include('partials.reservation')
  <div class="container">
    <div class="row">
      @include('partials.tekstovi')
      @include('partials.gallery')
      @include('partials.featured')
    </div>
  </div>
@endsection
