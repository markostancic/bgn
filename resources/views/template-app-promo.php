<?php
header('Content-Type: application/json;charset=utf-8');
header("Cache-Control: no-cache, must-revalidate");

define('VERSION', '2.5.3');
/*
Template Name: App Promo
*/

function executeQuery($sql) {
  $servername = "127.0.0.1";
  $username = "root";
  $password = "";
  $dbname = "nocu_app";
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->query("SET NAMES 'utf8'");
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    return $stmt;
}

if (isset($_REQUEST['getPromo'])) {
    $sql = "SELECT * FROM promo WHERE promo_start < NOW()";
    $response = array();
    $response['promo'] = array();
    try {
        $stmt = executeQuery($sql);
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            $promotion = array();
            $promotion['promoDay'] = $row['promo_day'];
            $promotion['placeId'] = $row['place_id'];
            $promotion['startTime'] = $row['start_time'];
            $promotion['endTime'] = $row['end_time'];
            $promotion['desc'] = $row['description'];
            $promotion['uuid'] = $row['uuid'];
            $promotion['message'] = $row['message'];
            //$promotion['promoStart'] = $row['promo_start'];

            $postdata = get_post($promotion['placeId'], ARRAY_A);
            if ($postdata != null) {
                $lokaldata = array();
                $lokaldata['about'] = htmlspecialchars($postdata['post_content']);
                $lokaldata['name'] = htmlspecialchars($postdata['post_title']);
                $lokaldata['url'] = get_permalink($promotion['placeId']);

                $beogradnocu_meta = get_post_meta($promotion['placeId']);
                // adresa
                if (isset($beogradnocu_meta['beogradnocu_ulica'])) {
                    $adresa = $beogradnocu_meta['beogradnocu_ulica']['0'];
                }
                // radnovreme
                if (isset($beogradnocu_meta['beogradnocu_radno_vreme'])) {
                    $radnovreme = $beogradnocu_meta['beogradnocu_radno_vreme']['0'];
                }

                if (isset($beogradnocu_meta['beogradnocu_google_location'])) {
                    $geolocation = $beogradnocu_meta['beogradnocu_google_location']['0'];
                }
                $image = wp_get_attachment_url(get_post_thumbnail_id($promotion['placeId']));
                $lokaldata['geolocation'] = $geolocation;
                $lokaldata['image'] = $image;
                $lokaldata['radnovreme'] = $radnovreme;
                $lokaldata['adresa'] = $adresa;

                $promotion['data'] = $lokaldata;
            }
            array_push($response['promo'], $promotion);
        }
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    echo json_encode($response);
}

if (isset($_REQUEST['getPayout'])) {
    $sql = "SELECT * FROM payout";
    $response = array();
    $response['payout'] = array();
    try {
        $stmt = executeQuery($sql);
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            $promotion = array();
            $promotion['placeId'] = $row['place_id'];
            $promotion['desc'] = $row['description'];
            //$promotion['promoStart'] = $row['promo_start'];

            $postdata = get_post($promotion['placeId'], ARRAY_A);
            if ($postdata != null) {
                $lokaldata = array();
                $lokaldata['about'] = htmlspecialchars($postdata['post_content']);
                $lokaldata['name'] = htmlspecialchars($postdata['post_title']);
                $lokaldata['url'] = get_permalink($promotion['placeId']);

                $beogradnocu_meta = get_post_meta($promotion['placeId']);
                // adresa
                if (isset($beogradnocu_meta['beogradnocu_ulica'])) {
                    $adresa = $beogradnocu_meta['beogradnocu_ulica']['0'];
                }
                // radnovreme
                if (isset($beogradnocu_meta['beogradnocu_radno_vreme'])) {
                    $radnovreme = $beogradnocu_meta['beogradnocu_radno_vreme']['0'];
                }

                if (isset($beogradnocu_meta['beogradnocu_google_location'])) {
                    $geolocation = $beogradnocu_meta['beogradnocu_google_location']['0'];
                }
                $image = wp_get_attachment_url(get_post_thumbnail_id($promotion['placeId']));
                $lokaldata['geolocation'] = $geolocation;
                $lokaldata['image'] = $image;
                $lokaldata['radnovreme'] = $radnovreme;
                $lokaldata['adresa'] = $adresa;

                $promotion['data'] = $lokaldata;
            }
            array_push($response['payout'], $promotion);
        }
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    echo json_encode($response);
}

if (isset($_REQUEST['visitPromo'])) {
    $data = file_get_contents("php://input");

    $user_data = json_decode($data);

    if (empty($user_data->version)) {
        $user_data->version = "old";
    }
    $response = array();
    $response['status'] = "fail";
    $response['version'] = VERSION;
    $response['user_version'] = $user_data->version;
    if (strcmp($user_data->version, VERSION) == 0) {
        // Distance check
        if (isset($user_data->lat1) && isset($user_data->lat2) && isset($user_data->lon1) && isset($user_data->lon2)) {
            $distance = distance($user_data->lat1, $user_data->lon1, $user_data->lat2, $user_data->lon2, 'K');
            $response['distance'] = $distance;
            if ($distance < 0.02) {
                try {
                    $sql = "SELECT * FROM promo WHERE uuid = '". $user_data->event ."' AND TIME(NOW()) >= start_time AND TIME(NOW()) < end_time";
                    $stmt = executeQuery($sql);
                    $result = $stmt->fetch();
                    if ($result) {
                        $sql = "INSERT INTO points (points, uuid, changed_at, type, event_uuid) VALUES (50, '". $user_data->uuid. "', NOW(), 0, '". $user_data->event ."')";
                        executeQuery($sql);
                        $response['status'] = "success";
                        $sql = "SELECT SUM(points) AS score FROM points WHERE uuid='". $user_data->uuid ."'";
                        $stmt = executeQuery($sql);
                        $result = $stmt->fetch();
                        $sql = "UPDATE users SET score='". $result['score'] ."' WHERE uuid='". $user_data->uuid ."'";
                        executeQuery($sql);
                    } else {
                        $response['status'] = "fail";
                        $response['message'] = "Ne postoji check-in!";
                    }
                } catch (PDOException $e) {
                    $response['status'] = "fail";
                    $response['message'] = "Možete se chekirati samo jednom dnevno!";
                }
            } else {
                $response['status'] = "fail";
                $response['message'] = "Greška u lokaciji, proverite vaša podešavanja i pokušajte ponovo!";
            }
        } else {
            $response['status'] = "fail";
            $response['message'] = "Greška nemate lokaciju, proverite vaša podešavanja i pokušajte ponovo!";
        }
    }
    echo json_encode($response);
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {
  if (($lat1 == $lat2) && ($lon1 == $lon2)) {
    return 0;
  } else {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
      return ($miles * 1.609344);
    } elseif ($unit == "N") {
      return ($miles * 0.8684);
    } else {
      return $miles;
    }
  }
}
