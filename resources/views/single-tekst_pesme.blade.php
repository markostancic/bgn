@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container single-magazine">
    <div class="row">
      @include('partials.breadcrumb')
      <div class="col-12 col-lg-8">
        <div class="hero magazine custom-flex-row">
          <div class="hero-text">
            <h1>{!! get_the_title() !!}</h1>
            <?php $artist = get_the_terms(get_the_ID(), 'izvodjaci'); ?>
            @if($artist)
              @foreach($artist as $artist)
                <a href="{{ get_term_link($artist->term_id) }}"
                   class="text_category post-category">{!! $artist->name !!}</a>
              @endforeach
            @endif
            <?php $genres = get_the_terms(get_the_ID(), 'zanr'); ?>
            @if($genres)
              @foreach($genres as $genre)
                <a href="{{ get_term_link($genre->term_id) }}"
                   class="text_category post-category">{!! $genre->name !!}</a>
              @endforeach
            @endif
            @php(wp_reset_postdata())
          </div>
        </div>
        <div class="post-content main-content custom-mb-big">
          <?php the_content(); ?>
        </div>
        @include('partials.tekstovi')
        @include('partials.tekstovi_copyright')
      </div>
      @include('partials.sidebar')
    </div>
  </div>
  @endwhile
@endsection
