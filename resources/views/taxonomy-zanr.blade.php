@extends('layouts.app')

@section('content')
  <div class="container single-magazine">
    <div class="row">
      @include('partials.breadcrumb')
      <div class="col-12 col-lg-8">
        <div class="similar_songs">
          <?php  $term = get_queried_object()->term_id;
          $term_name = get_term( $term )->name;
          $term_description = term_description( $term );
          ?>
          <div class="hero-text">
            <h1>{{ $term_name }} tekstovi pesama</h1>
          </div>
          <div class="description">
            <?php echo $term_description ?>
          </div>
        </div>
        <div class="custom_cards related-text custom-flex-row custom-mb-xs">
          <div class="related_songs p-0 row">

            @php $args = array(
    'post_type' => 'tekst_pesme',
    'posts_per_page' => -1,
     'orderby' => 'title',
    'order'   => 'ASC',
    'tax_query' => array(
        array (
            'taxonomy' => 'zanr',
            'field' => 'term_id',
            'terms' => $term,
        )
    ),
                 ); @endphp

            @php $query = new WP_Query( $args ); @endphp
            @if ( $query->have_posts() )
              @while ( $query->have_posts() )
                @php $query->the_post(); @endphp
                <div class="song col-lg-12 p-0">
                  <div class="song_content">
                    <h3 class="title"><a href="{{ the_permalink() }}">{{ the_title() }}</a></h3>
                    <span class="song_author">
              <?php
                      $terms = get_the_terms( $query->ID, 'izvodjaci' );
                      foreach ( $terms as $term ) { ?>
                <a href="{{ get_term_link($term) }}"><?php  echo $term->name; ?></a>
          <?php    } ?>
            </span>
                  </div>
                </div>
              @endwhile
            @endif
            @php wp_reset_postdata(); @endphp
          </div>
        </div>
        @include('partials.tekstovi_copyright')
      </div>
      @include('partials.sidebar')
    </div>
  </div>
@endsection
