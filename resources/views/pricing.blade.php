{{--
  Template Name: Cenovnik
--}}

@extends('layouts.app')

@section('content')
  <section class="pricing" style="background-image: url({{ get_the_post_thumbnail_url() }})">
    <div class="container">
      <div class="row">
        @include('partials.breadcrumb')
        <div class="col-lg-8 pricing-wrapper">
          <h1><?php the_title() ?></h1>
          <div class="description content_extra">
          @include('partials.content-page')
          </div>
          @if( have_rows('cenovnici') )
            @while ( have_rows('cenovnici') ) @php(the_row())
            <div class="pricing-list">
              <h2><?php the_sub_field('lokal'); ?></h2>
              <?php $price_lists = get_sub_field('cenovnik'); ?>
              @if($price_lists)
                <ul>
                  @foreach($price_lists as $price_list)
                    <li>
                      <span class="name">{{ $price_list['naziv'] }}@if($price_list['podnaslov']) <span class="sub-name">{{ $price_list['podnaslov'] }}</span> @endif</span>
                      <span class="price">{{ $price_list['cena'] }} <span class="sub-name">din</span></span>
                    </li>
                  @endforeach
                  @php(wp_reset_postdata())
                </ul>
              @endif
            </div>
            @endwhile
          @endif
          <?php $fluff = get_field('dodatan_opis'); ?>
          @if($fluff)
            <div class="description">
              {!! $fluff !!}
            </div>
            @endif
        </div>
        @include('partials.sidebar')
      </div>
    </div>
  </section>
  @include('partials.reservation')
  <div class="container">
    <div class="row">
      @include('partials.featured')
    </div>
  </div>
@endsection
