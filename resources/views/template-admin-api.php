<?php	

/*
Template Name: App Admin Api
*/
	
$servername = "localhost";
$username = "beogradnocu";
$password = "kdGr74!@ddklF";
$dbname = "beogradnocu_apps";	
	
if (isset($_REQUEST['users'])) {	
	$response = '{"message":"N/A"}';
	$data = array();		
	$data['users'] = array();
	
	$sql = "SELECT * FROM users";						
	try {		
		$conn = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->prepare($sql); 		
		$stmt->execute();				
		$rows = $stmt->fetchAll();				
		foreach ($rows as $row) {					
			$user = array();						
			$user['name'] = $row['name'];						
			$user['score'] = $row['score'];				
			$user['mobile'] = $row['mobile'];
			$user['created_at'] = $row['created_at'];
			$user['birthday'] = $row['birthday'];
			$user['uuid'] = $row['uuid'];
			$user['r'] = $row['r'];
			$user['r10k'] = $row['r10k'];
			$user['r20k'] = $row['r20k'];
			$user['r30k'] = $row['r30k'];
			$user['r50k'] = $row['r50k'];
			$user['r100k'] = $row['r100k'];
			$user['invites'] = $row['invites'];
			$user['checkins'] = $row['checkins'];
			$user['bills'] = $row['bills'];						
			if(empty($row['q'])) {
				$user['q'] = null;
			} else {
				$user['q'] = json_decode($row['q']);
			}										
			array_push($data['users'], $user);						
		}							
		$response = json_encode($data);				
	}
	catch(Exception $e) {		
		$response = '{"message":"error"}';
	}				
	echo $response;
}	

if (isset($_REQUEST['deleteUser'])) {
	$data = file_get_contents("php://input");
	
	$user_data = json_decode($data);
	
	$response = '{"message":"N/A"}';
	
	$sql = "DELETE FROM users WHERE uuid ='".$user_data->uuid."'";						
	try {
		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->prepare($sql); 
		$stmt->execute();
	
		$response = '{"message":"success"}';
	}
	catch(PDOException $e) {
		$response = $e->message;
	}
	echo $response;
}

if (isset($_REQUEST['updateUser'])) {
	$data = file_get_contents("php://input");
	
	$user_data = json_decode($data);
	
	$response = '{"message":"N/A"}';
	
	if(!empty($user_data->uuid)) {
		$sql = "UPDATE users SET name='".$user_data->name."', mobile='".$user_data->mobile."' WHERE uuid='". $user_data->uuid ."'";			
		try {
			$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $conn->prepare($sql); 
			$stmt->execute();		
			$response = '{"message":"success"}';
		}
		catch(PDOException $e) {
			$response = '{"message":"error"}';
		}
	}
	echo $response;
}

if (isset($_REQUEST['getUserPoints'])) {
	$data = file_get_contents("php://input");
	
	$user_data = json_decode($data);
	
	$response = '{"message":"N/A"}';
	
	if(!empty($user_data->uuid)) {
		$sql = "SELECT * FROM points WHERE uuid='". $user_data->uuid ."' ORDER BY changed_at DESC";		
		try {
			$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $conn->prepare($sql); 
			$stmt->execute();		
			$data = array(
				'scores' => array()
			);
			$rows = $stmt->fetchAll();		
			foreach ($rows as $row) {												
				$score = array();						
				$score['points'] = $row['points'];						
				$score['type'] = $row['type'];				
				$score['changedAt'] = $row['changed_at'];				
				array_push($data['scores'], $score);	
			}
			$response = json_encode($data);
		}
		catch(PDOException $e) {
			$response = '{"message":"error"}';
		}
	}
	echo $response;
}

function setScore($user_uuid) {
	try {					
		// Rezervacije
		$sql = "SELECT COUNT(*) AS r FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 100";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r=". $result['r'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Rezervacije preko 10k
		$sql = "SELECT COUNT(*) AS r10k FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 102";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r10k=". $result['r10k'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Rezervacije preko 20k
		$sql = "SELECT COUNT(*) AS r20k FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 105";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r20k=". $result['r20k'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Rezervacije preko 30k
		$sql = "SELECT COUNT(*) AS r30k FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 103";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r30k=". $result['r30k'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Rezervacije preko 50k
		$sql = "SELECT COUNT(*) AS r50k FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 106";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r50k=". $result['r50k'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Rezervacije preko 100k
		$sql = "SELECT COUNT(*) AS r100k FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 104";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r100k=". $result['r100k'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Invites
		$sql = "SELECT COUNT(*) AS invites FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 10";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET invites=". $result['invites'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Bills
		$sql = "SELECT COUNT(*) AS bills FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 101";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET bills=". $result['bills'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Checkins
		$sql = "SELECT COUNT(*) AS checkins FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 0";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET checkins=". $result['checkins'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Score
		$sql = "SELECT SUM(points) AS score FROM points WHERE uuid='". $user_uuid ."'";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET score=". $result['score'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
	}
	catch(PDOException $e) {		
		echo $e->message;
	}
}
		
function push($uuid, $points, $pointType) {
	$url = "https://fcm.googleapis.com/fcm/send";
	
	$sql = "SELECT * FROM users WHERE uuid='". $uuid ."'";
	$stmt = executeQuery($sql);
	$result = $stmt->fetch();	
	$deviceid = $result['deviceid'];
	
	$token = $deviceid;
	$serverKey = 'AAAAb8NuIPk:APA91bFAcnmU8KtimKNSuMVoPyVzLuR3wRsJoQdWBOY_7SswXprvPlzn9jTX5WR58G24vLwWIYGPWcQgzRecqO4n3WKlmrmNLcje4M1bfJ5NuxgQgUEtzB583eTwLWgqBtkuFrRrGqTgD4TOfl1LyONi4CGJCtRzHg';
	$title = "Beograd Noću! Osvojili ste poene!";
	$body = "Čestitamo, dobili ste ".$points." poena zbog: ";

	if($pointType == 100) {
		$body = $body ."realizovana rezervacija";
	} else if($pointType == 101) {
		$body = $body ."poslat račun";
	} else if($pointType == 102) {
		$body = $body ."realizovana rezervacija preko 10 hiljada dinara";
	} else if($pointType == 103) {
		$body = $body ."realizovana rezervacija preko 30 hiljada dinara";
	} else if($pointType == 104) {
		$body = $body ."realizovana rezervacija preko 100 hiljada dinara";
	} else if($pointType == 105) {
		$body = $body ."realizovana rezervacija preko 20 hiljada dinara";
	} else if($pointType == 106) {
		$body = $body ."realizovana rezervacija preko 50 hiljada dinara";
	} else if($pointType == 0) {
		$body = $body ."uspešno čekiranje";
	} else if($pointType == 1) {
		$body = $body ."instalacija aplikacije";
	} else if($pointType == 10) {
		$body = $body ."poziv prijatelja";
	} else if($pointType == 200) {
		$title = "Beograd Noću! Unovčili ste poene!";
		$points = -abs($points);
		$body = "Unovčili ste poene ".abs($points)." poena";
	} else if($pointType == 201) {
		$body = $body ."za desetu rezervaciju po redu";
	} else if($pointType == 202) {
		$body = $body ."za dvadesetu rezervaciju po redu";
	}			
	
	$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
	$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');
	$json = json_encode($arrayToSend);
	echo $json;
	$headers = array();
	$headers[] = 'Content-Type: application/json';
	$headers[] = 'Authorization: key='. $serverKey;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	//Send the request
	$response = curl_exec($ch);
	//Close request
	if ($response === FALSE) {
		die('FCM Send Error: ' . curl_error($ch));
	}
	curl_close($ch);
}
	
function executeQuery($sql) {
	$servername = "localhost";
	$username = "beogradnocu";
	$password = "kdGr74!@ddklF";
	$dbname = "beogradnocu_apps";
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn->query("SET NAMES 'utf8'");
	$stmt = $conn->prepare($sql); 
	$stmt->execute();
	return $stmt;
}									

if (isset($_REQUEST['addPointsToUser'])) {
	$data = file_get_contents("php://input");
	
	$user_data = json_decode($data);
	
	$response = '{"message":"N/A"}';
	
	if(!empty($user_data->uuid)) {
		try {				
			$sql = "INSERT INTO points (points, uuid, changed_at, type, event_uuid) VALUES ('". $user_data->points ."', '". $user_data->uuid. "', NOW(), ".$user_data->type.", UUID())";																
			executeQuery($sql);
			setScore($user_data->uuid);
			push($user_data->uuid, $user_data->points, $user_data->type);
			$response = '{"message":"success"}';
		}					
		catch(PDOException $e) {
			$response = $e->getMessage();
		}
	}
	echo $response;
}
	
?>