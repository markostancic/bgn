@extends('layouts.app')

@section('content')
  <div class="container single-magazine">
    <div class="row">
      @include('partials.breadcrumb')
      <div class="col-12 col-lg-8">
        <?php $category = get_the_category()[0];
        $post_image = get_the_post_thumbnail_url(get_the_ID(), 'single') ? get_the_post_thumbnail_url(get_the_ID(), 'single') : get_template_directory_uri() . '/assets/images/img_holder.jpg';
        ?>
        @if($category->slug == 'magazin')
          <div class="hero magazine custom-flex-row">
            <div class="hero-text">
              <h1>{!! get_the_title() !!}</h1>
              <p class="post-date">{{ get_the_date() }}</p>
              @if($category)
                <a href="{{ get_category_link($category->term_id) }}"
                   class="button post-category">{!! $category->name !!}</a>
              @endif
            </div>
            <div class="hero-image">
              <picture>
                <?php get_webp_image_type([$post_image => ''], '', 1); ?>
                <img src="{{ $post_image }}" alt="" class="img-fluid">
              </picture>
            </div>
          </div>
        @else
          <div class="hero">
            <div class="hero-image">
              <picture>
                <?php
                $post_image_single = get_the_post_thumbnail_url(get_the_ID(), 'single_post') ? get_the_post_thumbnail_url(get_the_ID(), 'single_post') : get_template_directory_uri() . '/assets/images/img_holder.jpg';
                ?>
                <img src="{{ $post_image_single }}" alt="" class="img-fluid">
              </picture>
            </div>
            <div class="hero-text single_text-content_style custom-mb-xs">
              <h1>{!! get_the_title() !!}</h1>
              @if($category)
                <a href="{{ get_category_link($category->term_id) }}"
                   class="gray-outline">{!! $category->name !!}</a>
              @endif
              <span class="post-date">{{ get_the_date() }}</span>
            </div>
          </div>
        @endif
        <div class="post-content main-content custom-mb-big">
          @include('partials.content-page')
        </div>
      </div>
      @include('partials.sidebar')
    </div>
  </div>
  @include('partials.reservation')
  <div class="container">
    <div class="row">
      @include('partials.povezani_tekstovi')
      @include('partials.gallery')
      @include('partials.featured')
    </div>
  </div>
@endsection
