{{--
  Template Name: Magazin grupna
--}}

@extends('layouts.app')

@section('content')
  <section class="magazin">
    <div class="container">
      <div class="row">
        @include('partials.breadcrumb')
        <div class="col-12 col-lg-8 custom-flex-row main-col">
          <h1>{{ the_title() }}</h1>
          <?php
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          $args = [
            'post_type'      => 'post',
            'posts_per_page' => 20,
            'paged'          => $paged,
          ];
          $counter = 0;
          $posts = new WP_Query( $args );
          ?>
          @if ( $posts->have_posts() )
            <ul class="posts">
              @while ( $posts->have_posts() )
                <?php $posts->the_post(); ?>
                @if($counter === 0)
                  <li class="big-post custom-mb-xs">
                    <a href="{{ get_the_permalink($posts->ID) }}">
                      @if(has_post_thumbnail($posts->ID))
                        <div class="image">
                          <picture>
                            <?php $image = get_the_post_thumbnail_url( $posts->ID );
                            get_webp_image_type( [ $image => '' ], '', 1 );
                            ?>
                            <img src="{{ $image }}" alt="">
                          </picture>
                        </div>
                      @endif
                      <h2 class="first_post">{!! get_the_title($posts->ID) !!}</h2>
                    </a>
                    <?php
                    $categories = get_the_category(); ?>
                    @if($categories)
                        @foreach($categories as $category)
                          <?php
                          $name = $category->name;
                          $category_link = get_category_link( $category->term_id ); ?>
                            <a href="{{ $category_link }}" class="gray-outline">
                              {!! $name !!}
                            </a>
                        @endforeach
                    @endif
                    <span class="date">{!! get_the_date() !!}</span>
                    @php(wp_reset_postdata())
                  </li>
                @else
                  <li>
                    <a href="{{ get_the_permalink($posts->ID) }}">
                      @if(has_post_thumbnail($posts->ID))
                        <div class="image">
                          <picture>
                            <?php
                            $images = [
                              get_the_post_thumbnail_url( $posts->ID, 'magazin_small' )  => " 255w, ",
                              get_the_post_thumbnail_url( $posts->ID, 'magazin_medium' ) => " 580w"
                            ];
                            $image = get_the_post_thumbnail_url( $posts->ID, 'magazin_small' );

                            $sizes = [
                              "(min-width: 1200px)",
                              "(max-width: 1199px)"
                            ];
                            get_webp_image_type( $images, $sizes );
                            ?>
                            <img src=""
                                 data-srcset="@foreach ( $images as $image => $size ) {!! $image . $size !!} @endforeach"
                                 sizes="(max-width: 1199px) 580px,
                                                                (min-width: 1200px) 255px" alt="">
                          </picture>
                        </div>
                    </a>
                    <div class="post-data">
                      <a href="{{ get_the_permalink($posts->ID) }}">
                        <h2>{!! get_the_title($posts->ID) !!}</h2>
                      </a>
                      <div class="info">
                        <?php
                        $category = get_the_category( $posts->ID )[0];
                        ?>
                        @if($category)
                          <a href="{!! get_term_link($category->term_id) !!}"
                             class="gray-outline">{!! $category->name !!}</a>
                        @endif
                        <span class="date">{!! get_the_date() !!}</span>
                      </div>
                      <a href="{{ get_the_permalink($posts->ID) }}" class="arrow-link"></a>
                    </div>
                    @endif
                  </li>
                @endif
                <?php $counter ++; ?>
              @endwhile
            </ul>
          @endif
          <?php
          $pagination = paginate_links(
            [
              'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
              'total'        => $posts->max_num_pages,
              'current'      => max( 1, get_query_var( 'paged' ) ),
              'format'       => '?paged=%#%',
              'show_all'     => false,
              'type'         => 'plain',
              'end_size'     => 1,
              'mid_size'     => 1,
              'prev_next'    => true,
              'prev_text'    => sprintf( '<i></i> %1$s', __( '', 'sage' ) ),
              'next_text'    => sprintf( '%1$s <i></i>', __( '', 'sage' ) ),
              'add_args'     => false,
              'add_fragment' => '',
            ] ); ?>
          @if($pagination)
            <nav class="navigation pagination">
              <div class="nav-links">
                {!! $pagination !!}
              </div>
            </nav>
          @endif
        </div>
        @include('partials.sidebar')
      </div>
    </div>
    @include('partials.reservation')
    <div class="container">
      <div class="row">
        @include('partials.featured-post')
        @include('partials.gallery')
        @include('partials.featured')
      </div>
    </div>
  </section>
@endsection
