@extends('layouts.app')

@section('content')
  <div class="hero_front">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-9">
          <div class="nightout">
            <div class="nightout_heading custom-flex-row">
              <div class="nightout_heading_main">
                <p class="custom-heading-2">Gde izaći večeras - izlasci Beograd</p>
                <?php
                $today = date('d.m.Y.');
                $today_format = date('d', strtotime($today)) . '. ' . month(date('n', strtotime($today))).' '.date('Y.');
                $post_types = array('kafane_beograd','klubovi_beograd','splavovi_beograd','restorani_beograd','barovi_beograd')
                ?>
                <p class="nightout_date">{{ $today_format  }}</p>
              </div>
              <a href="#" class="button yellow">Rezerviši</a>
            </div>
            <div class="nightout_content_container">
              <button type="button" class="nightout_toggler" data-target="#nightout_content" data-amount="100"
                      data-scroll="down"></button>
              <div class="nightout_content custom-flex-row" id="nightout_content">
                @php $args = array(
                      'post_type'=>$post_types,
                      'posts_per_page'=>-1,
                      ); @endphp

                @php $query = new WP_Query( $args ); @endphp
                @if ( $query->have_posts() )
                  @while ( $query->have_posts() )
                    @php $query->the_post(); @endphp
                    <?php
                    $thumbnail = get_the_post_thumbnail_url($query->ID, 'lokali_sidebar');
                    $najava_thumb = get_field('slika_za_program_lokala');
                    $thumb = $najava_thumb['sizes'][ 'lokali_sidebar' ];
                    $permalink = get_permalink();
                    if (have_rows('program')) :
                    while (have_rows('program')) : the_row();
                    $tekst_najave = get_sub_field('tekst_programa');
                    $date = get_sub_field('datum');
                    $today = date('d.m.Y.');
                    $nameOfDay = date('D', strtotime($date));
                    $nameOfMonth = date("F", strtotime($date));
                    $numDay = date('d', strtotime($date));
                    $loc_day = day($nameOfDay);
                    ?>
                    @if($date == $today)
                      <div class="nightout_item custom-flex-row">
                        <div class="nightout_image">
                          <img src="<?php echo empty($thumb) ? $thumbnail : $thumb;
                          ?>" alt="" class="img-fluid">
                        </div>
                        <div class="nightout_text">
                          <h2 class="nightout_title">{{ the_title()  }}</h2>
                          <p class="custom-heading-3">{{ $tekst_najave  }}</p>
                        </div>
                        <a href="{{ $permalink  }}" class="button yellow">Rezerviši</a>
                      </div>
                    @endif
                    <?php endwhile; endif; ?>
                  @endwhile
                @endif
                @php wp_reset_postdata(); @endphp
              </div>
              <button type="button" class="nightout_toggler" data-target="#nightout_content" data-amount="100"
                      data-scroll="up"></button>
            </div>
          </div>
        </div>
        <aside class="col-12 col-lg-3 homepage_sidebar">
          <?php $sidebar_posts = get_field('sidebar_stavke');
          if ($sidebar_posts) :
          foreach ($sidebar_posts as $sidebar_post) :
          ?>
          <div class="homepage_sidebar_item">
            <img src="{{ get_the_post_thumbnail_url($sidebar_post) }}" alt="" class="img-fluid">
            <a href="{{ get_permalink($sidebar_post) }}">{{ get_the_title($sidebar_post) }}</a>
          </div>
          <?php
          endforeach;
          endif; ?>
        </aside>
        <div class="col-12">
          <h1>Beograd noću - rezervacije klubova, kafana, splavova - preporuke za noćni život u Beogradu</h1>
        </div>
      </div>
    </div>
    @include('partials.modal')
  </div>
  @include('partials.reservation')
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-8">
        <div class="date_picker_filter custom-mb-big">
          <p class="custom-heading-3">Program za noćni život u Beogradu - maj 2019.</p>
          <div class="date_picker_date custom-flex-row" style="display: none;">
            <button type="button" class="button active">Pet 24.</button>
            <button type="button" class="button">Sub 25.</button>
            <button type="button" class="button">Ned 26.</button>
            <button type="button" class="button">Pon 27.</button>
            <button type="button" class="button">Uto 28.</button>
            <button type="button" class="button">Sre 29.</button>
            <button type="button" class="button">Čet 30.</button>
          </div>
          @include('partials.lokal-list-date')
          <div class="date_picker_type_lokal">
            <p class="custom-heading-3">Izaberite provod po vašoj želji</p>
            <div class="date_picker_type_lokal_radio custom-flex-row">
              <div class="custom_radio_container">
                <input type="radio" name="lokal" id="dinner" class="custom-radio" value="">
                <label for="dinner">Muzika sa večerom</label>
              </div>
              <div class="custom_radio_container">
                <input type="radio" name="lokal" id="clubbing" class="custom-radio" value="">
                <label for="clubbing">Clubbing</label>
              </div>
              <div class="custom_radio_container">
                <input type="radio" name="lokal" id="kafane" class="custom-radio" value="">
                <label for="kafane">Kafane</label>
              </div>
              <div class="custom_radio_container">
                <input type="radio" name="lokal" id="restaurants" class="custom-radio" value="">
                <label for="restaurants">Restorani</label>
              </div>
              <div class="custom_radio_container">
                <input type="radio" name="lokal" id="plays" class="custom-radio" value="">
                <label for="plays">Svirke</label>
              </div>
            </div>
          </div>
          <div class="date_picker_advanced_search">
            <p class="custom-heading-3" data-open="#date_picker_advanced_search_boxes" data-function="class">Detaljna
              pretraga</p>
            <div class="date_picker_boxes custom-flex-row hide" id="date_picker_advanced_search_boxes">
              <div class="column">
                <p class="custom-heading-4">Muzika</p>
                <div class="custom_checkbox_container">
                  <input type="checkbox" id="live" class="custom_checkbox">
                  <label for="live">Bend uživo</label>
                </div>
                <div class="custom_checkbox_container">
                  <input type="checkbox" id="ninety" class="custom_checkbox">
                  <label for="ninety">Devedesete</label>
                </div>
                <div class="custom_checkbox_container">
                  <input type="checkbox" id="dj" class="custom_checkbox">
                  <label for="dj">DJ</label>
                </div>
                <div class="custom_checkbox_container">
                  <input type="checkbox" id="house" class="custom_checkbox">
                  <label for="house">House</label>
                </div>
                <div class="custom_checkbox_container">
                  <input type="checkbox" id="jazz" class="custom_checkbox">
                  <label for="jazz">Jazz</label>
                </div>
                <div class="custom_checkbox_container">
                  <input type="checkbox" id="folk" class="custom_checkbox">
                  <label for="folk">Narodna</label>
                </div>
                <div class="custom_checkbox_container">
                  <input type="checkbox" id="pop" class="custom_checkbox">
                  <label for="pop">Pop</label>
                </div>
              </div>
              <div class="column custom-flex-row">
                <p class="custom-heading-4">Deo grada</p>
                <?php
                $terms = get_terms([
                  'taxonomy'   => 'deo_grada',
                  'hide_empty' => true,
                ]);
                foreach ($terms as $term) { ?>

                <div class="custom_checkbox_container">
                  <input type="checkbox" id="{{ $term->term_id  }}" class="custom_checkbox">
                  <label for="{{ $term->term_id  }}">{{ $term->name  }}</label>
                </div>
                <?php  } ?>
              </div>
              <span class="loader">
                                <svg class="spinner">
                                    <circle class="path" cx="75" cy="75" r="55" fill="none" stroke-width="20"></circle>
                                </svg>
                            </span>
              <button type="button" class="close" data-close="#date_picker_advanced_search_boxes" data-function="class">
                Zatvori
              </button>
            </div>
          </div>
          <div class="date_picker_filter_content lokali_cards custom-flex-row">
            <div class="lokali_cards custom-flex-row">
              @php
                $post_types = array('kafane_beograd','klubovi_beograd','splavovi_beograd','restorani_beograd','barovi_beograd');
              @endphp
              @php $args = [
   'post_type' => $post_types,
        'status' => 'published',
        'posts_per_page' => -1,
        'meta_query' => [
       [
        'key' => 'program_%_datum',
        'value' => '20200421',
        'compare' => '=',
           'type' => 'numeric'
       ]
    ]
        ]; @endphp
              @php $query = new WP_Query( $args ); @endphp
              @if ( $query->have_posts() )
                @while ( $query->have_posts() )
                  @php $query->the_post(); @endphp
                  @php
                    $title = get_the_title();
                    $telefon = get_field('telefon_kod','option');
                    $telefon_view = get_field('telefon_prikaz','option');
                  @endphp
                  <div class="lokali_card">
                    <div class="lokali_card_image">
                      <a href="<?php the_permalink()  ?>">
                        <?php if (has_post_thumbnail()) : ?>
                        <picture>
                          <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'lokali');

                          ?>
                          <img src="{{ $featured_img_url  }}" alt="" class="img-fluid">
                        </picture>
                        <?php endif; ?>
                      </a>
                    </div>
                    <div class="lokali_card_text">
                      <a href="<?php the_permalink() ?>">
                        <h2 class="lokali_card_heading">{{ $title  }}</h2>
                        <p class="info"><?php _e('Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h', 'beogradnocu'); ?></p>
                      </a>
                      <a href="#" class="button yellow"><?php _e('Rezerviši online', 'beogradnocu'); ?></a>
                      @if($telefon)
                        <a href="tel:{{ $telefon }}" class="button blue">
                          <img src="@asset('images/beograd_nocu__general_header_rezervacije.svg')" alt=""
                               class="img-fluid">{{ $telefon_view }}</a>
                      @endif
                    </div>
                  </div>
                @endwhile
              @endif
              @php wp_reset_postdata(); @endphp
            </div>

          </div>
          <p class="more_lokal custom-flex-row">Još mesta gde možete izaći</p>
          @include('partials.lokali-cards')
          <div class="main-content custom-mb-big">
            {!! get_the_content() !!}
          </div>
        </div>
        @include('partials.sidebar')
      </div>
    </div>
    @include('partials.reservation')
    <div class="container">
      <div class="row">
        @include('partials.featured')
      </div>
    </div>
@endsection
