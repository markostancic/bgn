{{--
  Template Name: Nova godina single
--}}

@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			@include('partials.breadcrumb')
			<div class="col-12 col-lg-8">
				<div class="row new-year-header">
					<h1 class="col-12">Klub Gotik doček nove godine</h1>
					<div class="col-12 col-lg-6 new-year__image">
						<img width="424" height="306" src="https://www.beogradnocu.com/wp-content/uploads/2019/09/Docek-Nove-godine-Beograd-2020-Klub-Gotik-baner-1.jpg"
						     class="attachment-beogradnocu_thumb_club_logo wp-post-image" alt="Docek Nove godine Beograd 2020 Klub Gotik baner"
						     srcset="https://www.beogradnocu.com/wp-content/uploads/2019/09/Docek-Nove-godine-Beograd-2020-Klub-Gotik-baner-1.jpg 424w,
						     https://www.beogradnocu.com/wp-content/uploads/2019/09/Docek-Nove-godine-Beograd-2020-Klub-Gotik-baner-1-300x217.jpg 300w,
						     https://www.beogradnocu.com/wp-content/uploads/2019/09/Docek-Nove-godine-Beograd-2020-Klub-Gotik-baner-1-215x156.jpg 215w"
						     sizes="(max-width: 424px) 100vw, 424px">
					</div>
					<div class="col-12 col-lg-6 new-year__features">
						<div class="feature">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 25" width="17" height="25">
								<path fill="#0e3351" d="M8.5,0A8.41,8.41,0,0,0,0,8.29c0,3.4,4.7,11.76,7.18,16a1.53,1.53,0,0,0,2.63,0C12.3,20.05,17,11.69,17,8.29A8.41,8.41,0,0,0,8.5,0Zm0,13.85A5.38,5.38,0,0,1,3.06,8.54,5.23,5.23,0,0,1,4.65,4.78a5.56,5.56,0,0,1,7.7,0,5.23,5.23,0,0,1,1.59,3.76A5.38,5.38,0,0,1,8.5,13.85Z"/>
							</svg>
							<span>Karađorđeva 2-4</span>
						</div>
						<div class="feature">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25" width="25" height="25">
								<path fill="#0e3351" d="M21.11,12.5A1.11,1.11,0,0,1,20,13.61H12.5a1.11,1.11,0,0,1-1.11-1.11V5a1.11,1.11,0,1,1,2.22,0v6.39H20A1.11,1.11,0,0,1,21.11,12.5ZM12.5,22.78a1.11,1.11,0,1,0,1.11,1.11A1.11,1.11,0,0,0,12.5,22.78ZM0,12.5a1.11,1.11,0,1,0,1.11-1.11A1.11,1.11,0,0,0,0,12.5Zm25,0h0A12.5,12.5,0,0,0,12.5,0a1.11,1.11,0,1,0,0,2.22h0A10.28,10.28,0,0,1,22.78,12.5h0a1.11,1.11,0,0,0,2.22,0ZM6.25,1.67a1.11,1.11,0,1,0,1.52.41A1.12,1.12,0,0,0,6.25,1.67ZM17.64,21.4a1.11,1.11,0,1,0,1.52.41A1.12,1.12,0,0,0,17.64,21.4Zm-16-2.65a1.11,1.11,0,1,0,.41-1.52A1.12,1.12,0,0,0,1.67,18.75Zm0-12.5a1.11,1.11,0,1,0,1.52-.41A1.12,1.12,0,0,0,1.67,6.25ZM21.4,17.64a1.11,1.11,0,1,0,1.52-.41A1.11,1.11,0,0,0,21.4,17.64ZM6.25,23.33a1.11,1.11,0,1,0-.41-1.52A1.12,1.12,0,0,0,6.25,23.33Z"/>
							</svg>
							<span>22:00h - 04:00h</span>
						</div>
						<div class="feature">
							<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25.3px" height="25px" viewBox="0 0 25.3 25">
								<path fill="#0E3351" d="M19.3,13.8V1.4c0-0.4-0.2-0.7-0.4-1C18.6,0.1,18.2,0,17.8,0L6.1,1.5C5.4,1.6,4.9,2.2,4.9,2.9V13c-0.5-0.2-1-0.3-1.6-0.3c-1.9,0-3.4,1.2-3.4,2.7s1.5,2.8,3.4,2.8s3.4-1.2,3.4-2.8V6.7l10.8-1.4v6.1c-0.5-0.2-1-0.3-1.5-0.3c-1.9,0-3.4,1.2-3.4,2.7s1.5,2.7,3.4,2.7C17.6,16.7,19.1,15.5,19.3,13.8C19.3,13.8,19.3,13.8,19.3,13.8z M24.4,8.8c-0.5,0-0.9,0.4-0.9,0.9v10.1c-0.5-0.2-1-0.3-1.5-0.3c-1.9,0-3.4,1.2-3.4,2.8S20,25,21.9,25s3.4-1.2,3.4-2.7V9.7C25.2,9.2,24.8,8.8,24.4,8.8z"/>
							</svg>
							<span>Maya Berović</span>
						</div>
						<div class="feature">
							<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25.9px" height="25px" viewBox="0 0 25.9 25">
								<path fill="#0E3351" d="M6.8,9.5l1,0.2l0.3,4.4c0,0.4,0.3,0.6,0.7,0.6c0,0,0,0,0,0h2c0.4,0,0.6-0.3,0.7-0.6l0.3-4.4l1.1-0.2C13,9.4,13.1,9.2,13.1,9l-0.3-1.8c-0.1-1.2-1.1-2-2.3-2H9c-1.2,0-2.2,0.8-2.3,2L6.4,9C6.4,9.2,6.6,9.5,6.8,9.5z M9.8,4.3c1.2,0,2.2-1,2.2-2.2S11,0,9.8,0C8.6,0,7.6,1,7.6,2.2c0,0,0,0,0,0C7.6,3.4,8.6,4.3,9.8,4.3C9.8,4.3,9.8,4.3,9.8,4.3z M19.6,9.5l1,0.2l0.3,4.4c0,0.4,0.3,0.6,0.7,0.6c0,0,0,0,0,0h2c0.4,0,0.6-0.3,0.7-0.6l0.3-4.4l1-0.2c0.2-0.1,0.4-0.3,0.4-0.5l-0.3-1.8c-0.1-1.2-1.1-2-2.3-2h-1.6c-1.2,0-2.2,0.9-2.3,2L19.2,9C19.2,9.2,19.4,9.4,19.6,9.5z M22.5,4.3c1.2,0,2.2-1,2.2-2.2S23.7,0,22.5,0c-1.2,0-2.2,1-2.2,2.2c0,0,0,0,0,0C20.4,3.4,21.3,4.3,22.5,4.3z M6.7,19.3l-0.2-1.9c-0.1-1.2-1.1-2-2.3-2H2.5c-1.2,0-2.2,0.8-2.3,2L0,19.3c0,0.2,0.1,0.5,0.4,0.5l1,0.2l0.3,4.4c0,0.4,0.3,0.6,0.7,0.6c0,0,0,0,0,0h2C4.7,25,5,24.7,5,24.4L5.3,20l1-0.2C6.5,19.8,6.7,19.6,6.7,19.3z M3.3,14.7c1.2,0,2.2-1,2.2-2.2s-1-2.2-2.2-2.2s-2.2,1-2.2,2.2c0,0,0,0,0,0C1.2,13.7,2.1,14.7,3.3,14.7z M19.5,19.3l-0.2-1.9c-0.1-1.2-1.1-2-2.3-2h-1.6c-1.2,0-2.2,0.8-2.3,2l-0.3,1.9c0,0.2,0.1,0.5,0.4,0.5l1.1,0.2l0.3,4.4c0,0.4,0.3,0.6,0.7,0.6c0,0,0,0,0,0h2c0.4,0,0.6-0.3,0.7-0.6l0.2-4.4l1-0.2C19.4,19.8,19.5,19.5,19.5,19.3z M16.1,14.7c1.2,0,2.2-1,2.2-2.1c0-1.2-1-2.2-2.1-2.2c-1.2,0-2.2,1-2.2,2.1c0,0,0,0,0,0C14,13.7,15,14.7,16.1,14.7z"/>
							</svg>
							<span>500 ljudi</span>
						</div>
					</div>
				</div>

				<div class="main-content">
					<div class="reviewBody" itemprop="reviewBody">
						<p><strong><a href="https://www.beogradnocu.com/docek-nove-godine/">Gotik Nova godina</a></strong><b>&nbsp;2021</b>
							<span> - najbolja novogodišnja proslava u gradu! Uz Beograd noću rezervišite sebi mesto u popularnom klubu jer je za Gotik Nova godina posebno veče i takav provod se dugo
								pamti. Mogućnost plaćanja preko računa i slanja karata na željenu adresu na teritoriji cele Srbije i u inostranstvu.</span></p>
						<h2>Gotik Nova godina rezervacije - proslavite najluđu noć u Gotiku</h2>
						<p>Kada je u pitanju Nova godina <b>Gotik je klub koji svakog 31. decembra nudi najbolji noćni provod</b> u gradu. U klubu Gotik Nova godina je doživljaj za pamćenje i to je
							iskustvo koje se prepričava!</p>
						<p><strong>Telefoni za sve informacije za doček Nove godine u klubu Gotik i prodaju karata su:<br> +381 62 34 34 33<br> +381 63 34 34 33<br> +381 63 33 33 44</strong>
						</p>
						<p><strong>MUZIKA:</strong><br> Maya Berović<br>
							<iframe width="500" height="281" src="https://www.youtube.com/embed/VxVnfCOhWEY?feature=oembed" frameborder="0"
							        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
						</p>
						<p><strong>PIĆE:</strong><br> Redovan klupski cenovnik pića uz mogućnost povećanja cena do 10%.</p>
						<p><strong>CENA:<br> </strong>Cena karte je 30 eur<br> Rezervacije se posebno plaćaju:<br> -Pultovi od 100 eur do 220 eur (uz najmanje od 2&nbsp;do 3 kupljene karte u
							zavisnosti od pozicije, cena u zavisnosti od pozicije, vidi na mapi, na mapama su pultovi označeni slovom P)<br> -Barski stolovi od 200 eur do 420 eur (uz najmanje od 5 do
							6 kupljenih karata u zavisnosti od pozicije, cena u zavisnosti od pozicije, vidi na mapi, na mapama su barski stolovi označeni slovom B)<br> -Visoka sedenja od 250 eur do
							500 eur (uz najmanje od 4&nbsp;do 7 kupljenih karata u zavisnosti od pozicije, cena u zavisnosti od pozicije, vidi na mapi, na mapi su visoka sedenja označena slovom V)<br>
							-Separei od 600 eur do 1500 eur (uz najmanje od 5 do 14 kupljenih karata u zavisnosti od pozicije, cena u zavisnosti od pozicije, vidi na mapi. Pojedini veći separei mogu
							se razdvojiti na dva manja separea. Pojedina dva manja separea mogu se spojiti u jedan veći separe, na mapi su separei&nbsp;označeni slovom S)<br>
							<strong>Rezervacije i prodaja karata su već krenule. Za više informacija pozovite +381 62 34 34 33</strong></p>
						<div id="attachment_224659" class="wp-caption aligncenter">
							<a href="https://www.beogradnocu.com/wp-content/uploads/2019/09/gotik-mapa-2020-26dec-bt.jpg">
								<img class="wp-image-224659 size-large" src="https://www.beogradnocu.com/wp-content/uploads/2019/09/gotik-mapa-2020-26dec-bt-1024x724.jpg"
								     alt="Docek Nove godine Beograd 2020 Klub Gotik mapa" width="1024" height="724"
								     srcset="https://www.beogradnocu.com/wp-content/uploads/2019/09/gotik-mapa-2020-26dec-bt-1024x724.jpg 1024w,
								     https://www.beogradnocu.com/wp-content/uploads/2019/09/gotik-mapa-2020-26dec-bt-300x212.jpg 300w,
								      https://www.beogradnocu.com/wp-content/uploads/2019/09/gotik-mapa-2020-26dec-bt-768x543.jpg 768w,
								      https://www.beogradnocu.com/wp-content/uploads/2019/09/gotik-mapa-2020-26dec-bt-770x545.jpg 770w"
								     sizes="(max-width: 1024px) 100vw, 1024px">
							</a>
							<p class="wp-caption-text">Docek Nove godine Beograd 2021 Klub Gotik mapa</p>
						</div>
						<p>Sve ponude za doček Nove godine 2021 možete pogledati&nbsp;<strong>
								<a title="Sve ponude za doček Nove godine na jednom mestu" href="https://www.beogradnocu.com/docek-nove-godine/">OVDE</a></strong>.</p>
						<h2>Nova godina Gotik - nezaboravan provod u popularnom klubu</h2>
						<p>Za klub Gotik Nova godina predstavlja idealan povod da svim svojim posetiocima priredi <b>provod koji se ne zaboravlja i žurku koja se danima prepričava</b>. Ovo je mesto
							koje u srpsku prestonicu donosi duh najboljih svetskih klubova, pa ne treba da čudi zbog čega su novogodišnje proslave u Gotiku toliko popularne.</p>
						<p>U luksuznom prostoru kluba okuplja se samo <b>najbolja gradska ekipa i slušaju najveći hitovi domaće muzike</b>, što je dovoljna garancija da će doček Nove godine u Gotiku
							biti itekako uzbudljiv i na svetskom nivou.</p>
						<p>Ako planirate da posetite klub <b>Gotik doček Nove godine</b> je sjajna prilika, s obzirom na to da u vrhunskom ambijentu kluba imate priliku da uživate u brojnim
							ekskluzivnim pogodnostima, a ono što je najvažnije jeste prepoznatljiva atmosfera - uvek dobra energija i veselo društvo.</p>
						<h2>Gotik Beograd Nova godina - žurka do ranih jutarnjih časova</h2>
						<p>Popularni klub se nalazi u Beton Hali, na<b> adresi Karađorđeva 2-4</b>. Ugodan ambijent i okruženje ovog kluba učiniće da novogodišnji provod bude još bolji i
							spektakularniji. Osim toga, ovde možete uživati do ranog jutra, s obzirom na to da je za Novu Godinu Gotik <b>otvoren od 22:00h do 04:00h</b>.</p>
						<p>Zato, ukoliko želite da se za Novu godinu najbolje provedete i da u nju uđete u velikom stilu, <b>u najluksuznijem prostoru i uz
								najprestižnije društvo</b>, neka vaš izbor za doček bude Gotik i sigurno nećete pogrešiti.</p>
					</div>
				</div>
			</div>
			@include('partials.sidebar')
		</div>
	</div>
	@include('partials.reservation')
	<div class="container">
		<div class="row">
			@include('partials.featured')
		</div>
	</div>
@endsection
