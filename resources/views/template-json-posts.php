<?php
header('Content-Type: application/json;charset=utf-8');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

/*
Template Name: JSON Posts
*/

$table_name1 = $wpdb->prefix . "pixs_gallery";
$table_name2 = $wpdb->prefix . "pixs_gallery_pic";
$table_name3 = $wpdb->prefix . "posts";
$table_name4 = $wpdb->prefix . "postmeta";
$table_name5 = $wpdb->prefix . "dogadjaji_bgn";
$table_name6 = $wpdb->prefix . "dogadjaji_bgn_ns";

if (isset($_REQUEST['lokalid'])) {
    $lokalId = $_REQUEST['lokalid'];
}
if (isset($_REQUEST['tiplokala'])) {
    $tipLokala = $_REQUEST['tiplokala'];
}
if (isset($_REQUEST['galleryid'])) {
    $galleryid = $_REQUEST['galleryid'];
}

if (array_key_exists('program', $_REQUEST)) {
    $today = date_i18n('Y-n-d');
    
    $days = array(
        date('Y-m-d', strtotime($today)),
        date('Y-m-d', strtotime($today . ' + 1 day')),
        date('Y-m-d', strtotime($today . ' + 2 day')),
        date('Y-m-d', strtotime($today . ' + 3 day')),
        date('Y-m-d', strtotime($today . ' + 4 day')),
        date('Y-m-d', strtotime($today . ' + 5 day')),
        date('Y-m-d', strtotime($today . ' + 6 day'))
    );
    $program = array();
    $program['program'] = array();
    foreach ($days as $date) {
        $daydata = array(
            'date' => $date,
            'dogadjaji' => array()
        );
        $posts_modified = array();
            
        query_posts(
            array(
            'post_type'=> array(
                'klubovi',
                'splavovi',
                'kafane',
                'barovi',
                'kafei',
                'restorani',
                'striptizbarovi'
            ),
            'orderby'=>'title',
            'order'=>'ASC',
            'posts_per_page'=>'-1'
            )
        );
        if (have_posts()) :
            while (have_posts()) : the_post();
                $localid = $post->ID;
                $postdata = get_post($lokalId, ARRAY_A);
                $lokaldata['name'] = htmlspecialchars($postdata['post_title']);
                $lokaldata['id_lokal'] = $localid;
                $query1 = 'SELECT * FROM ' . $table_name5 . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date . '"';
                $result1= $wpdb->get_results($query1, ARRAY_A);
                $query11 = 'SELECT * FROM ' . $table_name6 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date)) . '" AND tip_repetition = "w") ORDER BY priority DESC'; //(danu_nedelji = "'. date('N', strtotime($date1)) . '" )
                if (!$result1) {
                    $result1 = $wpdb->get_results($query11, ARRAY_A);
                }
                if ($result1) {
                    $result = $result1[0];
                    $dogtitle = '';
                    $dogadjaj = get_post($result['id_dogadjaja'], ARRAY_A);
                    if (function_exists('icl_object_id') & ICL_LANGUAGE_CODE != $deflang) {
                        global $sitepress;
                        $deflang = $sitepress->get_default_language();
                        $polje2 = ICL_LANGUAGE_CODE . '_beogradnocu_title_event';
                        $dogtitle = get_post_meta($result['id_dogadjaja'], $polje2, true);
                        if ($dogtitle == '') {
                            $dogtitle = $dogadjaj['post_title'];
                        }
                    } else {
                        $dogtitle = $dogadjaj['post_title'];
                    }
                    array_push($posts_modified, $dogadjaj['post_modified']);
                    $program['post_modified'] = date('Y-m-d H:i:s', strtotime(max($posts_modified)));
                    $lokaldata['dogadjaj'] = $dogtitle;
                    array_push($daydata['dogadjaji'], $lokaldata);
                }
            endwhile;
            array_push($program['program'], $daydata);
        endif;
    }
    
    wp_send_json($program);
    //echo json_encode($program);
}

if (array_key_exists('lokali', $_REQUEST)) {
    query_posts(
        array(
        'post_type'=> array(
            'klubovi',
            'splavovi',
            'kafane',
            'barovi',
            'kafei',
            'restorani',
            'striptizbarovi'
        ),
        'orderby'=>'title',
        'order'=>'ASC',
        'posts_per_page'=>'-1'
        )
    );
    $listaLokala = array(
        'lokali' => array()
    );
    $posts_modified = array();
    if (have_posts()) :
        while (have_posts()) : the_post();
            $beogradnocu_prikazilokal = get_post_meta($post->ID, 'beogradnocu_prikazivanjelokala', true);
            if ($beogradnocu_prikazilokal === '') {
                $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                $aboutLokal = array(
                    'name' => htmlspecialchars($post->post_title),
                    'id' => $post->ID,
                    'url' => get_permalink($post->ID),
                    'image' => $url,
                    'tip_lokala' => $post->post_type
                );
                array_push($listaLokala['lokali'], $aboutLokal);
                array_push($posts_modified, $post->post_modified);
                $listaLokala['post_modified'] = date('Y-m-d H:i:s', strtotime(max($posts_modified)));
            }
        endwhile;
    endif;

    wp_send_json($listaLokala);
    //echo json_encode($listaLokala);
}

if (isset($tipLokala)) {
    query_posts("post_type=$tipLokala&orderby=title&order=ASC&posts_per_page=-1");
    $listaLokala = array(
        'lokali' => array()
    );
    $posts_modified = array();
    if (have_posts()) :
        while (have_posts()) : the_post();
            $beogradnocu_prikazilokal = get_post_meta($post->ID, 'beogradnocu_prikazivanjelokala', true);
            if ($beogradnocu_prikazilokal === '') {
                $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                $aboutLokal = array(
                    'name' => $post->post_title,
                    'id' => $post->ID,
                    'url' => get_permalink($post->ID),
                    'image' => $url
                );
                array_push($listaLokala['lokali'], $aboutLokal);
                array_push($posts_modified, $post->post_modified);
                $listaLokala['post_modified'] = date('Y-m-d H:i:s', strtotime(max($posts_modified)));
            }
        endwhile;
    endif;

    wp_send_json($listaLokala);
    //echo json_encode($listaLokala);
}

if (isset($lokalId)) {
    $postdata = get_post($lokalId, ARRAY_A);

    if ($postdata == null) {
        echo "{}";
        return;
    }
    
    $lokaldata = array();
    $lokaldata['about'] = htmlspecialchars($postdata['post_content']);
    $lokaldata['name'] = htmlspecialchars($postdata['post_title']);
    $lokaldata['url'] = get_permalink($lokalId);
    $lokaldata['post_date'] = $postdata['post_date'];
    $lokaldata['post_date_gmt'] = $postdata['post_date_gmt'];
    $lokaldata['post_modified'] = date('Y-m-d H:i:s', strtotime($postdata['post_modified']));
    $lokaldata['post_modified_gmt'] = date('Y-m-d H:i:s', strtotime($postdata['post_modified_gmt']));
    $lokaldata['menu_order'] = $postdata['menu_order'];
    $lokaldata['gallery'] = array();
    
    if ($lokaldata['name'] == "") {
        echo "{}";
        return;
    }

    $listastolota = get_post_meta($lokalId); // , 'beogradnocu_tipsedenja_event', true

    $query1 = 'SELECT id, name, date, photocount, previewpic FROM '.$table_name1.' WHERE location LIKE "%'.$lokaldata['name'].'" ORDER BY date DESC LIMIT 50';
    $result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);
    
    if ($result1) {
        foreach ($result1 as $fe) {
            $gallerylist = array(
                'id_gallery' => $fe['id'],
                'name' => $fe['name'],
                'date' => $fe['date'],
                'photocount' => $fe['photocount'],
                'previewpic' => $fe['previewpic']
            );
            if ($fe['photocount'] > 0) {
                array_push($lokaldata['gallery'], $gallerylist);
            }
        }
    }
    
    // pozicije lokala
    $lokaldata['pozicije'] = array();
    if (isset($listastolota['beogradnocu_tipsedenja_event'])) {
        $listaarray = explode(',', $listastolota['beogradnocu_tipsedenja_event'][0]);
        foreach ($listaarray as $pozicija) {
            array_push($lokaldata['pozicije'], $pozicija);
        }
    }

    // adresa
    if (isset($listastolota['beogradnocu_ulica'])) {
        $lokaldata['adresa'] = $listastolota['beogradnocu_ulica']['0'];
    }
    // radnovreme
    if (isset($listastolota['beogradnocu_radno_vreme'])) {
        $lokaldata['radnovreme'] = $listastolota['beogradnocu_radno_vreme']['0'];
    }
    // kapacitetlokala
    if (isset($listastolota['beogradnocu_kapacitet'])) {
        $lokaldata['kapacitetlokala'] = $listastolota['beogradnocu_kapacitet']['0'];
    }

    // muzickizanr
    $muzika = get_the_terms($lokalId, 'muzikazanr');
    if (!empty($muzika) && $muzika && !is_wp_error($muzika)) {
        $musarray = array();
        foreach ($muzika as $term) {
            array_push($musarray, $term->name);
        }
        $lokaldata['muzickizanr'] = join(', ', $musarray);
    } elseif (isset($listastolota['beogradnocu_zanr'])) {
        $lokaldata['muzickizanr'] = $listastolota['beogradnocu_zanr']['0'];
    }


    // deo grada
    $lokacija = get_the_terms($lokalId, 'deograda');
    if (!empty($lokacija) && $lokacija && !is_wp_error($lokacija)) {
        $musarray = array();
        foreach ($lokacija as $term) {
            array_push($musarray, $term->name);
        }
        $lokaldata['deograda'] = join(', ', $musarray);
    }

    // tipkuhinje
    $kuhinja = get_the_terms($lokalId, 'tiprestorana');
    if (!empty($kuhinja) && $kuhinja && !is_wp_error($kuhinja)) {
        $musarray = array();
        foreach ($kuhinja as $term) {
            array_push($musarray, $term->name);
        }
        $lokaldata['tipkuhinje'] = join(', ', $musarray);
    }


    $lokaldata['program'] = array();
    // za program lokala
    if (function_exists('icl_object_id')) {
        global $sitepress;
        $deflang = $sitepress->get_default_language();
        $posttypes = get_post_type($lokalId);
        $localid = icl_object_id($lokalId, $posttypes, true, $deflang);
    } else {
        $localid = $lokalId;
    }

    global $wpdb;
    $table_name = $wpdb->prefix . "dogadjaji_bgn";
    $table_name2 = $wpdb->prefix . "dogadjaji_bgn_ns";

    $startline = date_i18n('N');

    $today = date_i18n('Y-n-d');
    $posttypes = get_post_type($lokalId);
    if ($posttypes === 'putovanja' or $posttypes === 'desavanja') {
        $query1 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date > "' . $today . '"';
        $result = $wpdb->get_results($query1, ARRAY_A);
        if (isset($result) and !empty($result)) {
            foreach ($result as $v) {
                $dogadjaj = get_post($v['id_dogadjaja'], ARRAY_A);

                if (function_exists('icl_object_id') & ICL_LANGUAGE_CODE != $deflang) {
                    global $sitepress;
                    $deflang = $sitepress->get_default_language();
                    $polje2 = ICL_LANGUAGE_CODE . '_beogradnocu_title_event';
                    $dogtitle = get_post_meta($v['id_dogadjaja'], $polje2, true);
                    if (!$dogtitle) {
                        $dogtitle = $dogadjaj['post_title'];
                    }
                } else {
                    $dogtitle = $dogadjaj['post_title'];
                }

                $lokaldata['program'][] = array(
                    'date' => $v['date'],
                    'name' => $dogtitle
                );
            }
        }
    } else {
            $date1 = $today;
            $date2 = date('Y-m-d', strtotime($today . ' + 1 day'));
            $date3 = date('Y-m-d', strtotime($today . ' + 2 day'));
            $date4 = date('Y-m-d', strtotime($today . ' + 3 day'));
            $date5 = date('Y-m-d', strtotime($today . ' + 4 day'));
            $date6 = date('Y-m-d', strtotime($today . ' + 5 day'));
            $date7 = date('Y-m-d', strtotime($today . ' + 6 day'));


            // Selektovanje po datumima


            $query1 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date1 . '" LIMIT 1';
            $result1 = $wpdb->get_results($query1, ARRAY_A);
            $query11 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date1)) . '" AND tip_repetition = "w") ORDER BY priority DESC LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date1)) . '" )
            if (!$result1) {
                $result1 = $wpdb->get_results($query11, ARRAY_A);
            }
            if ($result1) {
                $result[1] = $result1[0];
            }


            $query2 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date2 . '" LIMIT 1';
            $result2 = $wpdb->get_results($query2, ARRAY_A);
            $query22 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date2)) . '" AND tip_repetition = "w") ORDER BY priority DESC LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date1)) . '" )
            if (!$result2) {
                $result2 = $wpdb->get_results($query22, ARRAY_A);
            }
            if ($result2) {
                $result[2] = $result2[0];
            }


            $query3 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date3 . '" LIMIT 1';
            $result3 = $wpdb->get_results($query3, ARRAY_A);
            $query33 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date3)) . '" AND tip_repetition = "w") ORDER BY priority DESC LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date3)) . '" )
            if (!$result3) {
                $result3 = $wpdb->get_results($query33, ARRAY_A);
            }
            if ($result3) {
                $result[3] = $result3[0];
            }


            $query4 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date4 . '" LIMIT 1';
            $result4 = $wpdb->get_results($query4, ARRAY_A);
            $query44 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date4)) . '" AND tip_repetition = "w") ORDER BY priority DESC LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date4)) . '" )
            if (!$result4) {
                $result4 = $wpdb->get_results($query44, ARRAY_A);
            }
            if ($result4) {
                $result[4] = $result4[0];
            }


            $query5 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date5 . '" LIMIT 1';
            $result5 = $wpdb->get_results($query5, ARRAY_A);
            $query55 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date5)) . '" AND tip_repetition = "w") ORDER BY priority DESC LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date5)) . '" )
            if (!$result5) {
                $result5 = $wpdb->get_results($query55, ARRAY_A);
            }
            if ($result5) {
                $result[5] = $result5[0];
            }


            $query6 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date6 . '" LIMIT 1';
            $result6 = $wpdb->get_results($query6, ARRAY_A);
            $query66 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date6)) . '" AND tip_repetition = "w") LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date6)) . '" )
            if (!$result6) {
                $result6 = $wpdb->get_results($query66, ARRAY_A);
            }
            if ($result6) {
                $result[6] = $result6[0];
            }


            $query7 = 'SELECT * FROM ' . $table_name . ' WHERE id_lokal = "' . $localid . '" AND date = "' . $date7 . '" LIMIT 1';
            $result7 = $wpdb->get_results($query7, ARRAY_A);
            $query77 = 'SELECT * FROM ' . $table_name2 . ' WHERE id_lokal = "' . $localid . '" AND (tip_repetition = "d" OR danu_nedelji = "' . date('N', strtotime($date7)) . '" AND tip_repetition = "w") LIMIT 1'; //(danu_nedelji = "'. date('N', strtotime($date7)) . '" )
            if (!$result7) {
                $result7 = $wpdb->get_results($query77, ARRAY_A);
            }
            if ($result7) {
                $result[7] = $result7[0];
            }

            if (isset($result)) {
                foreach ($result as $k => $v) {
                    $dogadjaj = get_post($v['id_dogadjaja'], ARRAY_A);
                    switch ($k) {
                        case 1:
                            $todate = date('d.m.Y', strtotime($today));//$daprint1;
                            break;
                        case 2:
                            $todate = date('d.m.Y', strtotime($today . ' + 1 day'));//$daprint2;
                            break;
                        case 3:
                            $todate = date('d.m.Y', strtotime($today . ' + 2 day'));//$daprint3;
                            break;
                        case 4:
                            $todate = date('d.m.Y', strtotime($today . ' + 3 day'));//$daprint4;
                            break;
                        case 5:
                            $todate = date('d.m.Y', strtotime($today . ' + 4 day'));//$daprint5;
                            break;
                        case 6:
                            $todate = date('d.m.Y', strtotime($today . ' + 5 day'));//$daprint6;
                            break;
                        case 7:
                            $todate = date('d.m.Y', strtotime($today . ' + 6 day'));//$daprint7;
                            break;
                    }
                    if (function_exists('icl_object_id') & ICL_LANGUAGE_CODE != $deflang) {
                        global $sitepress;
                        $deflang = $sitepress->get_default_language();
                        $polje2 = ICL_LANGUAGE_CODE . '_beogradnocu_title_event';
                        $dogtitle = get_post_meta($v['id_dogadjaja'], $polje2, true);
                        if ($dogtitle == '') {
                            $dogtitle = $dogadjaj['post_title'];
                        }
                    } else {
                        $dogtitle = $dogadjaj['post_title'];
                    }

                    $lokaldata['program'][] = array(
                        'date' => $todate,
                        'name' => $dogtitle
                    );
                }
            }
    }

    if ($lokaldata['name']) {
    }
    
    wp_send_json($lokaldata);
    //echo json_encode($lokaldata);
    // print_r($lokaldata);
}

if (isset($galleryid)) {
    $gallerylist = array(
        'pictures' => array()
    );
    $query1 = 'SELECT t1.source FROM '.$table_name2.' AS t1, '.$table_name1.' AS t2 WHERE t1.gid = t2.gid AND t2.id = "'.$galleryid.'"';
    $result1 = $wpdb->get_results($query1, ARRAY_A);
    if ($result1) {
        foreach ($result1 as $fe) {
            $source = $fe['source'];
            array_push($gallerylist['pictures'], $source);
        }
    }
    wp_send_json($gallerylist);
    //echo json_encode($gallerylist);
}

if (array_key_exists('vesti', $_REQUEST)) {
    if (isset($_REQUEST['meta'])) {
        $meta = $_REQUEST['meta'];
    } else {
        $meta = 'false';
    }
    if (isset($_REQUEST['post_id'])) {
        $postid = $_REQUEST['post_id'];
        $postdata = get_post($postid, ARRAY_A);
        if ($meta == 'true') {
            $vest = array(
                'post_id' => $postid,
                'post_modified' => date('Y-m-d H:i:s', strtotime($postdata['post_modified'])),
            );
        } else {
            $vest = array(
                'title' => $postdata['post_title'],
                'post_id' => $postid,
                'url' => get_permalink($postid),
                'post_modified' => date('Y-m-d H:i:s', strtotime($postdata['post_modified'])),
                'featured_image' => wp_get_attachment_image_src(get_post_thumbnail_id($postid), 'full')[0],
                'link' => get_permalink($postid),
                'comments' => get_comments_link($postid),
                'pubDate' => date('Y-m-d H:i:s', strtotime($postdata['post_date'])),
                'categories' => array(),
                'content' => $postdata['post_content']
            );
            $post_categories = wp_get_post_categories($postid);
            foreach ($post_categories as $c) {
                $cat = get_category($c);
                array_push($vest['categories'], $cat->cat_name);
            }
        }
        wp_send_json($vest);
        //echo json_encode($vest);
    } else {
        if (isset($_REQUEST['start'])) {
            $start = $_REQUEST['start'];
        } else {
            $start = 0;
        }
        if (isset($_REQUEST['limit'])) {
            $limit = $_REQUEST['limit'];
            $limitmeta = $_REQUEST['limit'];
            if ($limitmeta == -1) {
                $limit = 1000;
                $limitmeta = 1000;
            }
        } else {
            $limit = 1000;
            $limitmeta = 1000;
        }
        if ($meta == 'true') {
            $posts_modified = array();
            $vesti = array(
                'vesti' => array(),
            );
            if ($limitmeta == -1) {
                $query1 = 'SELECT ID, post_modified FROM '.$table_name3.' WHERE post_status = "publish" AND post_type = "post" ORDER BY post_modified DESC';
            } else {
                $query1 = 'SELECT ID, post_modified FROM '.$table_name3.' WHERE post_status = "publish" AND post_type = "post" ORDER BY post_modified DESC LIMIT '.$start.','.$limitmeta;
            }
            $result1 = $wpdb->get_results($query1, ARRAY_A);
            if ($result1) {
                foreach ($result1 as $fe) {
                    $postid = $fe['ID'];
                    $tmp = get_permalink($postid);
                    $vest = array(
                        'post_id' => $postid,
                        'url' => $tmp,
                        'post_modified' => date('Y-m-d H:i:s', strtotime($fe['post_modified'])),
                    );
                    array_push($posts_modified, $fe[post_modified]);
                    $vesti['post_modified'] = date('Y-m-d H:i:s', strtotime(max($posts_modified)));
                    array_push($vesti['vesti'], $vest);
                }
            }
            wp_send_json($vesti);
            //echo json_encode($vesti);
        } else {
            query_posts(
                array(
                    'post_type'=> 'post',
                    'offset'=> $start,
                    'posts_per_page' => $limit
                )
            );
            $posts_modified = array();
            $vesti = array(
                'vesti' => array(),
            );
            if (have_posts()) :
                while (have_posts()) : the_post();
                    $vest = array(
                        'title' => $post->post_title,
                        'post_id' => $post->ID,
                        'url' => get_permalink($post->ID),
                        'post_modified' => date('Y-m-d H:i:s', strtotime($post->post_modified)),
                        'featured_image' => wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full')[0],
                        'link' => get_permalink($post->ID),
                        'comments' => get_comments_link($post->ID),
                        'pubDate' => date('Y-m-d H:i:s', strtotime($post->post_date)),
                        'categories' => array(),
                        'content' => $post->post_content
                    );
                    $post_categories = wp_get_post_categories($post->ID);
                    foreach ($post_categories as $c) {
                        $cat = get_category($c);
                        array_push($vest['categories'], $cat->cat_name);
                    }
                    array_push($posts_modified, $post->post_modified);
                    $vesti['post_modified'] = date('Y-m-d H:i:s', strtotime(max($posts_modified)));
                    array_push($vesti['vesti'], $vest);
                endwhile;
            endif;
            wp_send_json($vesti);
            //echo json_encode($vesti);
        }
    }
}

if (array_key_exists('docek-nove', $_REQUEST)) {
    if (isset($_REQUEST['meta'])) {
        $meta = $_REQUEST['meta'];
    } else {
        $meta = 'false';
    }
    if (isset($_REQUEST['post_id'])) {
        $postid = $_REQUEST['post_id'];
        $postdata = get_post($postid, ARRAY_A);
        if ($meta == 'true') {
            $kategorija = wp_get_post_terms($postid, 'kategorijalokalanovagodina')[0]->slug;
            $docek = array(
                'post_id' => $postid,
                'kategorija' => $kategorija,
                'post_modified' => date('Y-m-d H:i:s', strtotime($postdata['post_modified'])),
            );
        } else {
            $adresa = get_post_meta($postid, 'beogradnocu_ulica', true);
            $radno_vreme = get_post_meta($postid, 'beogradnocu_radno_vreme', true);
            $kapacitet = get_post_meta($postid, 'beogradnocu_kapacitet', true);
            $cena = get_post_meta($postid, 'beogradnocu_ng_cena', true);
            $izvodjac = get_post_meta($postid, 'beogradnocu_zanr', true);
            $kategorija = wp_get_post_terms($postid, 'kategorijalokalanovagodina')[0]->slug;
            $docek = array(
                'title' => $postdata['post_title'],
                'post_id' => $postid,
                'priority' => $postdata['menu_order'],
                'url' => get_permalink($postid),
                'adresa' => $adresa,
                'radno_vreme' => $radno_vreme,
                'kapacitet' => $kapacitet,
                'cena' => $cena,
                'izvodjac' => $izvodjac,
                'kategorija' => $kategorija,
                'post_modified' => date('Y-m-d H:i:s', strtotime($postdata['post_modified'])),
                'featured_image' => wp_get_attachment_image_src(get_post_thumbnail_id($postid), 'full')[0],
                'link' => get_permalink($postid),
                'comments' => get_comments_link($postid),
                'pubDate' => date('Y-m-d H:i:s', strtotime($postdata['post_date'])),
                'categories' => array(),
                'content' => $postdata['post_content']
            );
            $post_categories = wp_get_post_categories($postid);
            foreach ($post_categories as $c) {
                $cat = get_category($c);
                array_push($docek['categories'], $cat->cat_name);
            }
        }
        wp_send_json($docek);
        //echo json_encode($docek);
    } else {
        if (isset($_REQUEST['start'])) {
            $start = $_REQUEST['start'];
        } else {
            $start = 0;
        }
        if (isset($_REQUEST['limit'])) {
            $limit = $_REQUEST['limit'];
            $limitmeta = $_REQUEST['limit'];
        } else {
            $limit = 1000;
            $limitmeta = -1;
        }
        if ($meta == 'true') {
            $posts_modified = array();
            $nova = array(
                'novagodina' => array(),
            );
            if ($limitmeta == -1) {
                $query1 = 'SELECT ID, post_modified FROM '.$table_name3.' WHERE post_status = "publish" AND post_type = "nova-godina" ORDER BY post_modified DESC';
            } else {
                $query1 = 'SELECT ID, post_modified FROM '.$table_name3.' WHERE post_status = "publish" AND post_type = "nova-godina" ORDER BY post_modified DESC LIMIT '.$start.','.$limitmeta;
            }
            $result1 = $wpdb->get_results($query1, ARRAY_A);
            if ($result1) {
                foreach ($result1 as $fe) {
                    $postid = $fe['ID'];
                    $kategorija = wp_get_post_terms($postid, 'kategorijalokalanovagodina')[0]->slug;
                    $tmp = get_permalink($postid);
                    $docek = array(
                        'post_id' => $postid,
                        'url' => $tmp,
                        'kategorija' => $kategorija,
                        'post_modified' => date('Y-m-d H:i:s', strtotime($fe['post_modified'])),
                    );
                    array_push($posts_modified, $fe[post_modified]);
                    $nova['post_modified'] = date('Y-m-d H:i:s', strtotime(max($posts_modified)));
                    if ($kategorija != null) {
                        array_push($nova['novagodina'], $docek);
                    }
                }
            }
            wp_send_json($nova);
            //echo json_encode($nova);
        } else {
            query_posts(
                array(
                    'post_type'=> 'nova-godina',
                    'offset'=> $start,
                    'posts_per_page' => $limit
                )
            );
            $posts_modified = array();
            $nova = array(
                'novagodina' => array(),
            );
            if (have_posts()) :
                while (have_posts()) : the_post();
                    $postid = $post->ID;
                    $adresa = get_post_meta($postid, 'beogradnocu_ulica', true);
                    $radno_vreme = get_post_meta($postid, 'beogradnocu_radno_vreme', true);
                    $kapacitet = get_post_meta($postid, 'beogradnocu_kapacitet', true);
                    $cena = get_post_meta($postid, 'beogradnocu_ng_cena', true);
                    $izvodjac = get_post_meta($postid, 'beogradnocu_zanr', true);
                    $kategorija = wp_get_post_terms($postid, 'kategorijalokalanovagodina')[0]->slug;
                    $docek = array(
                        'title' => $post->post_title,
                        'post_id' => $post->ID,
                        'priority' => $post->menu_order,
                        'url' => get_permalink($post->ID),
                        'adresa' => $adresa,
                        'radno_vreme' => $radno_vreme,
                        'kapacitet' => $kapacitet,
                        'cena' => $cena,
                        'izvodjac' => $izvodjac,
                        'kategorija' => $kategorija,
                        'post_modified' => date('Y-m-d H:i:s', strtotime($post->post_modified)),
                        'featured_image' => wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full')[0],
                        'link' => get_permalink($post->ID),
                        'comments' => get_comments_link($post->ID),
                        'pubDate' => date('Y-m-d H:i:s', strtotime($post->post_date)),
                        'categories' => array(),
                        'content' => $post->post_content
                    );
                    $post_categories = wp_get_post_categories($post->ID);
                    foreach ($post_categories as $c) {
                        $cat = get_category($c);
                        array_push($docek['categories'], $cat->cat_name);
                    }
                    array_push($posts_modified, $post->post_modified);
                    $nova['post_modified'] = date('Y-m-d H:i:s', strtotime(max($posts_modified)));
                    if ($kategorija != null) {
                        array_push($nova['novagodina'], $docek);
                    }
                endwhile;
            endif;
            wp_send_json($nova);
            //echo json_encode($nova);
        }
    }
}
