<?php

/*
Template Name: App List Users Dev
*/

?>

<?php
$servername = "127.0.0.1";
$username = "root";
$password = "";
$dbname = "nocu_app";

 $connect = mysqli_connect($servername, $username, $password, $dbname);
 $querytotal = "SELECT * from users ORDER BY created_at DESC";
 $query = "SELECT id,name,mobile,birthday,created_at,DATE(  created_at ), COUNT( * ) as total FROM  users GROUP BY DATE(  created_at ) ORDER BY created_at DESC";

 $resultotal = mysqli_query($connect, $querytotal);
 $result = mysqli_query($connect, $query);
 ?>
 <!DOCTYPE html>
 <html>
      <head>
           <title>Broj registrovanih korisnika po datumu</title>
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
           <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
           <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      </head>
      <body>
           <style>
                th, td {
                     text-align: center;
                }
                </style>
           <br /><br />
           <div class="container">
                <div class="row">
                <div class="col-lg-12">
                <div style="display: none;" id="info">
                     <table class="table table-bordered table-hover">
                    <tr>
                      <th>Period</th>
                      <th>Od</th>
                      <th>Do</th>
                      <th>B.K</th>
                      <th>P.K</th>
                    </tr>
                    <tr>
                         <td id="period"></td>
                         <td id="od"></td>
                         <td id="do"></td>
                         <td id="bk"></td>
                         <td id="pk"></td>
                    </tr>

                     </table>
                </div>
                </div>
                <h2 align="center">Broj registrovanih korisnika po datumu</h2>

                <div class="col-md-3 col-xs-3">
                     <input type="text" name="from_date" id="from_date" class="form-control" placeholder="Datum OD" />
                </div>
                <div class="col-md-3 col-xs-3">
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="Datum DO" />
                </div>
                <div class="col-md-3 col-xs-3">
                     <select class="form-control" name="days" id="days">
                          <option value=""></option>
                          <option value="days">Danas</option>
                          <option value="week">Nedelju dana</option>
                          <option value="month">Mesec dana</option>
                          <option value="7">Poslednjih 7 dana</option>
                          <option value="30">Poslednjih 30 dana</option>
                          <option value="tekucanedelja">Tekuća nedelja</option>
                          <option value="tekucimesec">Tekući mesec</option>
                     </select>
                </div>
                <div class="col-md-3 col-xs-3">
                     <input type="button" name="filter" id="filter" value="Filter" class="form-control btn btn-info" />
                </div>
                <div style="clear:both"></div>
                <br />
                <div class="col-lg-12">
                <div id="order_table">
                     <table class="table table-bordered table-hover">
                    <tr>
                         <th width="15%">Datumi</th>
                         <th width="15%">Ukupno: <?php echo mysqli_num_rows($resultotal); ?></th>
                    </tr>
                     <?php

                     while ($row = mysqli_fetch_array($result)) {
                         $created_at =  $row["created_at"];
                         $total = $row["total"];
                     ?>
                          <tr>

                             <?php if ($created_at === '0000-00-00 00:00:00') { ?>
                                   <td><?php echo 'Nije uneto.'; ?></td>
                              <?php } else { ?>
                                   <td><?php echo  date("d.m.Y", strtotime($row["created_at"])) ?></td>
                             <?php  } ?>
                             <td><?php echo $total; ?></td>
                          </tr>
                     <?php
                     }

                     ?>

                     </table>
                </div>
                </div>
           </div>
           </div>
      </body>
 </html>
 <script>
      $(document).ready(function(){
           $.datepicker.setDefaults({
                dateFormat: 'yy-mm-dd'
           });
           $(function(){
                $("#from_date").datepicker();
                $("#to_date").datepicker();
           });
           $('#filter').click(function(){
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var days = $('#days').find(":selected").text();
                $('#od').text(from_date);
                $('#do').text(to_date);


                if(from_date != '' && to_date != '')  {
                     $.ajax({
                          url:"/wp-content/themes/beogradnocu2016/filter.php",
                          method:"POST",
                          data:{from_date:from_date,to_date:to_date},
                          success:function(data)
                          {
                               $('#order_table').html(data);
                          }
                     });
                } else if(from_date == '' && to_date == '')  {
                    var days = $('#days').find(":selected").val();
                    var daystext = $('#days').find(":selected").text();
                    $('#period').text(daystext);
                    $.ajax({
                          url:"/wp-content/themes/beogradnocu2016/filter.php",
                          method:"POST",
                          data:{days: days},
                          success:function(data)
                          {
                               $('#order_table').html(data);
                          }
                     });
               } else if(days !='')  {
                    var days = $('#days').find(":selected").val();
                    var daystext = $('#days').find(":selected").text();
                    $('#period').text(daystext);
                    $.ajax({
                          url:"/wp-content/themes/beogradnocu2016/filter.php",
                          method:"POST",
                          data:{days: days},
                          success:function(data)
                          {
                               $('#order_table').html(data);
                          }
                     });
                } else {
               var from_date = $('#from_date').val();
               var to_date = "<?php echo date('Y-m-d'); ?>";
               $('#od').text(from_date);
               $('#do').text(to_date);

                    $.ajax({
                         url:"/wp-content/themes/beogradnocu2016/filter.php",
                          method:"POST",
                          data:{from_date:from_date, to_date:to_date},
                          success:function(data)
                          {
                               $('#order_table').html(data);
                          }
                     });
                }
           });
      });
 </script>
