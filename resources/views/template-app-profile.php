<?php
header('Content-Type: application/json;charset=utf-8');
header("Cache-Control: no-cache, must-revalidate");

define('VERSION', '2.5.3');
/*
Template Name: App Users
*/

function executeQuery($sql) {
	$servername = "localhost";
	$username = "beogradnocu";
	$password = "kdGr74!@ddklF";
	$dbname = "beogradnocu_apps";
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn->query("SET NAMES 'utf8'");
	$stmt = $conn->prepare($sql); 
	$stmt->execute();
	return $stmt;
}

function setScore($user_uuid) {
	try {					
		// Rezervacije
		$sql = "SELECT COUNT(*) AS r FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 100";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r=". $result['r'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Rezervacije preko 10k
		$sql = "SELECT COUNT(*) AS r10k FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 102";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r10k=". $result['r10k'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Rezervacije preko 20k
		$sql = "SELECT COUNT(*) AS r20k FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 105";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r20k=". $result['r20k'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Rezervacije preko 30k
		$sql = "SELECT COUNT(*) AS r30k FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 103";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r30k=". $result['r30k'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Rezervacije preko 50k
		$sql = "SELECT COUNT(*) AS r50k FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 106";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r50k=". $result['r50k'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Rezervacije preko 100k
		$sql = "SELECT COUNT(*) AS r100k FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 104";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET r100k=". $result['r100k'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Invites
		$sql = "SELECT COUNT(*) AS invites FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 10";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET invites=". $result['invites'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Bills
		$sql = "SELECT COUNT(*) AS bills FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 101";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET bills=". $result['bills'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Checkins
		$sql = "SELECT COUNT(*) AS checkins FROM points WHERE uuid='". $user_uuid ."' AND TYPE = 0";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET checkins=". $result['checkins'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
		// Score
		$sql = "SELECT SUM(points) AS score FROM points WHERE uuid='". $user_uuid ."'";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();				
		$sql = "UPDATE users SET score=". $result['score'] ." WHERE uuid='". $user_uuid ."'";		
		executeQuery($sql);
	}
	catch(PDOException $e) {		
		echo $e->message;
	}
}

if (isset($_REQUEST['addUser'])) {
	$data = file_get_contents("php://input");
	
	$user_data = json_decode($data);			
	
	$response = array();
	$response['status'] = "";
	$mobile = $user_data->mobile;
	$mobile = str_replace("-", "", $mobile);
	$mobile = str_replace(" ", "", $mobile);
	$mobile = str_replace("(", "", $mobile);
	$mobile = str_replace(")", "", $mobile);
	$sql = "INSERT INTO users (uuid, deviceid, name, mobile, created_at) VALUES ('". $user_data->uuid. "','". $user_data->deviceid. "','". $user_data->name. "','". $mobile ."', NOW())";		
	try {
		executeQuery($sql);	
		$response['status'] = "success";
		$sql = "INSERT INTO points (points, uuid, changed_at, type, event_uuid) VALUES (500, '". $user_data->uuid. "', NOW(), 1, '". $user_data->event ."')";		
		executeQuery($sql);		
		$sql = "SELECT SUM(points) AS score FROM points WHERE uuid='". $user_data->uuid ."'";
		$stmt = executeQuery($sql);
		$result = $stmt->fetch();	
		$sql = "UPDATE users SET score='". $result['score'] ."' WHERE uuid='". $user_data->uuid ."'";				
		executeQuery($sql);
		
		$mobile = substr($user_data->mobile, -7, 7);
		$sql = "SELECT * FROM invites WHERE mobile LIKE '%". $mobile ."' AND status = 1";				
		$stmt = executeQuery($sql);		
				
		$result = $stmt->fetch();	
		if($result) {
			$invite_uuid = $result['uuid'];						
			$invite_id = $result['id'];
			$sql = "INSERT INTO points (points, uuid, changed_at, type, event_uuid) VALUES (100, '". $invite_uuid. "', NOW(), 10, '".$user_data->uuid."')";		
			executeQuery($sql);
			$sql = "SELECT SUM(points) AS score FROM points WHERE uuid='". $invite_uuid ."'";
			$stmt = executeQuery($sql);
			$result = $stmt->fetch();				
			$sql = "UPDATE users SET score=". $result['score'] .", invites = invites + 1 WHERE uuid='". $invite_uuid ."'";		
			executeQuery($sql);
			$sql = "UPDATE invites SET status = 0 WHERE id='". $invite_id ."'";		
			executeQuery($sql);
		}		
	}
	catch(PDOException $e) {
		$response['status'] = "fail";
		$response['e'] = $e;
	}	
	echo json_encode($response);
}

if (isset($_REQUEST['fillQuestionary'])) {
	$data = file_get_contents("php://input");
	
	$user_data = json_decode($data);			
	
	$response = array();
	$response['status'] = "";	
	$name = $user_data->name;
	if(empty($name)) {
		$sql = "UPDATE users SET q='".json_encode($user_data->q)."' WHERE uuid='". $user_data->uuid ."'";			
	} else {
		$sql = "UPDATE users SET q='".json_encode($user_data->q)."', name='".$name."' WHERE uuid='". $user_data->uuid ."'";			
	}
	try {
		executeQuery($sql);	
		$response['status'] = "success";
	}
	catch(PDOException $e) {
		$response['status'] = "fail";		
	}	
	echo json_encode($response);
}

if (isset($_REQUEST['deviceId'])) {
	$data = file_get_contents("php://input");
	
	$user_data = json_decode($data);			
	
	$response = array();
	$response['status'] = "";	
	$sql = "UPDATE users SET deviceid='".$user_data->deviceId."' WHERE uuid='". $user_data->uuid ."'";			
	try {
		executeQuery($sql);	
		$response['status'] = "success";
	}
	catch(PDOException $e) {
		$response['status'] = "fail";		
	}	
	echo json_encode($response);
}

if (isset($_REQUEST['invite'])) {
	$data = file_get_contents("php://input");
	
	$user_data = json_decode($data);			
	
	$response = array();
	$response['status'] = "";
	try {		
		$response['status'] = "success";
		$mobile = $user_data->mobile;
		$mobile = str_replace("-", "", $mobile);
		$mobile = str_replace(" ", "", $mobile);
		$mobile = str_replace("(", "", $mobile);
		$mobile = str_replace(")", "", $mobile);
		$sql = "INSERT INTO invites (mobile, uuid, status) VALUES ('". $mobile. "', '". $user_data->uuid ."', 1)";		
		executeQuery($sql);				
	}
	catch(PDOException $e) {
		$response['status'] = "fail ". $e;
	}
	echo json_encode($response);
}

if (isset($_REQUEST['getUser'])) {
	$userid = $_REQUEST['getUser'];
	
	if(!empty($userid)) {
		$sql = "SELECT * FROM users WHERE uuid='".$userid."'";		
		$response = array();
		try {
			$stmt = executeQuery($sql);
			
			$result = $stmt->fetch();	
			if($result) {
				$response['name'] = $result['name'];
				$response['mobile'] = $result['mobile'];								
				if(empty($result['q'])) {
					$response['q'] = null;
				} else {
					$response['q'] = json_decode($result['q']);
				}
				$response['score'] = $result['score'];
				$response['r'] = $result['r'];
				$response['r10k'] = $result['r10k'];
				$response['r20k'] = $result['r20k'];
				$response['r30k'] = $result['r30k'];
				$response['r50k'] = $result['r50k'];
				$response['r100k'] = $result['r100k'];
				$response['invites'] = $result['invites'];
			}
		}
		catch(PDOException $e) {
			$response['status'] = "fail";
		}
	} else {
		$response['status'] = "fail";
	}
	echo json_encode($response);
}

if (isset($_REQUEST['getProfile'])) {
	$data = file_get_contents("php://input");
	
	$user_data = json_decode($data);

	if(empty($user_data->version)) {
		$user_data->version = "old";
	}
	$response = array();
	$response['status'] = "fail";
	$response['version'] = VERSION;
	$response['user_version'] = $user_data->version;
	if(strcmp($user_data->version, VERSION) == 0) {
		$sql = "SELECT * FROM users WHERE uuid='".$user_data->uuid."'";				
		try {
			$stmt = executeQuery($sql);			
			$result = $stmt->fetch();	
			if($result) {
				$response['status'] = "success";
				$response['name'] = $result['name'];
				$response['mobile'] = $result['mobile'];								
				if(empty($result['q'])) {
					$response['q'] = null;
				} else {
					$response['q'] = json_decode($result['q']);
				}
				$response['score'] = $result['score'];
				$response['r'] = $result['r'];
				$response['r10k'] = $result['r10k'];
				$response['r20k'] = $result['r20k'];
				$response['r30k'] = $result['r30k'];
				$response['r50k'] = $result['r50k'];
				$response['r100k'] = $result['r100k'];
				$response['invites'] = $result['invites'];
				$response['checkins'] = $result['checkins'];
				$response['bills'] = $result['bills'];
			}
		}
		catch(PDOException $e) {			
			$response['message'] = "Error get profile";
		}
	} else {
		$response['message'] = "Instalirajte novu verziju aplikacije";		
	}
	echo json_encode($response);
}

if (isset($_REQUEST['fix'])) {
    $uuid = $_REQUEST['fix'];
	
	try {			
		$sql = "SELECT * FROM users WHERE uuid = '".$uuid."'";		
		$stmt = executeQuery($sql);				
		$result = $stmt->fetch();
		if($result) {
			if(empty($result['q'])) {
				$anketa = null;
			} else {
				$anketa = json_decode($result['q']);
			}
			if($anketa) {
				if (strpos($anketa->age, '-') !== false) {
					$month = substr($anketa->age, 0, 2);    
					$day = substr($anketa->age, 3, 2);    
					$year = substr($anketa->age, -4);
					$birthday = $day .".". $month .".". $year;					
					$anketa->age = $birthday;
					$sql = "UPDATE users SET q='".json_encode($anketa)."' WHERE uuid='". $uuid ."'";		
					$stmt = executeQuery($sql);						
				}			
			}
		}
	}
	catch(PDOException $e) {		
		echo $e->message;
	}
}

if (isset($_REQUEST['fixBirthday'])) {    	
	try {					
		$sql = "SELECT * FROM users";		
		$stmt = executeQuery($sql);				
		$rows = $stmt->fetchAll();				
		foreach ($rows as $row) {				
			if(empty($row['q'])) {				
				$anketa = null;
			} else {				
				$anketa = json_decode($row['q']);
			}						
			if($anketa) {							
				$uuid = $row['uuid'];			
				
				$day = substr($anketa->age, 0, 2);    
				$month = substr($anketa->age, 3, 2);    
				$year = substr($anketa->age, -4);
				$birthday = $year ."-". $month ."-". $day;									
				
				$sql = "UPDATE users SET birthday='".$birthday."' WHERE uuid='". $uuid ."'";		
				$stmt = executeQuery($sql);						
			}
		}
	}
	catch(PDOException $e) {		
		echo $e->message;
	}
}

if (isset($_REQUEST['fixScore'])) {
    $uuid = $_REQUEST['fixScore'];
	
	setScore($uuid);
}