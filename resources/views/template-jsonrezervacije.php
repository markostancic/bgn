<?php
/*
Template Name: JSON Lista Rezervacija
*/
$table_name = $wpdb->prefix . "dogadjaji_bgn";
$table_name2 = $wpdb->prefix . "dogadjaji_bgn_ns";
$table_name3 = $wpdb->prefix . "posts";

$datum = date_i18n('Y-m-d');

$daylist = array(
    $datum,
    date("Y-m-d", strtotime("$datum +1 day")),
    date("Y-m-d", strtotime("$datum +2 day")),
    date("Y-m-d", strtotime("$datum +3 day")),
    date("Y-m-d", strtotime("$datum +4 day")),
    date("Y-m-d", strtotime("$datum +5 day")),
    date("Y-m-d", strtotime("$datum +6 day"))
);

$typelist = array(
  'klubovi_beograd',
  'splavovi_beograd',
  'kafane_beograd',
  'barovi_beograd',
  'restorani_beograd',
  'striptiz_beograd',
  'desavanja'
);

$jsonresponse = array('dani' => array());

foreach ($daylist as $day) {
    $da = explode('-', $day);
    $danecho = $da[2] . '.' . $da[1] . '.' . $da[0];

    $daylist = array(
        'date' => $danecho,
        'data' => array()
    );
    foreach ($typelist as $type) {
        $datalist = array(
            'type' => $type,
            'lokali' => array()
        );
        $query1 = 'SELECT t1.id_lokal, t1.id_dogadjaja, t2.post_title FROM '.$table_name.' AS t1, '.$table_name3.' AS t2 WHERE t1.id_lokal = t2.id AND t1.date="'.$day.'" AND t1.tip_lokala="'.$type.'" ORDER BY t2.post_title';
        $result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);

        if ($result1) {
            foreach ($result1 as $fe) {
                $title = get_the_title($fe['id_lokal']);
                $loc[]=$fe['id_lokal'];
                $description = get_the_title($fe['id_dogadjaja']);
                $tipsed = get_post_meta($fe['id_dogadjaja'], 'beogradnocu_tipsedenja_event', true);
                if ($tipsed != '') {;
                    $araysede = explode(',', $tipsed);
                } else {
                    $tipsed = get_post_meta($fe['id_lokal'], 'beogradnocu_tipsedenja_event', true);
                    if ($tipsed != '') {;
                        $araysede = explode(',', $tipsed);
                    }
                }

                $listloc = array(
                    'id_dogadjaja'  => $fe['id_dogadjaja'],
                    'id_lokal'      => $fe['id_lokal'],
                    'lokal' => $title,
                    'description' => $description,
                    'tip_sedenja' => $araysede
                );
                array_push($datalist['lokali'], $listloc);
            }
            $IDS = esc_sql(implode(',', $loc));
        }

        $query11 = 'SELECT t1.id_lokal, t1.id_dogadjaja, t2.post_title FROM '.$table_name2.' AS t1, '.$table_name3.' AS t2 WHERE t1.id_lokal = t2.id AND t1.tip_lokala="'.$type.'" AND';
        if ($result1) {
            $query11 .= " t1.id_lokal NOT IN ('$IDS') AND ";
        }
        $query11 .= "(t1.tip_repetition = 'd' OR t1.danu_nedelji = '". date('N', strtotime($day)) . "' AND t1.tip_repetition = 'w') AND t1.start_date <='" . $day . "' ORDER BY t2.post_title";
        $result11 = $wpdb->get_results($query11, ARRAY_A);

        if ($result11) {
            foreach ($result11 as $fe) {
                $title = get_the_title($fe['id_lokal']);
                $description = get_the_title($fe['id_dogadjaja']);
                $tipsed = get_post_meta($fe['id_dogadjaja'], 'beogradnocu_tipsedenja_event', true);
                if ($tipsed != '') {;
                    $araysede = explode(',', $tipsed);
                } else {
                    $tipsed = get_post_meta($fe['id_lokal'], 'beogradnocu_tipsedenja_event', true);
                    if ($tipsed != '') {;
                        $araysede = explode(',', $tipsed);
                    }
                }

                $listloc = array(
                    'id_dogadjaja'  => $fe['id_dogadjaja'],
                    'id_lokal'      => $fe['id_lokal'],
                    'lokal' => htmlspecialchars_decode($title),
                    'description' => $description,
                    'tip_sedenja' => $araysede
                );

                array_push($datalist['lokali'], $listloc);
            }
        }

        array_push($daylist['data'], $datalist);
    }
    array_push($jsonresponse['dani'], $daylist);
}

echo json_encode($jsonresponse);
