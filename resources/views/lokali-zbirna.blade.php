{{--
  Template Name: Lokali zbirna
--}}

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.breadcrumb')
            <div class="col-12 col-lg-8">
              <?php
                $today = date('d.m.Y.');
                $today_format = month(date('n', strtotime($today))).' '.date('Y.');
                ?>
                <h1>Klubovi Beograd - {{ $today_format }}</h1>
                @include('partials.lokal-list-date')
                @include('partials.lokali-cards')
                <div class="main-content custom-mb-big">
                    @include('partials.content-page')
                </div>
            </div>
            @include('partials.sidebar')
        </div>
    </div>
    @include('partials.reservation')
    <div class="container">
        <div class="row">
            @include('partials.list-lokal')
            @include('partials.featured')
        </div>
    </div>
@endsection
