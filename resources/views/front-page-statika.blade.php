{{--
Template Name: Front-page statika
--}}

@extends('layouts.app')

@section('content')
	<div class="hero_front">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-9">
					<div class="nightout">
						<div class="nightout_heading custom-flex-row">
							<div class="nightout_heading_main">
								<p class="custom-heading-2">Gde izaći večeras - izlasci Beograd</p>
								<?php
								$today = date( 'd.m.Y.' );
								$today_format = date( 'd', strtotime( $today ) ) . '. ' . month( date( 'n', strtotime( $today ) ) ) . ' ' . date( 'Y.' );
								$post_types = array(
									'kafane_beograd',
									'klubovi_beograd',
									'splavovi_beograd',
									'restorani_beograd',
									'barovi_beograd'
								)
								?>
								<p class="nightout_date">7. JUN 2019.</p>
							</div>
							<a href="#" class="button yellow">Rezerviši</a>
						</div>
						<div class="nightout_content_container">
							<button type="button" class="nightout_toggler" data-target="#nightout_content" data-scroll="down"></button>
							<div class="nightout_content custom-flex-row" id="nightout_content">
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Stefan-Braun-logo.jpg" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Stefan Braun</h2>
										<p class="custom-heading-3">Balkan Hits - Mask off (closing week)</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Klub-Gotik-logo-1.png" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Gotik</h2>
										<p class="custom-heading-3">Balkan Hits - Mask off (closing week)</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Ona-Moja-logo.jpg" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Kafana Ona Moja</h2>
										<p class="custom-heading-3">Hitovi kao nekada - DJ Luka</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/02/The-Bank-logo.png" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">The Bank</h2>
										<p class="custom-heading-3">Hitovi kao nekada - DJ Luka</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2019/09/Bar-Cross-logo.jpg" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Cross</h2>
										<p class="custom-heading-3">Miloš Zeka Petrović od 21h</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/klub-hype-logo.png" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Hype</h2>
										<p class="custom-heading-3">Miloš Zeka Petrović od 21h</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Ona-Moja-logo.jpg" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Kafana Ona Moja</h2>
										<p class="custom-heading-3">Hitovi kao nekada - DJ Luka</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Klub-Kasina-by-Community-logo.jpg"
										     alt="" class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Kasina</h2>
										<p class="custom-heading-3">Hitovi kao nekada - DJ Luka</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Stefan-Braun-logo.jpg" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Stefan Braun</h2>
										<p class="custom-heading-3">Balkan Hits - Mask off (closing week)</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Klub-Gotik-logo-1.png" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Gotik</h2>
										<p class="custom-heading-3">Balkan Hits - Mask off (closing week)</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Ona-Moja-logo.jpg" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Kafana Ona Moja</h2>
										<p class="custom-heading-3">Hitovi kao nekada - DJ Luka</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/02/The-Bank-logo.png" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">The Bank</h2>
										<p class="custom-heading-3">Hitovi kao nekada - DJ Luka</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2019/09/Bar-Cross-logo.jpg" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Cross</h2>
										<p class="custom-heading-3">Miloš Zeka Petrović od 21h</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/klub-hype-logo.png" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Hype</h2>
										<p class="custom-heading-3">Miloš Zeka Petrović od 21h</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Ona-Moja-logo.jpg" alt=""
										     class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Kafana Ona Moja</h2>
										<p class="custom-heading-3">Hitovi kao nekada - DJ Luka</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>
								<div class="nightout_item custom-flex-row">
									<div class="nightout_image">
										<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Klub-Kasina-by-Community-logo.jpg"
										     alt="" class="img-fluid">
									</div>
									<div class="nightout_text">
										<h2 class="nightout_title">Kasina</h2>
										<p class="custom-heading-3">Hitovi kao nekada - DJ Luka</p>
									</div>
									<a href="#" class="button yellow">Rezerviši</a>
								</div>

							</div>
							<button type="button" class="nightout_toggler" data-target="#nightout_content" data-scroll="up"></button>
						</div>
					</div>
				</div>
				<aside class="col-12 col-lg-3 homepage_sidebar">
					<div class="homepage_sidebar_item">
						<img src="@asset('images/blackrosevip.png')" alt="" class="img-fluid">
						<a href="#">Black Rose Vip</a>
					</div>
					<div class="homepage_sidebar_item">
						<img src="@asset('images/hollywoodland.png')" alt="" class="img-fluid">
						<a href="#">Hollywoodland</a>
					</div>
				</aside>
			</div>
		</div>
		@include('partials.modal')
	</div>
	@include('partials.reservation')
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-8">
				<div class="date_picker_filter custom-mb-big">
					<p class="custom-heading-3">Program za noćni život u Beogradu - maj 2019.</p>
					@include('partials.lokal-list-date')
					<div class="date_picker_type_lokal">
						<p class="custom-heading-3">Izaberite provod po vašoj želji</p>
						<div class="date_picker_type_lokal_radio custom-flex-row">
							<div class="custom_radio_container">
								<input type="radio" name="lokal" id="dinner" class="custom-radio" value="">
								<label for="dinner">Muzika sa večerom</label>
							</div>
							<div class="custom_radio_container">
								<input type="radio" name="lokal" id="clubbing" class="custom-radio" value="">
								<label for="clubbing">Clubbing</label>
							</div>
							<div class="custom_radio_container">
								<input type="radio" name="lokal" id="kafane" class="custom-radio" value="">
								<label for="kafane">Kafane</label>
							</div>
							<div class="custom_radio_container">
								<input type="radio" name="lokal" id="restaurants" class="custom-radio" value="">
								<label for="restaurants">Restorani</label>
							</div>
							<div class="custom_radio_container">
								<input type="radio" name="lokal" id="plays" class="custom-radio" value="">
								<label for="plays">Svirke</label>
							</div>
						</div>
					</div>
					<div class="date_picker_advanced_search" id="date_picker_advanced_search_boxes">
						<p class="custom-heading-3" data-open="#date_picker_advanced_search_boxes" data-function="class">Detaljna pretraga</p>
						<div class="date_picker_boxes custom-flex-row">
							<div class="column">
								<p class="custom-heading-4">Muzika</p>
								<div class="custom_checkbox_container">
									<input type="checkbox" id="live" class="custom_checkbox">
									<label for="live">Bend uživo</label>
								</div>
								<div class="custom_checkbox_container">
									<input type="checkbox" id="ninety" class="custom_checkbox">
									<label for="ninety">Devedesete</label>
								</div>
								<div class="custom_checkbox_container">
									<input type="checkbox" id="dj" class="custom_checkbox">
									<label for="dj">DJ</label>
								</div>
								<div class="custom_checkbox_container">
									<input type="checkbox" id="house" class="custom_checkbox">
									<label for="house">House</label>
								</div>
								<div class="custom_checkbox_container">
									<input type="checkbox" id="jazz" class="custom_checkbox">
									<label for="jazz">Jazz</label>
								</div>
								<div class="custom_checkbox_container">
									<input type="checkbox" id="folk" class="custom_checkbox">
									<label for="folk">Narodna</label>
								</div>
								<div class="custom_checkbox_container">
									<input type="checkbox" id="pop" class="custom_checkbox">
									<label for="pop">Pop</label>
								</div>
							</div>
							<div class="column custom-flex-row">
								<p class="custom-heading-4">Deo grada</p>
								<?php
								$terms = get_terms( [
									'taxonomy'   => 'deo_grada',
									'hide_empty' => true,
								] );
								foreach ($terms as $term) { ?>

								<div class="custom_checkbox_container">
									<input type="checkbox" id="{{ $term->term_id  }}" class="custom_checkbox">
									<label for="{{ $term->term_id  }}">{{ $term->name  }}</label>
								</div>
								<?php  } ?>
							</div>
							<span class="loader">
                                <svg class="spinner">
                                    <circle class="path" cx="75" cy="75" r="55" fill="none" stroke-width="20"></circle>
                                </svg>
                            </span>
						</div>
						<button type="button" class="close" data-close="#date_picker_advanced_search_boxes" data-function="class">Zatvori</button>
					</div>

					<div class="date_picker_filter_content lokali_cards custom-flex-row show">
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2019/09/Bar-Cross-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Cross Bar</h2>
									<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Ellingtons-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Toro Restoran</h2>
									<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2019/09/Splav-Leto-logo-1-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Splav Leto</h2>
									<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2019/09/Klub-Magic-Diamond-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Magic Diamond klub</h2>
									<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Half-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Na Vodi kafana</h2>
									<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
					</div>

					<div class="date_picker_filter_content lokali_cards custom-flex-row">
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Ellingtons-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Ellington`s Bar</h2>
									<p class="info">NEMANJA STALETOVIĆ I ALEKSANDRA MARJANOVIĆ</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2019/07/Klub-Magic-Diamond-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Klub Magic Diamond</h2>
									<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Half-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Klub Half</h2>
									<p class="info">DJ RUŽA (DEVEDESETE I DVEHILJADITE)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Baraka-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Baraka klub</h2>
									<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="/klubovi-beograd/tranzit-bar">
									<picture>
										<img src="/wp-content/uploads/2020/03/Tranzit-Basta-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Tranzit Bar</h2>
									<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Kafana-Tarapana-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Klub Kafana Tarapana</h2>
									<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
					</div>

					<div class="date_picker_filter_content lokali_cards custom-flex-row">
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Ellingtons-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Ellington`s Bar</h2>
									<p class="info">NEMANJA STALETOVIĆ I ALEKSANDRA MARJANOVIĆ</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2019/07/Klub-Magic-Diamond-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Klub Magic Diamond</h2>
									<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Half-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Klub Half</h2>
									<p class="info">DJ RUŽA (DEVEDESETE I DVEHILJADITE)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Baraka-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Baraka klub</h2>
									<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Tranzit-Basta-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Tranzit Bar</h2>
									<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Kafana-Tarapana-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Klub Kafana Tarapana</h2>
									<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
					</div>

					<div class="date_picker_filter_content lokali_cards custom-flex-row">
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Ellingtons-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Ellington`s Bar</h2>
									<p class="info">NEMANJA STALETOVIĆ I ALEKSANDRA MARJANOVIĆ</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2019/07/Klub-Magic-Diamond-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Klub Magic Diamond</h2>
									<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Half-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Klub Half</h2>
									<p class="info">DJ RUŽA (DEVEDESETE I DVEHILJADITE)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Baraka-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Baraka klub</h2>
									<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Tranzit-Basta-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Tranzit Bar</h2>
									<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
						<div class="lokali_card">
							<div class="lokali_card_image">
								<a href="#">
									<picture>
										<img src="/wp-content/uploads/2020/03/Klub-Kafana-Tarapana-logo-380x275.jpg" alt="" class="img-fluid">
									</picture>
								</a>
							</div>
							<div class="lokali_card_text">
								<a href="#">
									<h2 class="lokali_card_heading">Klub Kafana Tarapana</h2>
									<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
								</a>
								<a href="#" class="button yellow">Rezerviši online</a>
								<a href="tel:38163343433" class="button blue">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
										<path fill="#fff"
										      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
									</svg>
									063 34 34 33</a>
							</div>
						</div>
					</div>

					<p class="more_lokal custom-flex-row">Još mesta gde možete izaći</p>
					@include('partials.lokali-cards')
					<div class="main-content custom-mb-big">
						{!! get_the_content() !!}
					</div>
				</div>

			</div>
			@include('partials.sidebar')
		</div>
	</div>
	@include('partials.reservation')
	<div class="container">
		<div class="row">
			@include('partials.featured')
		</div>
	</div>
@endsection
