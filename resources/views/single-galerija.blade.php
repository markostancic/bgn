@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			@include('partials.breadcrumb')
			<div class="col-12 col-lg-8 single_gallery">
				<div class="gallery_hero">
					<p class="gallery_date"><?php the_field( 'datum_galerije' )  ?></p>
					<?php
					$lokal = get_field( 'lokal' );
					$lokal_id = $lokal->ID;
					?>
					<h1>Slike iz provoda - {{ $lokal->post_title  }}</h1>
					<a href="{{ get_permalink($lokal_id)  }}">{{$lokal->post_title}} - poseti stranicu</a>
				</div>
				@if(get_the_content())
					<div class="main-content custom-mb-small">
						{!! get_the_content() !!}
					</div>
				@endif

				<div class="gallery">
					<?php
					$images = get_field( 'galerija' );
					$size = 'galerija-top';
					if ($images) :
					foreach ($images as $image) : ?>
					<div class="gallery__item">
						<a href="{{ $image['url']  }}" data-lightbox="roadtrip">
							<img src="<?php echo $image[ 'sizes' ][ $size ]; ?>" alt="">
						</a>
					</div>
					<?php endforeach;
					endif; ?>
				</div>
				@include('partials.pagination')
				@include('partials.datepicker')
			</div>
			@include('partials.sidebar')
		</div>
	</div>
	@include('partials.reservation')
	<div class="container">
		<div class="row">
			@include('partials.featured')
		</div>
	</div>
@endsection
