@extends('layouts.app')

@section('content')
  <div class="container single-magazine">
    <div class="row">
      @include('partials.breadcrumb')
      <div class="col-12 col-lg-8">
        <div class="post-content main-content custom-mb-big">
          <h1>{{ the_title() }}</h1>
          @include('partials.content-page')
        </div>
      </div>
      @include('partials.sidebar')
    </div>
  </div>
@endsection
