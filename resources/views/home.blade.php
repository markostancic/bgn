<!-- Prikazuje sve wordpress postove iz svih kategorija -->
@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			@include('partials.breadcrumb')
			<div class="col-12 col-lg-8 custom-mb-big">
				<?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                if (have_posts()) :
                $count = 0;
                while (have_posts()) : the_post();
                $post_tags = get_the_tags();
                $post_image = get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : get_template_directory_uri() . '/assets/images/img_holder.jpg';
                if ($count > 0) : ?>
                <div class="magazin-post custom-flex-row">
                    <div class="post-image">
                        <a href="{{ get_permalink() }}">
                            <picture>
                                <?php get_webp_image_type([ $post_image => '' ], '', 1) ?>
                                <img src="{{ $post_image }}" alt="{!! get_the_title() !!}">
                            </picture>
                        </a>
                    </div>
                    <div class="post-info">
                        <a href="{{ get_permalink() }}" class="custom_card_link custom-flex-row">
                            <h2 class="post-title">{!! get_the_title() !!}</h2>
                            <p class="post-date">{{ get_the_date() }}</p>
                        </a>
                        @if($post_tags)
                            <a href="{{ get_tag_link($post_tags[0]->term_id) }}" class="button post-category">{!! $post_tags[0]->name !!}</a>
                        @endif
                    </div>
                </div>
                <?php else : ?>
                <div class="featured-post">
                    <div class="post-image">
                        <a href="{{ get_permalink() }}">
                            <picture>
                                <?php get_webp_image_type([$post_image => ''], '', 1); ?>
                                <img src="{{ $post_image }}" alt="" class="img-fluid">
                            </picture>
                        </a>
                    </div>
                    <div class="post-info">
                        <h2 class="post-title">
                            <a href="{{ get_permalink() }}">{!! get_the_title() !!}</a>
                        </h2>
                        <p class="post-date">{{ get_the_date() }}</p>
                        @if($post_tags)
                            <div class="post-categories">
                                @foreach($post_tags as $tag)
                                    <a href="{{ get_tag_link($tag->term_id) }}" class="button post-category">{!! $tag->name !!}</a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <?php endif;
                $count++;
                endwhile;
                wp_reset_postdata();
                endif;
                ?>
                <?php global $wp_query; $custom_query = $wp_query; ?>
                @include('partials.pagination')
            </div>
            @include('partials.sidebar')
        </div>
    </div>
    @include('partials.reservation')
    <div class="container">
        <div class="row">
            @include('partials.tekstovi')
            @include('partials.gallery')
            @include('partials.featured')
        </div>
    </div>
@endsection
