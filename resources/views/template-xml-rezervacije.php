<?php
/*
Template Name: XML Lista Rezervacija
*/

header("Content-type: text/xml");
echo "<?xml version='1.0' encoding='UTF-8'?>";
echo '<Events>';


$table_name = $wpdb->prefix . "dogadjaji_bgn";
$table_name2 = $wpdb->prefix . "dogadjaji_bgn_ns";

$datum = date('Y-m-d');
$listalokar = $listaposttype;

$daylist = array(
    $datum,
    date("Y-m-d", strtotime("$datum +1 day")),
    date("Y-m-d", strtotime("$datum +2 day")),
    date("Y-m-d", strtotime("$datum +3 day")),
    date("Y-m-d", strtotime("$datum +4 day")),
    date("Y-m-d", strtotime("$datum +5 day")),
    date("Y-m-d", strtotime("$datum +6 day"))
);

$listaposttype = array(
    'klubovi' => array(),
    'splavovi' => array(),
    'kafane' => array(),
    'barovi' => array(),
    'kafei' => array(),
    'restorani' => array(),
    'striptizbarovi' => array(),
    'desavanja' => array(),
    'kazina' => array()
);

$listas = array();


foreach ($daylist as $day) {
    $loc = array(); // lista za jedan dan lokala koji su pronadjeni u jednom danu
    // Preuzimanje i reorder za dati dan
    $query1 = 'SELECT id_lokal, id_dogadjaja, tip_lokala FROM '.$table_name.' WHERE date="'. $day.'"';// OR ('     GROUP BY "tip_lokala"
    $result1 = $wpdb->get_results($query1, ARRAY_A); //$result1 = objectToArray($result1);
    
    
    $da = explode('-', $day);
    $danecho = $da[2] . '.' . $da[1] . '.' . $da[0];
    
    
    if ($result1) {
        foreach ($result1 as $fe) {
            $loc[]=$fe['id_lokal'];
            
            $listloc = array(
                'id_dogadjaja'  => $fe['id_dogadjaja'],
                'id_lokal'      => $fe['id_lokal'],
                'date'          => $danecho
            );
            array_push($listaposttype[$fe['tip_lokala']], $listloc);
        }
        
        $IDS = esc_sql(implode(',', $loc));
    }
    
    
    
    $query11 = "SELECT id_lokal, id_dogadjaja, tip_lokala FROM ".$table_name2." WHERE ";
    if ($result1) {
        $query11 .= "id_lokal NOT IN ('$IDS') AND";
    }
    $query11 .= "(tip_repetition = 'd' OR danu_nedelji = '". date('N', strtotime($day)) . "' AND tip_repetition = 'w') AND start_date <='" . $day . "'";//(danu_nedelji = "'. date('N', strtotime($date1)) . '" )
    $result11 = $wpdb->get_results($query11, ARRAY_A);

    if ($result11) {
        foreach ($result11 as $fe) {
            $listloc = array(
                'id_dogadjaja' => $fe['id_dogadjaja'],
                'id_lokal'      => $fe['id_lokal'],
                'date'          => $danecho
            );
            array_push($listaposttype[$fe['tip_lokala']], $listloc);
        }
    }
}

//$resultm1 = array_merge((array)$result1, (array)$result11);
//print_r($result11);
//print_r($result1);
//print_r($listaposttype);


foreach ($listaposttype as $tiplokala => $dan) {
    echo "<" . $tiplokala . "Event>";
    
    foreach ($dan as $lokal) {
            echo '<Event>';
            echo '<Location>' . get_the_title($lokal['id_lokal']) . '</Location>'; // Naziv KLUBA gde se odrzava dogadjaj
            echo '<Date>' . $lokal['date'] . '.</Date>'; // Datum kad je dogadjaj (format: dd.mm.yyyy.)
            echo '<Description>' . get_the_title($lokal['id_dogadjaja']) . '</Description>'; // Naziv dogadjaja
            $tipsed = get_post_meta($lokal['id_dogadjaja'], 'beogradnocu_tipsedenja_event', true);
            if ($tipsed != '') {
                $araysede = explode(',', $tipsed);
                foreach ($araysede as $sed) {
                    echo '<Table>' . $sed . '</Table>'; // Lista stolova iz dogadjaja
                }
            }
            echo '</Event>';
    }
    echo "</" . $tiplokala . "Event>";
}

echo '</Events>';
