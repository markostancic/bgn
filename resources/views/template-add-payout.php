<?php

/*
Template Name: Add Payout
*/

$title = "Unos promocija za mobilne aplikacije";

?>

<!DOCTYPE HTML>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>Unos mesta gde mogu da se potroše bodovi</title>
    <style>
    div {
        margin-left: 100px;
        margin-right: 40px;
    }
    </style>
</head>
<body>
    <?php
  $servername = "127.0.0.1";
  $username = "root";
  $password = "";
  $dbname = "nocu_app";

        $start_time = $end_time = $promo_start = $promo_day = "";
        $place_id = '';
        $description = '';
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            $place_id = $_GET['lokal'];
            $description = $_GET['description'];

            if (!empty($description)) {
                $sql = "INSERT INTO payout (place_id, description, uuid) VALUES ('". $place_id. "','". $description. "', UUID())";
                try {
                    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                } catch (PDOException $e) {
                    echo "Error: " . $e->getMessage();
                }
            }
        }
    ?>

    <div>
        <h2>Unos mesta gde mogu da se potroše bodovi</h2>
        <form>
            <select name="lokal">
            <?php
                query_posts(
                    array(
                    'post_type'=> array(
                        'klubovi',
                        'splavovi',
                        'kafane',
                        'barovi',
                        'kafei',
                        'restorani',
                        'striptizbarovi'
                    ),
                    'orderby'=>'title',
                    'order'=>'ASC',
                    'posts_per_page'=>'-1'
                    )
                );
                $listaLokala = array(
                    'lokali' => array()
                );
                if (have_posts()) :
                    while (have_posts()) : the_post();
                        $beogradnocu_prikazilokal = get_post_meta($post->ID, 'beogradnocu_prikazivanjelokala', true);
                        if ($beogradnocu_prikazilokal === '') {
                            $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                            $aboutLokal = array(
                                'name' => htmlspecialchars($post->post_title),
                                'id' => $post->ID
                            );
                            echo "<option value=".$aboutLokal['id'].">" . $aboutLokal['name'] . "</option>";
                        }
                    endwhile;
                endif;
            ?>
            </select><br><br>
            Opis:<br><textarea name="description" rows="5" cols="80"></textarea><br><br>
            <input type="submit" name="submit" value="Sačuvaj">
        </form>
    </div>
</body>
</html>
