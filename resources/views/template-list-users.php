<?php

/*
Template Name: App List Users
*/

?>

<!DOCTYPE HTML>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <title>Pregled korisnika</title>
  <style>
    div {
      margin-left: 100px;
      margin-right: 40px;
    }
    table, th, td {
      border: 1px solid black;
    }
  </style>
</head>
<body>
<?php
$m1Filter = '';
$m2Filter = '';
$m3Filter = '';
$m4Filter = '';
$m5Filter = '';
$m6Filter = '';
$d1Filter = '';
$d2Filter = '';
$d3Filter = '';
$d4Filter = '';
$smokerFilter = 'false';
if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if (isset($_GET['m1'])) {
    $m1Filter = '"m1"';
  }
  if (isset($_GET['m2'])) {
    $m2Filter = '"m2"';
  }
  if (isset($_GET['m3'])) {
    $m3Filter = '"m3"';
  }
  if (isset($_GET['m4'])) {
    $m4Filter = '"m4"';
  }
  if (isset($_GET['m5'])) {
    $m5Filter = '"m5"';
  }
  if (isset($_GET['m6'])) {
    $m5Filter = '"m6"';
  }
  if (isset($_GET['d1'])) {
    $d1Filter = '"d1"';
  }
  if (isset($_GET['d2'])) {
    $d2Filter = '"d2"';
  }
  if (isset($_GET['d3'])) {
    $d3Filter = '"d3"';
  }
  if (isset($_GET['d4'])) {
    $d4Filter = '"d4"';
  }
  if (isset($_GET['smoker'])) {
    if ($_GET['smoker'] == 'all') {
      $smokerFilter = 'smoker';
    } elseif ($_GET['smoker'] == 'true') {
      $smokerFilter = 'true';
    } else {
      $smokerFilter = 'false';
    }
  } else {
    $smokerFilter = 'smoker';
  }
}
?>
<div>
  <h2>Pregled korisnika</h2>
  <form>
    Muzika<br>
    <input type="checkbox" name="m1" value="Folk">Folk
    <input type="checkbox" name="m2" value="Pop">Pop
    <input type="checkbox" name="m3" value="Rock">Rock
    <input type="checkbox" name="m4" value="HouseRnb">House/RNB
    <input type="checkbox" name="m6" value="Techno">Techno
    <input type="checkbox" name="m5" value="Ostalo">Ostalo<br>
    Piće<br>
    <input type="checkbox" name="d1" value="Pivo">Pivo
    <input type="checkbox" name="d2" value="Vino">Vino
    <input type="checkbox" name="d3" value="Žestina">Žestina
    <input type="checkbox" name="d4" value="Bezalkoholno piće">Bezalkoholno piće<br>
    Pušač<br>
    <select name="smoker">
      <option value="all">Svi</option>
      <option value="true">Pušači</option>
      <option value="false">Nepušači</option>
    </select>
    <input type="submit" name="submit" value="Primeni"><br><br>
  </form>
  <?php echo $smokerFilter; ?>
  <table id='table1' style="width:100%">
    <tr>
      <th>Korisnik</th>
      <th>Poeni</th>
      <th>Godine</th>
      <th>R</th>
      <th>R10</th>
      <th>R30</th>
      <th>R100</th>
      <th>PP</th>
      <th>C</th>
      <th>PR</th>
      <th>Mobilni</th>
      <th>Muzika</th>
      <th>Piće</th>
      <th>Pušač</th>
      <th>Kreiran</th>
      <th>Rođendan</th>
    </tr>
    <?php
    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "nocu_app";

    $sql = "SELECT * FROM users WHERE (q LIKE '%".$m1Filter."%') AND (q LIKE '%".$m2Filter."%') AND (q LIKE '%".$m3Filter."%') AND (q LIKE '%".$m4Filter."%') AND (q LIKE '%".$m5Filter."%') AND (q LIKE '%".$m6Filter."%') AND (q LIKE '%".$d1Filter."%') AND (q LIKE '%".$d2Filter."%') AND (q LIKE '%".$d3Filter."%') AND (q LIKE '%".$d4Filter."%') AND (q LIKE '%".$smokerFilter."%')";
    try {
      $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $rows = $stmt->fetchAll();
      foreach ($rows as $row) {
        $user = array();
        $user['name'] = $row['name'];
        $user['score'] = $row['score'];
        $user['mobile'] = $row['mobile'];
        $user['created_at'] = $row['created_at'];
        $user['birthday'] = $row['birthday'];
        $user['uuid'] = $row['uuid'];
        $user['r'] = $row['r'];
        $user['r10k'] = $row['r10k'];
        $user['r30k'] = $row['r30k'];
        $user['r100k'] = $row['r100k'];
        $user['invites'] = $row['invites'];
        $user['checkins'] = $row['checkins'];
        $user['bills'] = $row['bills'];
        if (empty($row['q'])) {
          $user['q'] = null;
        } else {
          $user['q'] = json_decode($row['q']);
        }
        $music = '';
        $drinks = '';
        $age = '-';
        $smoker = "-";
        if ($user['q'] != null) {
          $anketa = $user['q'];
          foreach ($anketa->music as $value) {
            if ($value == 'm1') {
              $music .= 'Folk ';
            } elseif ($value == 'm2') {
              $music .= 'Pop ';
            } elseif ($value == 'm3') {
              $music .= 'Rock ';
            } elseif ($value == 'm4') {
              $music .= 'House/RNB. ';
            } elseif ($value == 'm5') {
              $music .= 'Ostalo ';
            } elseif ($value == 'm6') {
              $music .= 'Techno ';
            }
          }
          foreach ($anketa->drinks as $value) {
            if ($value == 'd1') {
              $drinks .= 'Pivo ';
            } elseif ($value == 'd2') {
              $drinks .= 'Vino ';
            } elseif ($value == 'd3') {
              $drinks .= 'Zestina ';
            } elseif ($value == 'd4') {
              $drinks .= 'Bezalk. ';
            }
          }
          $age = $anketa->age;
          if ($anketa->smoker) {
            $smoker = 'jeste';
          } else {
            $smoker = 'nije';
          }
        }

        echo '<tr>';
        echo '<td>'.$user['name'].'</td>';
        echo '<td><a href="https://www.beogradnocu.com/app-users/?fixScore='.$user['uuid'].'">'.$user['score'].'</a></td>';
        echo '<td><a href="https://www.beogradnocu.com/app-users/?fix='.$user['uuid'].'">'.$age.'</a></td>';
        echo '<td>'.$user['r'].'</td>';
        echo '<td>'.$user['r10k'].'</td>';
        echo '<td>'.$user['r30k'].'</td>';
        echo '<td>'.$user['r100k'].'</td>';
        echo '<td>'.$user['invites'].'</td>';
        echo '<td>'.$user['checkins'].'</td>';
        echo '<td>'.$user['bills'].'</td>';
        echo '<td>'.$user['mobile'].'</td>';
        echo '<td>'.$music.'</td>';
        echo '<td>'.$drinks.'</td>';
        echo '<td>'.$smoker.'</td>';
        echo '<td>'.$user['created_at'].'</td>';
        echo '<td>'.$user['birthday'].'</td>';
        echo '</tr>';
      }
    } catch (Exception $e) {
      echo '<tr>';
      echo '<td>Greska</td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '<td></td>';
      echo '</tr>';
    }
    ?>
  </table>
</div>
<br>
<br>
</body>
</html>
