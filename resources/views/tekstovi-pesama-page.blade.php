{{--
Template Name: Tekstovi pesama zbirna
--}}

@extends('layouts.app')

@section('content')
  <div class="container single-magazine">
    <div class="row">
      @include('partials.breadcrumb')
      <div class="col-12 col-lg-8">
        <div class="hero magazine custom-flex-row">
          <div class="hero-text">
            <h1>{{ the_title() }}</h1>
            <div class="description">
              {{ the_content() }}
            </div>
          </div>
        </div>
        @include('partials.najpoznatije_pesme')
        <div class="hero custom-mb-xs">
          <div class="hero-text">
            @include('partials.najpopularniji_izvodjaci')
            @include('partials.pesme_po_zanrovima')
          </div>
        </div>
        <div class="description">
          {{ the_field('detailed_description') }}
        </div>
        @include('partials.tekstovi_copyright')
      </div>
      @include('partials.sidebar')
    </div>
  </div>
@endsection
