{{--
  Template Name: App Landing page
--}}

@extends('layouts.app')

@section('content')
  <div class="app-store">
    <div class="container">
      <div class="row">
        <div class="col-12 custom-flex-row">
          <h1>Beograd noću aplikacija</h1>
          <div class="app-store-container">
            <p>Preuzmi besplatno</p>
            @include('partials.app-links')
          </div>
        </div>
      </div>
    </div>
    <img src=""
         srcset="   @asset('images/B22_bgdn_LP_backgrounds-04-md.png') 991w,
                        @asset('images/B22_bgdn_LP_backgrounds-04.png') 1920w"
         alt="" class="img-fluid">
  </div>
  <div class="gradient">

    <?php if (have_rows( 'prva_sekcija' )) : while (have_rows( 'prva_sekcija' )) : the_row();
    $naslov = get_sub_field( 'naslov' );
    $podnaslov = get_sub_field( 'podnaslov' );
    $tekst = get_sub_field( 'tekst' );
    $slika = get_sub_field( 'slika' );
    $tekst_slika = get_sub_field( 'tekst_slike' );
    ?>
    <div class="blue-container">
      <div class="container">
        <div class="row">
          <div class="col-12 custom-flex-row">
            <div class="column wide">
              <p class="custom-heading-2">{{ $naslov }}</p>
              <p class="custom-heading-3">{{ $podnaslov }}</p>
              <div class="rank-text">
                {!! $tekst !!}
              </div>
              <div class="ranks custom-flex-row">
                <?php if (have_rows( 'clanovi' )) : while (have_rows( 'clanovi' )) : the_row();?>
                <div class="rank">
                  <div class="rank__image">
                    <img src="" data-srcset="{{ the_sub_field('slika') }}" alt="" class="img-fluid ">
                  </div>
                  <p>{{ the_sub_field('naziv') }}</p>
                  <p>{{ the_sub_field('poeni') }}</p>
                </div>
                <?php endwhile; endif; ?>

              </div>
            </div>
            <div class="column narrow get-app">
              <img src="" data-srcset="{{ $slika }}" alt="" class="img-fluid  marginl-auto">
              <p>{{ $tekst_slika }}</p>
              @include('partials.app-links')
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endwhile; endif; ?>

    <div class="gradient-image__green">
      <img src="@asset('images/B22_bgdn_LP_backgrounds-01.png')" alt="" class="non-fluid">
    </div>

    <?php if (have_rows( 'druga_sekcija' )) : while (have_rows( 'druga_sekcija' )) : the_row();
    $naslov = get_sub_field('naslov');
    $podnaslov = get_sub_field('podnaslov');
    $tekst = get_sub_field('tekst');
    $slika = get_sub_field('slika');
    $tekst_slika = get_sub_field('tekst_slike');
      ?>
    <div class="yellow-container">
      <div class="container">
        <div class="row">
          <div class="col-12 custom-flex-row">
            <div class="column narrow get-app">
              <img src="" data-srcset="{{ $slika }}" alt="" class="img-fluid  marginr-auto">
              <p>{{ $tekst_slika }}</p>
              @include('partials.app-links')
            </div>
            <div class="column wide offers">
              <p class="custom-heading-2">{{ $naslov }}</p>
              <p class="text-offer">{{ $podnaslov }}</p>
              <?php if ( have_rows( 'poeni' ) ) : while ( have_rows( 'poeni' ) ) : the_row();
              $naziv = get_sub_field( 'naziv' );
              $broj = get_sub_field( 'poeni' )
              ?>
              <div class="offer custom-flex-row">
                <p>{{ $naziv }}</p>
                <p>{{ $broj }} <img src="" data-srcset="@asset('images/B22_bgdn_LP_buttons-01-03.png')" alt=""
                                    class="img-fluid "></p>
              </div>
              <?php endwhile; endif; ?>

              @if($tekst)
                <div class="additional-info custom-flex-row">
                  <img src="" data-srcset="@asset('images/B22_bgdn_LP_buttons-01-03.png')" alt="" class="img-fluid ">
                  {!! $tekst !!}
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endwhile; endif; ?>

    <div class="gradient-image__orange">
      <img src="" data-srcset="@asset('images/B22_bgdn_LP_backgrounds-02.png')" alt="" class="non-fluid ">
    </div>

    <?php if (have_rows( 'treca_sekcija' )) : while (have_rows( 'treca_sekcija' )) : the_row();
    $naslov = get_sub_field( 'naslov' );
    $podnaslov = get_sub_field( 'podnaslov' );
    $tekst = get_sub_field( 'tekst' );
    $slika = get_sub_field( 'slika' );
    $tekst_slika = get_sub_field( 'tekst_slike' );
    ?>
    <div class="purple-container">
      <div class="container">
        <div class="row">
          <div class="col-12 custom-flex-row">
            <div class="column wide offers">
              <p class="custom-heading-2">{{ $naslov }}</p>
              <p class="text-offer">{{ $podnaslov }}</p>
              <?php if ( have_rows( 'poeni' ) ) : while ( have_rows( 'poeni' ) ) : the_row();
              $naziv = get_sub_field( 'naziv' );
              $broj = get_sub_field( 'poeni' ) ?>
              <div class="offer custom-flex-row">
                <p>{{ $naziv }}</p>
                <p>{{ $broj }} <img src="" data-srcset="@asset('images/B22_bgdn_LP_buttons-01-03.png')" alt=""
                                    class="img-fluid "></p>
              </div>
              <?php endwhile; endif; ?>

              <div class="additional-info">
                {!! $tekst !!}
              </div>
            </div>
            <div class="column narrow get-app">
              <img src="" data-srcset="{{ $slika }}" alt="" class="img-fluid  marginr-auto">
              <p>{{ $tekst_slika }}</p>
              @include('partials.app-links')
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endwhile; endif; ?>

  </div>
@endsection
