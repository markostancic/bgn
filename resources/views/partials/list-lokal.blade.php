<div class="col-12 custom-mb-big">
	<div class="lokal-list" id="lokal-listing">
		<div class="lokal-list-heading custom-flex-row">
			<p class="custom-heading-4" data-togle="#lokal-listing" data-function="class">Lista svih klubova</p>
			<a href="#" class="button yellow">Rezerviši online</a>
		</div>
		<div class="lokal-list-content">
			<?php
			$query = new WP_Query( [
				'post_type'      => 'klubovi_beograd',
				'posts_per_page' => -1,
				'tax_query'      => [
					'relation' => 'AND',
					[
						'taxonomy' => 'vrsta_listinga',
						'field'    => 'name',
						'terms'    => 'klubovi',
					]
				],
			] );
			?>
			@if ( $query->have_posts() )
				@while ( $query->have_posts() )
					@php $query->the_post(); @endphp
					<a href="{{ get_permalink() }}">{!! get_the_title() !!}</a>
				@endwhile
			@endif
			@php wp_reset_postdata(); @endphp
		</div>
		<div class="custom-flex-row closing__list">
			<a href="#" class="button yellow">Rezerviši online</a>
			<button type="button" class="button btn-close" data-close="#lokal-list" data-function="class"></button>
		</div>
	</div>
</div>
