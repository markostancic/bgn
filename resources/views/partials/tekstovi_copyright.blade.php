<div class="custom-mt-xs tekstovi_copyright">
  <div class="main-content">
    <div class="custom-card custom-flex-row">
      <div class="text">
        <p>Tekstovi pesama su u vlasništvu njihovih autora i prikazani su isključivo u edukativne svrhe. <br> Ovde ne postoji mogućnost preuzimanja pesama u audio/video formatu niti mogućnost preslušavanja.
        </p>
      </div>
    </div>
  </div>
</div>
