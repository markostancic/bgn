@if(get_field('android_app_link', 'option'))
    <a href="{{ get_field('android_app_link', 'option') }}" target="_blank" rel="nofollow noopener">
        <img src="" data-srcset="@asset('images/beograd_nocu__sidebar_google_play.svg')" alt="">
    </a>
@endif
@if(get_field('ios_app_link', 'option'))
    <a href="{{ get_field('ios_app_link', 'option') }}" target="_blank" rel="nofollow noopener">
        <img src="" data-srcset="@asset('images/beograd_nocu__sidebar_app_store.svg')" alt="">
    </a>
@endif
