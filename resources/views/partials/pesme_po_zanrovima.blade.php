<div class="similar_songs">
  <h2>Pesme po žanrovima</h2>
</div>
  <?php
  $terms = get_terms([
    'taxonomy' => 'zanr',
    'hide_empty' => false,
  ]);
  ?>
  <?php foreach( $terms as $term ): ?>
  <h3 class="similar_content_songs"><a class="text_category post-category" href="<?php echo esc_url( get_term_link( $term ) ); ?>"><?php echo esc_html( $term->name ); ?></a></h3>
  <?php endforeach; ?>
