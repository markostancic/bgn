<div class="col-12 gallery custom_cards custom-flex-row custom-mb-big">
  <div class="custom-card-heading custom-flex-row partials-heading">
    <h4 class="custom-heading-4">Najnovije galerije</h4>
    <a href="#" class="button">Sve galerije</a>
  </div>
  <!-- TODO ubaciti link za sve galerije lokala -->

  <div class="custom-card">
    <a href="#" class="custom-card-link custom-flex-row">
      <div class="custom-card-image">
        <picture>
          <img src=""
               data-srcset="https://bgn.avokado.dev/wp-content/uploads/2020/05/devojka-u-haljini-jelka-255x150.jpg"
               alt="">
        </picture>
      </div>
      <h3 class="custom-card-title">Splav Shake 'n' Shake</h3>
      <div class="custom-card-info">
        <p>Datum: 19.05.2019</p>
        <p>Broj foto: 32</p>
      </div>
    </a>
  </div>
  <div class="custom-card">
    <a href="#" class="custom-card-link custom-flex-row">
      <div class="custom-card-image">
        <picture>
          <img src=""
               data-srcset="https://bgn.avokado.dev/wp-content/uploads/2020/05/basta-kafic-stolovi-zelenilo-255x150.jpg"
               alt="">
        </picture>
      </div>
      <h3 class="custom-card-title">Splav Kartel</h3>
      <div class="custom-card-info">
        <p>Datum: 29.05.2019</p>
        <p>Broj foto: 12</p>
      </div>
    </a>
  </div>
  <div class="custom-card">
    <a href="#" class="custom-card-link custom-flex-row">
      <div class="custom-card-image">
        <picture>
          <img src=""
               data-srcset="https://bgn.avokado.dev/wp-content/uploads/2020/05/publika-stand-up-komedija-255x150.jpg"
               alt="">
        </picture>
      </div>
      <h3 class="custom-card-title">Kafana Ona Moja</h3>
      <div class="custom-card-info">
        <p>Datum: 09.05.2019</p>
        <p>Broj foto: 22</p>
      </div>
    </a>
  </div>
  <div class="custom-card">
    <a href="#" class="custom-card-link custom-flex-row">
      <div class="custom-card-image">
        <picture>
          <img src=""
               data-srcset="https://bgn.avokado.dev/wp-content/uploads/2020/05/boca-alkohol-covek-mamurluk-255x150.jpg"
               alt="">
        </picture>
      </div>
      <h3 class="custom-card-title">Kafana Mala Maca</h3>
      <div class="custom-card-info">
        <p>Datum: 19.05.2019</p>
        <p>Broj foto: 32</p>
      </div>
    </a>
  </div>


</div>
