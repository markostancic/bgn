@if(is_front_page() || is_page_template('views/front-page-statika.blade.php'))
	<!-- TODO ubaciti link za zanrove muzike -->

	<div class="lokali_cards custom-flex-row" id="Kafane">
		<h3 class="lokali_cards_heading" data-function="class" data-togle="#Kafane">Kafane Beograd</h3>
		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-krcma">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Krcma-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-krcma">
					<h2 class="lokali_card_heading">Kafana Krčma</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-tarapana">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Tarapana-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-tarapana">
					<h2 class="lokali_card_heading">Kafana Tarapana</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-sipaj-ne-pitaj">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Sipaj-Ne-Pitaj-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-sipaj-ne-pitaj">
					<h2 class="lokali_card_heading">Kafana Sipaj Ne Pitaj</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/kafane-beograd/na-vodi-kafana">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2019/07/Splav-Na-Vodi-Kafana-logo-1-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/kafane-beograd/na-vodi-kafana">
					<h2 class="lokali_card_heading">Na Vodi kafana</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-brodarac">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Brodarac-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-brodarac">
					<h2 class="lokali_card_heading">Kafana Brodarac</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-kajak">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Kajak-logo2-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-kajak">
					<h2 class="lokali_card_heading">Kafana Kajak</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-novo-burence">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Novo-Burence-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-novo-burence">
					<h2 class="lokali_card_heading">Kafana Novo Burence</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-kopakabana">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/Kafana-Kopakabana-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/kafane-beograd/kafana-kopakabana">
					<h2 class="lokali_card_heading">Kafana Kopakabana</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/kafane-beograd/u-kraju-kafana">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/04/U-Kraju-Kafana-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/kafane-beograd/u-kraju-kafana">
					<h2 class="lokali_card_heading">U Kraju Kafana</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>
	</div>
	<div class="lokali_cards custom-flex-row" id="Klubovi">
		<h3 class="lokali_cards_heading" data-function="class" data-togle="#Klubovi">Klubovi Beograd</h3>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/ellingtons-bar">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Klub-Ellingtons-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/ellingtons-bar">
					<h2 class="lokali_card_heading">Ellington`s Bar</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/klub-kafana-tarapana">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Klub-Kafana-Tarapana-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/klub-kafana-tarapana">
					<h2 class="lokali_card_heading">Klub Kafana Tarapana</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/magic-diamond">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2019/09/Klub-Magic-Diamond-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/magic-diamond">
					<h2 class="lokali_card_heading">Magic diamond</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/cloud-festivalska-platforma">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Cloud-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/cloud-festivalska-platforma">
					<h2 class="lokali_card_heading">Cloud Festivalska Platforma</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/ulaz-festival">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Ulaz-Festival-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/ulaz-festival">
					<h2 class="lokali_card_heading">Ulaz Festival</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/klub-magic-diamond">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2019/07/Klub-Magic-Diamond-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/klub-magic-diamond">
					<h2 class="lokali_card_heading">Klub Magic Diamond</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/klub-half">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Klub-Half-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/klub-half">
					<h2 class="lokali_card_heading">Klub Half</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/klub-baraka">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Klub-Baraka-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/klubovi-beograd/klub-baraka">
					<h2 class="lokali_card_heading">Baraka klub</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>
	</div>
	<div class="lokali_cards custom-flex-row" id="Splavovi">
		<h3 class="lokali_cards_heading" data-function="class" data-togle="#Splavovi">Splavovi Beograd</h3>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/leto">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2019/09/Splav-Leto-logo-1-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/leto">
					<h2 class="lokali_card_heading">Leto</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/belgrade-music-week">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Belgrade-Music-Week-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/belgrade-music-week">
					<h2 class="lokali_card_heading">Belgrade Music Week</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/belgrade-beer-fest">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2019/07/Belgrade-Beer-Fest-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/belgrade-beer-fest">
					<h2 class="lokali_card_heading">Belgrade Beer Fest</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-kucica">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Splav-Kucica-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-kucica">
					<h2 class="lokali_card_heading">Splav Kućica</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-bajka">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Splav-Bajka-logo-1-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-bajka">
					<h2 class="lokali_card_heading">Splav Bajka</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-kafana-uzbuna">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2013/03/Splav-Kafana-Uzbuna-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-kafana-uzbuna">
					<h2 class="lokali_card_heading">Splav Kafana Uzbuna</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-kartel">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2013/03/Splav-Kartel-logo-1-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-kartel">
					<h2 class="lokali_card_heading">Splav Kartel</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-bridge">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2020/03/Splav-Bridge-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-bridge">
					<h2 class="lokali_card_heading">Splav Bridge</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>


		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-na-vodi-kafana">
					<picture>
						<img src="https://bgn.avokado.ninja/wp-content/uploads/2019/07/Splav-Na-Vodi-Kafana-logo-1-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="https://bgn.avokado.ninja/splavovi-beograd/splav-na-vodi-kafana">
					<h2 class="lokali_card_heading">Splav Na Vodi Kafana</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>
	</div>

@else
	<div class="lokali_cards custom-flex-row">
		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="/klubovi-beograd/ellingtons-bar">
					<picture>
						<img src="/wp-content/uploads/2020/03/Klub-Ellingtons-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="/klubovi-beograd/ellingtons-bar">
					<h2 class="lokali_card_heading">Ellington`s Bar</h2>
					<p class="info">NEMANJA STALETOVIĆ I ALEKSANDRA MARJANOVIĆ</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>
		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="/klubovi-beograd/klub-magic-diamond">
					<picture>
						<img src="/wp-content/uploads/2019/07/Klub-Magic-Diamond-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="/klubovi-beograd/klub-magic-diamond">
					<h2 class="lokali_card_heading">Klub Magic Diamond</h2>
					<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>
		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="/klubovi-beograd/klub-half">
					<picture>
						<img src="/wp-content/uploads/2020/03/Klub-Half-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="/klubovi-beograd/klub-half">
					<h2 class="lokali_card_heading">Klub Half</h2>
					<p class="info">DJ RUŽA (DEVEDESETE I DVEHILJADITE)</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>
		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="/klubovi-beograd/klub-baraka">
					<picture>
						<img src="" data-srcset="/wp-content/uploads/2020/03/Klub-Baraka-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="/klubovi-beograd/klub-baraka">
					<h2 class="lokali_card_heading">Baraka klub</h2>
					<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>
		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="/klubovi-beograd/tranzit-bar">
					<picture>
						<img src="" data-srcset="/wp-content/uploads/2020/03/Tranzit-Basta-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="/klubovi-beograd/tranzit-bar">
					<h2 class="lokali_card_heading">Tranzit Bar</h2>
					<p class="info">Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>
		<div class="lokali_card">
			<div class="lokali_card_image">
				<a href="/klubovi-beograd/tranzit-bar">
					<picture>
						<img src="" data-srcset="/wp-content/uploads/2020/03/Klub-Kafana-Tarapana-logo-380x275.jpg" alt="" class="img-fluid">
					</picture>
				</a>
			</div>
			<div class="lokali_card_text">
				<a href="/klubovi-beograd/tranzit-bar">
					<h2 class="lokali_card_heading">Klub Kafana Tarapana</h2>
					<p class="info">DJ DIZEL DACHA (CLOSING WEEK)</p>
				</a>
				<a href="#" class="button yellow">Rezerviši online</a>
				<a href="tel:38163343433" class="button blue">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
						<path fill="#fff"
						      d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
					</svg>
					063 34 34 33</a>
			</div>
		</div>
	</div>
@endif


