<header id="header">
  <div class="main-header">
    <div class="container">
      <nav class="navbar navbar-expand-lg">
        @if(get_field('logo', 'option'))
          <a class="brand" href="{{ home_url('/') }}">
            <img src="{{ get_field('logo', 'option') }}" alt="" class="img-fluid">
          </a>
        @endif
        <div class="search-openner order-xl-3 order-sm-2">
          <button type="button" class="button" id="open-search">
            <img src="@asset('images/beograd_nocu__general_header_search.svg')" alt="" class="img-fluid">
          </button>
          {{--                {!! get_custom_search_form(false) !!}--}}
        </div>
        <div class="wpml-lang_current order-xl-4 order-sm-2">EN</div>
        <button class="navbar-toggler order-sm-2" type="button" data-togle="#main-navigation" data-function="class"
                aria-controls="#main-navigation" aria-expanded="false"
                aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse order-lg-2 order-sm-2" id="main-navigation">
          @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav']) !!}
          @endif
        </div>
      </nav>
    </div>
    <div class="mega-menu bg-deffer" id="servis"
         data-xl="{{ \App\asset_path('images/beograd_nocu_footer_bg_logo.svg') }}">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-2 no-heading mega_menu_item mobile_spacing_menu_item">
            <div>
              @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'menu_6','menu_class' => 'megamenu_links-spacing']) !!}
              @endif
            </div>
            <div>
              @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'menu_7','menu_class' => 'megamenu_links-spacing']) !!}
              @endif
            </div>
            <div>
              @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'menu_8','menu_class' => 'megamenu_links-spacing']) !!}
              @endif
            </div>
          </div>
          <div class="col-12 col-lg-3 col-xl-3 offset-lg-1 mega_menu_item mobile_spacing_menu_item">
            <div class="mega-menu-heading custom-flex-column">
              <?php
              $naslov_kol2 = get_field( 'kolona_2_naslov_tekst', 'option' );
              $link_kol2 = get_field( 'kolona_2_naslov_link', 'option' );
              ?>
              @if($naslov_kol2)
                <a href="{{ $link_kol2 }}" class="custom-heading-2">{{ $naslov_kol2 }}</a>
              @endif
            </div>
            @if (has_nav_menu('primary_navigation'))
              {!! wp_nav_menu(['theme_location' => 'menu_9','menu_class' => 'megamenu_links-spacing']) !!}
            @endif
          </div>
          <div class="col-12 col-lg-3 mega_menu_item mobile_spacing_menu_item">
            <div class="mega-menu-heading custom-flex-column">
              <?php
              $naslov_kol3 = get_field( 'kolona_3_naslov_tekst', 'option' );
              $link_kol3 = get_field( 'kolona_3_naslov_link', 'option' );
              ?>
              @if($naslov_kol3)
                <a href="{{ $link_kol3 }}" class="custom-heading-2">{{ $naslov_kol3 }}</a>
              @endif
            </div>
            @if (has_nav_menu('primary_navigation'))
              {!! wp_nav_menu(['theme_location' => 'menu_10','menu_class' => 'megamenu_links-spacing']) !!}
            @endif
          </div>
          <div class="col-12 col-lg-3 col-xl-3 mega_menu_item">
            <div class="mega-menu-heading custom-flex-column">
              <?php
              $naslov_kol4 = get_field( 'kolona_4_naslov_jedan_tekst', 'option' );
              $link_kol4 = get_field( 'kolona_4_naslov_jedan_link', 'option' );
              ?>
              @if($naslov_kol4)
                <a href="{{ $link_kol4 }}" class="custom-heading-2">{{ $naslov_kol4 }}</a>
              @endif
            </div>
            @if (has_nav_menu('primary_navigation'))
              {!! wp_nav_menu(['theme_location' => 'menu_11','menu_class' => 'megamenu_links-spacing']) !!}
            @endif
            <div class="mega-menu-heading custom-flex-column">
              <?php
              $naslov_kol4_2 = get_field( 'kolona_4_naslov_dva_tekst', 'option' );
              $link_kol4_2 = get_field( 'kolona_4_naslov_dva_link', 'option' );
              ?>
              @if($naslov_kol4_2)
                <a href="{{ $link_kol4_2 }}" class="custom-heading-2">{{ $naslov_kol4_2 }}</a>
              @endif
            </div>
            @if (has_nav_menu('primary_navigation'))
              {!! wp_nav_menu(['theme_location' => 'menu_12','menu_class' => 'megamenu_links-spacing']) !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="secondary-header">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-3">
          <?php $rez_online_link = get_field('rezervisi_online_link','option'); ?>
          <a href="{{ $rez_online_link }}" class="button yellow">Rezerviši online</a>
        </div>
        <div class="col-12 col-lg-5 header-call">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.36 20" width="19.36" height="20">
            <path fill="#0e3351"
                  d="M17.07,7.36a7.39,7.39,0,0,0-14.78,0A3.55,3.55,0,0,0,0,10.7V12a3.58,3.58,0,0,0,3.57,3.57h.25A1.54,1.54,0,0,0,5.35,14V8.66a1.5,1.5,0,0,0-1-1.42,5.35,5.35,0,0,1,10.7,0,1.52,1.52,0,0,0-1,1.42V14a1.52,1.52,0,0,0,1.17,1.48,1.56,1.56,0,0,1-1.17.56H12.94a2.9,2.9,0,0,0-2.75-1.91,2.93,2.93,0,1,0,0,5.86h0a3,3,0,0,0,2.75-1.91H14a3.56,3.56,0,0,0,3.52-3A3.57,3.57,0,0,0,19.36,12V10.7A3.55,3.55,0,0,0,17.07,7.36ZM3.31,13.48A1.54,1.54,0,0,1,2,12V10.7A1.53,1.53,0,0,1,3.31,9.2ZM10.19,18h0a.89.89,0,0,1-.89-.89.89.89,0,1,1,.89.89Zm7.13-6a1.54,1.54,0,0,1-1.27,1.51V9.17a1.54,1.54,0,0,1,1.27,1.51Z"/>
          </svg>
          <span class="custom-heading-2">Rezerviši pozivom:</span>
          @if(get_field('telefon_kod', 'option'))
            <a href="tel:+{{ get_field('telefon_kod', 'option') }}"
               class="custom-heading-2">{{ get_field('telefon_prikaz', 'option') }}</a>
          @endif
        </div>
        <div class="col-12 col-lg-4 text-align-right">
          <?php
          $button1_tekst = get_field('button_1_tekst','option');
          $button1_link = get_field('button_1_link','option');
          $button2_tekst = get_field('button_2_tekst','option');
          $button2_link = get_field('button_2_link','option');
          ?>
         @if($button1_tekst)
              <a href="{{ $button1_link }}" class="button blue mr-lg-2">{{ $button1_tekst }}</a>
           @endif
            @if($button2_tekst)
              <a href="{{ $button2_link }}" class="button blue">{{ $button2_tekst }}</a>
            @endif
        </div>
      </div>
    </div>
  </div>
</header>
