<?php
$startline = date_i18n('N');
$today = date_i18n('Ymd');
$date1 = $today;
$daprint1 = days(date('D', strtotime($today))) . ' ' . date('d', strtotime($today)) . '.' ;
$daprint2 = days(date('D', strtotime($today . ' + 1 day'))) . ' ' . date('d', strtotime($today . ' + 1 day')) . '. ' ;
$daprint3 = days(date('D', strtotime($today . ' + 2 day'))) . ' ' . date('d', strtotime($today . ' + 2 day')) . '. ' ;
$daprint4 = days(date('D', strtotime($today . ' + 3 day'))) . ' ' . date('d', strtotime($today . ' + 3 day')) . '. ' ;
$daprint5 = days(date('D', strtotime($today . ' + 4 day'))) . ' ' . date('d', strtotime($today . ' + 4 day')) . '. ' ;
$daprint6 = days(date('D', strtotime($today . ' + 5 day'))) . ' ' . date('d', strtotime($today . ' + 5 day')) . '. ' ;
$daprint7 = days(date('D', strtotime($today . ' + 6 day'))) . ' ' . date('d', strtotime($today . ' + 6 day')) . '. ' ;

?>
<div class="date_picker_filter custom-mb-small">
    <div class="date_picker_date custom-flex-row">
        <button type="button" class="button active button_date-click" data-date="{{ $today  }}">{{ $daprint1  }}</button>
        <button type="button" class="button button_date-click" data-date="{{ date('Ymd', strtotime(' +1 day'))  }}">{{ $daprint2  }}</button>
        <button type="button" class="button button_date-click" data-date="{{ date('Ymd', strtotime(' +2 day'))  }}">{{ $daprint3  }}</button>
        <button type="button" class="button button_date-click" data-date="{{ date('Ymd', strtotime(' +3 day'))  }}">{{ $daprint4  }}</button>
        <button type="button" class="button button_date-click" data-date="{{ date('Ymd', strtotime(' +4 day'))  }}">{{ $daprint5  }}</button>
        <button type="button" class="button button_date-click" data-date="{{ date('Ymd', strtotime(' +5 day'))  }}">{{ $daprint6  }}</button>
        <button type="button" class="button button_date-click" data-date="{{ date('Ymd', strtotime(' +6 day'))  }}">{{ $daprint7  }}</button>
    </div>
</div>
