<footer id="footer" class="bg-deffer" data-xl="{{ \App\asset_path('images/beograd_nocu_footer_bg_logo.svg') }}"
        data-md="{{ \App\asset_path('images/beograd_nocu_footer_bg_logo_mobile.svg') }}">
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-12 footer-top-container">
          @if(get_field('logo', 'option'))
            <div class="logo">
              <a href="{{ site_url() }}">
                <img src="" data-srcset="{{ get_field('logo', 'option') }}" alt="" class="img-fluid">
              </a>
            </div>
          @endif
          <div class="social">
            @if(get_field('facebook', 'option'))
              <a href="{{ get_field('facebook', 'option') }}" target="_blank" rel="noopener nofollow">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.03 40" width="21.03" height="40">
                  <path fill="#4c4c4c"
                        d="M20.51.26A42.8,42.8,0,0,0,14.87,0a8.42,8.42,0,0,0-4.36,1A9.18,9.18,0,0,0,6.15,8.46V14.1c0,.26,0,.52-.51.52H.51c-.25,0-.51,0-.51.51v6.15c0,.51,0,.51.51.51H5.38c.77,0,.77,0,.77.77V39.23c0,.77,0,.77.77.77h5.9c.77,0,.77,0,.77-.77V21.79H19c.26,0,.52,0,.52-.51q.38-3.08.77-6.15c0-.51,0-.51-.52-.51H13.85c-.26,0-.52,0-.52-.26V9.74A2.3,2.3,0,0,1,15.9,7.18H20c.51,0,.51,0,.51-.51A25.73,25.73,0,0,1,21,.77C21,.51,21,.26,20.51.26Z"/>
                </svg>
              </a>
            @endif
            @if(get_field('instagram', 'option'))
              <a href="{{ get_field('instagram', 'option') }}" target="_blank" rel="noopener nofollow">
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38.8" height="40" viewBox="0 0 38.8 39">
                  <path fill="#4C4C4C"
                        d="M32.4,0.8c-1-0.4-2-0.7-3-0.8H9.7C9.2,0.1,8.6,0.2,8.1,0.3C4,1.2,0.9,4.5,0.2,8.6l0,0.2C0,9,0,9.4,0,9.7v19.6c0.1,0.6,0.2,1.1,0.3,1.7c0.9,4,4.2,7.1,8.3,7.8c0.6,0.1,1.3,0.2,1.9,0.2h17.8c3.5,0,6.7-1.7,8.6-4.6c1.3-1.7,1.9-3.8,1.9-6V10.6C38.9,6.3,36.4,2.4,32.4,0.8z M36.4,10.9v17.4c0.1,4.4-3.4,8.1-7.8,8.1c0,0,0,0,0,0H10.4C7,36.4,4.1,34.2,3,31c-0.3-0.9-0.4-1.8-0.4-2.8V10.8C2.5,7.3,4.7,4.1,8,3c0.9-0.3,1.8-0.4,2.7-0.4h17.5C31.8,2.5,34.9,4.7,36,8C36.3,8.9,36.4,9.9,36.4,10.9z M19.5,9.7c-5.4,0-9.8,4.4-9.8,9.8c0,5.4,4.4,9.8,9.8,9.8l0,0c5.4,0,9.8-4.4,9.8-9.8S24.9,9.7,19.5,9.7C19.5,9.7,19.5,9.7,19.5,9.7z M19.5,26.7c-4,0-7.2-3.2-7.2-7.2s3.2-7.2,7.2-7.2s7.2,3.2,7.2,7.2l0,0C26.6,23.5,23.4,26.7,19.5,26.7L19.5,26.7z M31.8,7.1c-0.4-0.3-1-0.5-1.5-0.4h-0.1c-1.1,0-2.1,0.9-2.1,2.1l0,0c0,1.1,0.9,2.1,2.1,2.1c0,0,0.1,0,0.1,0h0.5c0.6-0.1,1.1-0.3,1.4-0.8c0.3-0.4,0.5-1,0.4-1.5C32.5,7.9,32.3,7.4,31.8,7.1z"/>
                </svg>
              </a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-10 mx-auto">
        <div class="row">
          <div class="col-12 col-md-6 col-lg-4 footer-widget">
            @if ( is_active_sidebar( 'footer_widget' ) )
              @php(dynamic_sidebar( 'footer_widget' ))
            @endif
          </div>
          <div class="col-12 col-md-6 col-lg-3 offset-lg-1 footer-menu">
            <?php
            $locations = get_nav_menu_locations(); //get all menu locations
            $menu1 = wp_get_nav_menu_object( $locations["menu_1"] );?>
            <?php
            $footer_kolona_dva_meni_jedan_text = get_field( 'footer_kolona_dva_meni_jedan_text', 'option' );
            $footer_kolona_dva_meni_jedan_link = get_field( 'footer_kolona_dva_meni_jedan_link', 'option' );

            $footer_kolona_dva_meni_dva_text = get_field( 'footer_kolona_dva_meni_dva_text', 'option' );
            $footer_kolona_dva_meni_dva_link = get_field( 'footer_kolona_dva_meni_dva_link', 'option' );

            $footer_kolona_tri_meni_jedan_text = get_field( 'footer_kolona_tri_meni_jedan_text', 'option' );
            $footer_kolona_tri_meni_jedan_link = get_field( 'footer_kolona_tri_meni_jedan_link', 'option' );

            $footer_kolona_tri_meni_dva_text = get_field( 'footer_kolona_tri_meni_dva_text', 'option' );
            $footer_kolona_tri_meni_dva_link = get_field( 'footer_kolona_tri_meni_dva_link', 'option' );
            ?>
            @if($footer_kolona_dva_meni_jedan_text)
              <p class="custom-heading-2"><a href="{{ $footer_kolona_dva_meni_jedan_link }}">{{ $footer_kolona_dva_meni_jedan_text }}</a></p>
            @else
                <p class="custom-heading-2"><?php echo $menu1->name; ?></p>
            @endif
            @if (has_nav_menu('menu_1'))
              {!! wp_nav_menu(['theme_location' => 'menu_1', 'menu_class' => 'navbar-nav']) !!}
            @endif
          </div>
          <div class="col-12 col-md-6 col-lg-3 offset-lg-1 footer-menu">
            <?php $menu2 = wp_get_nav_menu_object($locations['menu_2']);?>
              @if($footer_kolona_tri_meni_jedan_text)
                <p class="custom-heading-2"><a href="{{ $footer_kolona_tri_meni_jedan_link }}">{{ $footer_kolona_tri_meni_jedan_text }}</a></p>
              @else
                <p class="custom-heading-2"><?php echo $menu2->name; ?></p>
              @endif

            @if (has_nav_menu('menu_2'))
              {!! wp_nav_menu(['theme_location' => 'menu_2', 'menu_class' => 'navbar-nav']) !!}
            @endif
          </div>
          <div class="col-12 col-md-6 col-lg-4 footer-menu vazni_linkovi_order">
            <p class="custom-heading-2">Važni linkovi</p>
            @if (has_nav_menu('menu_3'))
              {!! wp_nav_menu(['theme_location' => 'menu_3', 'menu_class' => 'navbar-nav']) !!}
            @endif
          </div>
          <div class="col-12 col-md-6 col-lg-3 offset-lg-1 footer-menu">
            <?php $menu4 = wp_get_nav_menu_object($locations['menu_4'] );?>
              @if($footer_kolona_dva_meni_dva_text)
                <p class="custom-heading-2">
                  <a href="{{ $footer_kolona_dva_meni_dva_link }}">{{ $footer_kolona_dva_meni_dva_text }}</a>
                </p>
              @else
                <p class="custom-heading-2"><?php echo $menu4->name; ?></p>
              @endif
            @if (has_nav_menu('menu_4'))
              {!! wp_nav_menu(['theme_location' => 'menu_4', 'menu_class' => 'navbar-nav']) !!}
            @endif
          </div>
          <div class="col-12 col-md-6 col-lg-3 offset-lg-1 footer-menu">
            <?php $menu5 = wp_get_nav_menu_object($locations['menu_5']);?>
              @if($footer_kolona_dva_meni_dva_text)
                <p class="custom-heading-2">
                  <a href="{{ $footer_kolona_tri_meni_dva_link }}">{{ $footer_kolona_tri_meni_dva_text }}</a>
                </p>
              @else
                <p class="custom-heading-2"><?php echo $menu5->name; ?></p>
              @endif
            @if (has_nav_menu('menu_5'))
              {!! wp_nav_menu(['theme_location' => 'menu_5', 'menu_class' => 'navbar-nav']) !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-10 mx-auto">
          <div class="footer-contact">
            <?php
            $cta_footer1 = get_field( 'cta_footer_1' );
            $cta_footer1_link = $cta_footer1['link'];
            $cta_footer1_text = $cta_footer1['text']; ?>
            @if($cta_footer1)
              <a href="{{ $cta_footer1_link }}">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 42.96" width="50" height="42.96">
                  <path
                    d="M13.58,12c13.57-1,15.6-2.57,21.59-7.21,1-.76,2.07-1.6,3.34-2.54A4.79,4.79,0,0,1,42.22,0C48.68,0,50,15.27,50,19.78s-1.32,19.77-7.78,19.77a4.65,4.65,0,0,1-3.58-2.09c-1.32-1-2.45-1.85-3.47-2.64-3.86-3-6.07-4.7-10.8-5.81.28,1.06.64,2.4.92,3.36.19.64-.26,1-1,1.25-.45.13-1.09.28-1.69.41-.32.08-.63.14-.88.21.72.84,1.73,1.88,2.64,2.81.71.73,1.37,1.4,1.8,1.89a2.61,2.61,0,0,1,0,3.8.79.79,0,0,1-.58.22l-9.25-.17a.85.85,0,0,1-.85-.61L11.35,27.65H7.78a7.78,7.78,0,0,1,0-15.56c2,0,3.85.05,5.81-.09Zm9,16.64c-.93-.17-1.94-.32-3.06-.46.38,1.44,1,3.59,1.25,4.57.31-.09.87-.22,1.46-.35l1.28-.3C23.12,30.88,22.76,29.51,22.53,28.64ZM17.68,28c-1.38-.14-2.9-.28-4.62-.39h0l3.8,13.52,8.26.16A1.09,1.09,0,0,0,24.83,40c-.4-.46-1-1.11-1.73-1.82-1.62-1.66-3.55-3.64-3.92-4.77S18.06,29.42,17.68,28Zm-5.3-2.13c15.27,1,17.33,2.63,23.81,7.65l.48.37-.16-.42a40.63,40.63,0,0,1-2.08-12.87A41.06,41.06,0,0,1,36.51,6.09l.12-.31-.44.34c-6.42,5-8.5,6.57-23.31,7.61l-.38.13c-.36.13-.73.78-1.05,1.75a14.49,14.49,0,0,0-.58,4.27,14.44,14.44,0,0,0,.58,4.27,3.42,3.42,0,0,0,.93,1.69ZM10.44,13.77H7.76A6.11,6.11,0,0,0,7.76,26h2.66a7.36,7.36,0,0,1-.55-1.3,16,16,0,0,1-.67-4.79,15.92,15.92,0,0,1,.67-4.78A7.2,7.2,0,0,1,10.44,13.77Zm26.09-.43a6.77,6.77,0,0,1,5.15,1.87,6.29,6.29,0,0,1,0,9.14,6.73,6.73,0,0,1-5.15,1.87,32.2,32.2,0,0,0,1.56,6.66c1.14,3.08,2.59,5,4.12,5,4.9,0,6.1-14.78,6.1-18.09s-1.2-18.09-6.1-18.09c-1.53,0-3,1.91-4.12,5a32.35,32.35,0,0,0-1.56,6.66Zm-.2,11.17h0a5.13,5.13,0,0,0,4.16-1.37,4.64,4.64,0,0,0,0-6.72,5.11,5.11,0,0,0-4.19-1.36,49.73,49.73,0,0,0,0,9.45Z"/>
                </svg>
                <span>{{ $cta_footer1_text }}</span>
              </a>
            @endif
            <?php
            $cta_footer2 = get_field( 'cta_footer_2' );
            $cta_footer2_link = $cta_footer2['link'];
            $cta_footer2_text = $cta_footer2['text']; ?>
            @if($cta_footer2)
              <a href="{{ $cta_footer2_link }}">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.51 50" width="40" height="50">
                  <path
                    d="M7.52,0H24.34a2.21,2.21,0,0,1,2.34,2.34V20.46h.82a2.93,2.93,0,0,1,2.14.94,3.22,3.22,0,0,1,0,4.4,2.57,2.57,0,0,1-.39.35A2.87,2.87,0,0,1,30,28.08a2.71,2.71,0,0,1-.89,2,3,3,0,0,1-1.66.82A3.23,3.23,0,0,1,27,34.77a3,3,0,0,1-2.21.94c-1,1.85-4.13,6.43-10.09,7.09-.17,0-.18.53-.54,1.08l0,.07,2.44,1.19L14.23,50,0,43c1-2.07,2-4.1,3-6.16L1.13,25.24l.05-.43,4-9.6V2.34A2.23,2.23,0,0,1,7.51,0Zm5.16,14.48,3.08,4.74L21,10l1.82,1L16.73,21.76l-1.79.06-4-6.19,1.75-1.14ZM23,35.72H10.55c-.2.33-.41.61-.64.93l-1.29-.92a8.51,8.51,0,0,0,1.57-3.38A5.79,5.79,0,0,0,9.84,29a5.41,5.41,0,0,0-1.43-2,14.24,14.24,0,0,0-2.2-1.56L5.1,24.76l1.13-.67c.48-.28,1.69-3.2,1.95-4a6.32,6.32,0,0,0,.36-3.35,2.89,2.89,0,0,0-.63-1.29,2.54,2.54,0,0,0-.86-.62L2.72,25.22,4,33l.62,3.9-.06.45-.72,1.58,8.93,4.37c0-.08.09-.16.14-.24.57-.87,1.13-1.72,1.69-1.78A11.68,11.68,0,0,0,23,35.73ZM11.86,31h7.53a3.3,3.3,0,0,1,.47-.64,3.23,3.23,0,0,1,.68-.54,2.64,2.64,0,0,1-.62-1.71A2.86,2.86,0,0,1,20.81,26a4.31,4.31,0,0,1,.4-.32,3.22,3.22,0,0,1,.09-4.3,2.93,2.93,0,0,1,2.14-.94h.63V2.64H7.8V13.43c2.7,1.29,2.69,4.61,1.88,7.12a17.77,17.77,0,0,1-1.94,4,11.42,11.42,0,0,1,1.72,1.3,6.94,6.94,0,0,1,1.83,2.58A7.45,7.45,0,0,1,11.86,31ZM27.3,26.75H23a1.46,1.46,0,0,0-1.11.38,1.32,1.32,0,0,0,0,1.93,1.43,1.43,0,0,0,.54.33c1.53,0,3,.08,4.55.08a1.47,1.47,0,0,0,1-.41,1.32,1.32,0,0,0,0-1.93A1.44,1.44,0,0,0,27.3,26.75ZM27.51,22H23.44a1.4,1.4,0,0,0-1,.44A1.65,1.65,0,0,0,22,23.61a1.75,1.75,0,0,0,.43,1.17,1.36,1.36,0,0,0,1,.44h4.07a1.35,1.35,0,0,0,1-.44,1.73,1.73,0,0,0,.43-1.17,1.65,1.65,0,0,0-.43-1.13A1.38,1.38,0,0,0,27.51,22ZM24.86,31H22a1.44,1.44,0,0,0-1,.45,1.62,1.62,0,0,0-.44,1.14A1.68,1.68,0,0,0,21,33.7a1.47,1.47,0,0,0,1,.45h2.82a1.49,1.49,0,0,0,1-.45,1.68,1.68,0,0,0,.44-1.14,1.66,1.66,0,0,0-.44-1.14A1.43,1.43,0,0,0,24.86,31Z"/>
                </svg>
                <span>{{ $cta_footer2_text }}</span>
              </a>
            @endif
          </div>
          <div class="bg-nocu-projects">
            <p class="custom-heading-2 text-center">Beograd noću projekti</p>
            <div class="logos">
              <?php
              if (have_rows( 'projekti', 'option' ) ): while (have_rows( 'projekti', 'option' ) ): the_row();
              $link = get_sub_field( 'link_do_projekta' );
              $image = get_sub_field( 'logo_projekta' ); ?>
              <a href="{{ $link }}">
                <img src="" data-srcset="{{ $image }}" alt="" class="img-fluid">
              </a>
              <?php endwhile; endif; ?>
            </div>
          </div>
          <div class="mobile-apps">
            <p class="custom-heading-2">Preuzmi aplikaciju</p>
            @if(get_field('android_app_link', 'option'))
              <a href="{{ get_field('android_app_link', 'option') }}" target="_blank" rel="nofollow noopener">
                <img src="" data-srcset="@asset('images/beograd_nocu__sidebar_google_play.svg')" alt=""
                     class="img-fluid ">
              </a>
            @endif
            @if(get_field('ios_app_link', 'option'))
              <a href="{{ get_field('ios_app_link', 'option') }}" target="_blank" rel="nofollow noopener">
                <img src="" data-srcset="@asset('images/beograd_nocu__sidebar_app_store.svg')" alt=""
                     class="img-fluid ">
              </a>
            @endif
          </div>
        </div>
        <div class="col-12 copy">
          <p>beogradnocu.com 2012 - {{ date('Y') }}</p>
        </div>
      </div>
    </div>
  </div>
</footer>

<script defer>
  // Dinamički import modula po potrebi
  window.addEventListener('load', () => {

    // 	if ( document.querySelector( '.bg-deffer' ) ) {
    // 		import('/wp-content/themes/bgn/resources/assets/scripts/custom/img-defer.js')
    // 			.then( module => {
    // 				module.defer_background();
    // 			} )
    // 			.catch( err => console.error( err ) );
    // 	}

    if (document.querySelector('.date_picker_filter')) {
      document.addEventListener('click', e => {
        if (!e.target.closest('.button_date-click') && !e.target.closest('.date_picker_type_lokal_radio label') && !e.target.closest('.custom_checkbox_container label')) return;
        console.info('Started Module Import ...');
        import('/wp-content/themes/bgn/resources/assets/scripts/custom/search_lokali-category.js')
          .then(module => {
            console.info('Module imported');
            module.datePickerControl(document.querySelector('.date_picker_filter'));
          })
          .catch(err => console.error(err));
      }, {once: true});
    }
  });
</script>
