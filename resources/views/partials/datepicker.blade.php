<div class="datepicker custom-mb-big">
  <div class="datepicker_header custom-flex-row">
    <button type="button" class="datepicker_arrow button left prev-month"
            data-prev-month="<?php echo date( 'm' ) - 1 ?>" data-year="<?php echo date( 'Y' ) ?>"></button>
    <?php
    $today = date_i18n( 'd.m.Y.' );

    $meseci = array(
      array(
        'num'  => '01',
        'name' => 'Januar'
      ),
      array(
        'num'  => '02',
        'name' => 'Februar'
      ),
      array(
        'num'  => '03',
        'name' => 'Mart'
      ),
      array(
        'num'  => '04',
        'name' => 'April'
      ),
      array(
        'num'  => '05',
        'name' => 'Maj'
      ),
      array(
        'num'  => '06',
        'name' => 'Jun'
      ),
      array(
        'num'  => '07',
        'name' => 'Jul'
      ),
      array(
        'num'  => '08',
        'name' => 'Avgust'
      ),
      array(
        'num'  => '09',
        'name' => 'Septembar'
      ),
      array(
        'num'  => '10',
        'name' => 'Oktobar'
      ),
      array(
        'num'  => '11',
        'name' => 'Novembar'
      ),
      array(
        'num'  => '12',
        'name' => 'Decembar'
      )
    );

    function searchMultiArray( $val, $array ) {
      foreach ( $array as $element ) {
        if ( $element['num'] == $val ) {
          return $element['name'];
        }
      }

      return null;
    }


    $today_format = month( date( 'n', strtotime( $today ) ) ) . ' ' . date( 'Y.' );
    $nameOfDay = date( 'D', strtotime( $today ) );
    $key = searchMultiArray( date( 'm' ), $meseci )
    ?>
    <p class="datepicker_month"> {{ $key }} {{ date('Y.') }} </p>
    <button type="button" class="datepicker_arrow button right next-month"
            data-next-month="<?php echo date( 'm' ) + 1 ?>" data-year="<?php echo date( 'Y' ) ?>"></button>
  </div>
  <div class="datepicker_days custom-flex-row">
    <?php
    $weekDayName = array( "PON", "UTO", "SRE", "ČET", "PET", "SUB", "NED" );

    $month = date( 'm' );
    $year = date( 'Y' );
    $daysInMonth = cal_days_in_month( CAL_GREGORIAN, $month, $year );
    $first_day = date( '01-' . $month . '-' . $year . '' );
    $first_day_named = date( 'l', strtotime( $first_day ) );

    $day_of_week = (int) date( 'N', strtotime( $first_day_named ) ) - 1;
    $days = [];
    for ( $day = 1; $day <= $day_of_week; $day ++ ) :
      $days[] = 0;
    endfor;

    for ( $day = 1; $day <= $daysInMonth; $day ++ ) :
      $days[] = $day;
    endfor;
    ?>
    @if($weekDayName)
      @foreach($weekDayName as $name)
        <span>{{ $name }}</span>
      @endforeach
    @endif
  </div>
  <div class="datepicker_dates">
    @if($daysInMonth > 0)
      <?php $i = 1;
      $results = [];
      ?>
      @foreach($days as $dayz)
        <?php echo $i == 1 ? '<div class="datepicker_dates_row custom-flex-row">' : '';
        $today = date( 'd-m-Y' );
        ?>
        <?php

        $days[] = $dayz;

        $current_date = $dayz . '-' . $month . '-' . $year;
        $current_date = date( 'd-m-Y', strtotime( $current_date ) );
        $date_2 = date( 'Ymd', strtotime( $current_date ) );
        $posts = new WP_Query( [
          'post_type'      => 'galerija',
          'status'         => 'published',
          'posts_per_page' => - 1,
          'meta_query'     => array(
            array(
              'key'     => 'datum_galerije',
              'value'   => $date_2,
              'compare' => '=',
              'type'    => 'DATE',
            ),
          ),
        ] );

        $today = explode( '-', $today );
        $dan = $today[0];
        $curr_day = date( 'd-m-Y' );

        if ( $posts->have_posts() ) {
          while ( $posts->have_posts() ) {
            $posts->the_post();
            $lokal          = get_field( 'lokal', get_the_ID() );
            $datum_galerije = date( "d-m-Y", strtotime( get_field( 'datum_galerije', get_the_ID() ) ) );
            $results[]      = $datum_galerije;
          }
        }
        $results = array_unique( $results );
        $class = "";
        if ( in_array( $current_date, $results ) ) {
          $class = "has_result";
        }
        if ( $curr_day == $current_date ) {
          $class = "today active";
        }

        ?>
        <button today="{{ $curr_day }}" id="gallery-{{ $current_date }}" data-date="{{ $current_date }}" type="button"
                class="{{ $class }} button datepicker_dates_button">{{ $dayz === 0 ? '' : $dayz }}</button>
        <?php
        echo $i == 7 ? '</div>' : '';
        $i ++;

        if ( $i > 7 ) {
          $i = 1;
        }
        ?>
      @endforeach
      <?php  ?>
    @endif
  </div>
</div>
<script>
  var $ = jQuery;

  $(document).ready(function () {
    $('.next-month').click(function () {
      var month = $(this).attr('data-next-month');
      var year = $(this).attr('data-year');

      $.ajax({
        dataType: 'json'
        , url: '/wp-json/load/calendar?month=' + month + '&year=' + year
        , type: 'GET'
        , success: function (data) {
          if (data) {
            $.each(data, function (i, item) {
              console.log(item);
              let html = '';
              var count = 0;
              $.each(item['days'], function (index, day) {
                if (count == 0) {
                  html += '<div class="datepicker_dates_row custom-flex-row">';
                }
                if (day === '') {
                  day = '';
                }

                let new_date = ('0' + day).slice(-2) + '-' + ('0' + item['month']).slice(-2) + '-' + item['year'];
                html += '<button id="gallery-' + new_date + '" data-date="' + new_date + '" type="button" class="button datepicker_dates_button">' + day + '</button>'
                if (count == 6) {
                  html += '</div>';
                }
                count++;
                if (count > 6) {
                  count = 0;
                }
              });

              let month_next = parseInt(item['month']) + 1;
              let month_prev = parseInt(item['month']) - 1;
              let year_next = item['year'];
              let previous_year = parseInt(item['year']);

              if (month_next > 12) {
                month_next = 1;
                //year_next = item['next_year'];
              } else if (month_next < 1) {
                year_next--;
              }

              if (parseInt(item['month']) === 12) {
                year_next = item['next_year'];
              }

              if (month_prev < 1) {
                month_prev = 12;
                previous_year = previous_year - 1;
              }

              $('.next-month').attr('data-next-month', month_next);
              $('.prev-month').attr('data-prev-month', month_prev);
              $('.next-month').attr('data-year', year_next);
              $('.prev-month').attr('data-year', previous_year);
              $('.datepicker_month').html(item['icl_month'] + ' ' + parseInt(item['year']) + '.');
              $('.datepicker_dates').html(html);

              $.each(item['posts'], function (index, result) {
                if (result['date'] === result['datum_galerije']) {
                  let gallery_id = $('#gallery-' + result['datum_galerije']);
                  gallery_id.addClass('has_result');
                }
              });
            });
          }
        }
      });
    });

    $('.prev-month').click(function () {
      var month = $(this).attr('data-prev-month');
      var year = $(this).attr('data-year');


      $.ajax({
        dataType: 'json'
        , url: '/wp-json/load/calendar?month=' + month + '&year=' + year
        , type: 'GET'
        , success: function (data) {
          if (data) {
            $.each(data, function (i, item) {
              console.log(item);
              let html = '';
              var count = 0;
              $.each(item['days'], function (index, day) {
                if (count == 0) {
                  html += '<div class="datepicker_dates_row custom-flex-row">';
                }
                if (day === '') {
                  day = '';
                }

                let new_date = ('0' + day).slice(-2) + '-' + ('0' + item['month']).slice(-2) + '-' + item['year'];
                html += '<button id="gallery-' + new_date + '" data-date="' + new_date + '" type="button" class="button datepicker_dates_button">' + day + '</button>'
                if (count == 6) {
                  html += '</div>';
                }
                count++;
                if (count > 6) {
                  count = 0;
                }
              });

              let month_next = parseInt(item['month']) + 1;
              let month_prev = parseInt(item['month']) - 1;
              let year_next = item['year'];
              let previous_year = parseInt(item['year']);

              if (month_next > 12) {
                month_next = 1;
              } else if (month_next < 1) {
                year_next++;
              }

              if (parseInt(item['month']) === 12) {
                year_next = item['next_year'];
              }

              if (month_prev < 1) {
                month_prev = 12;
                previous_year = previous_year - 1;
              }

              $('.next-month').attr('data-next-month', month_next);
              $('.prev-month').attr('data-prev-month', month_prev);
              $('.next-month').attr('data-year', year_next);
              $('.prev-month').attr('data-year', previous_year);
              $('.datepicker_month').html(item['icl_month'] + ' ' + item['year'] + '.');
              $('.datepicker_dates').html(html);

              $.each(item['posts'], function (index, result) {
                if (result['date'] === result['datum_galerije']) {
                  let gallery_id = $('#gallery-' + result['datum_galerije']);
                  gallery_id.addClass('has_result');
                }
              });
            });
          }
        }
      });
    });
  })


  window.onload = () => {
    let label = $('.has_result');
    document.addEventListener('click', e => {
      if (!e.target.closest('.has_result')) return;
      load_gallery(e.target)
    });

    function load_gallery(e) {
      let $this = $(e);
      let date = $this.data('date');


      $.ajax({
        beforeSend: function (request) {
          $('body').find('.gallery_group_cards').empty();
          $('.gallery_group_cards').css('display', 'flex').addClass('show');
          $('.has_result').removeClass('active');
        },
        dataType: 'json',
        url: '/wp-json/kalendar/galerije?date=' + date,
        type: 'GET',
        success: function (data) {
          $this.addClass('active');
          if (data.length) {
            console.log(data)
            $gcounter = 0;
            $.each(data, function (i, item) {
              console.log(data);
              var html = '<div class="custom-card">';
              html += '<div class="custom-card-link custom-flex-row">'
              html += '<div class="custom-card-image">'
              $.each(item.galerija, function (i, item) {
                $gcounter++;
                html += '<a href="' + item['url_do_fotografije'] + '" data-lightbox="gallery">';
                if ($gcounter < 2) {
                  html += '<img src="' + item['url_do_fotografije'] + '">';
                }
                html += '</a>';
              })
              html += '</div>';
              html += '<h3 class="custom-card-title"><a href="' + item.url_lokala + '">' + item.title_lokala + ' </a></h3>';
              html += '<div class="custom-card-info">';
              html += '<p>Datum: ' + item.date + '</p>';
              html += '<p>Broj foto: ' + item.num_photos + '</p>';
              html += '</div></div></div>';
              $('body').find('.gallery_group_cards').html(html);
            });
          }
        }
      });
    }
  }

</script>
