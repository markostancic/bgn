@if(have_rows('grupa_lokala', 'option'))
	<div class="col-12 col-lg-10 mx-auto featured custom-mb-big">
		<div id="featured-local" class="carousel slide defer" data-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-controls custom-flex-row">
					<a class="carousel-control-prev controls" href="#featured-local" role="button" data-slide="prev">
						<span class="carousel-control-icon carousel-control-icon-prev" aria-hidden="true"></span>
						<span class="sr-only"><?php _e( 'Previous', 'beogradnocu' ); ?></span>
					</a>
					<a class="carousel-control-next controls" href="#featured-local" role="button" data-slide="next">
						<span class="carousel-control-icon carousel-control-icon-next" aria-hidden="true"></span>
						<span class="sr-only"><?php _e( 'Next', 'beogradnocu' ); ?></span>
					</a>
				</div>
				<?php $i = 0; ?>
				<?php while (have_rows( 'grupa_lokala', 'option' )) : the_row(); ?>
				<div class="carousel-item {{ $i == 0 ? 'active' : '' }}">
					<div class="carousel-head partials-heading">
						<h4 class="carousel-heading custom-heading-4">{{ get_sub_field('naslov') }}</h4>
					</div>
					<div class="custom-flex-row">
						@foreach(get_sub_field('lokali') as $lokal)
							<div class="local">
								<a href="{{ get_permalink($lokal->ID) }}">
									<picture>
										<?php //get_webp_image_type( [ get_the_post_thumbnail_url( $lokal->ID ) => '' ], '' ) ?>
										<img src="" {{ $i == 0 ? 'data-srcset' : 'data-src' }}="{{ get_the_post_thumbnail_url($lokal->ID) }}"
										alt="" class="img-fluid {{ $i == 0 ? '' : 'defer' }}" width="230" height="140">
									</picture>
									<h3 class="featured-title">{{ $lokal->post_title }}</h3>
								</a>
							</div>
						@endforeach
					</div>
				</div>
				<?php $i++;
				endwhile;
				?>
			</div>
		</div>
	</div>
@endif
