<?php
$posts = get_field( 'povezani_tekstovi', 'option' );
if ( !empty( get_field( 'povezani_tekstovi' ) ) ):
	$posts = get_field( 'povezani_tekstovi' );
endif;
?>
@if($posts)
	<div class="col-12 gallery custom_cards custom-flex-row custom-mb-big">
		<div class="custom-card-heading custom-flex-row partials-heading">
			<h4 class="custom-heading-4"><?php _e( 'Povezani tekstovi', 'beogradnocu' ) ?></h4>
			@if(get_field('link_magazin', 'option'))
				<a href="{{ get_field('link_magazin', 'option')['url'] }}" class="button"><?php _e( 'Svi tekstovi', 'beogradnocu' ) ?></a>
			@endif
		</div>
		@foreach($posts as $post)
			<div class="custom-card">
				<a href="{{ get_the_permalink($post->ID)  }}" class="custom-card-link custom-flex-row">
					@if(has_post_thumbnail($post->ID))
						<div class="custom-card-image">
							<picture>
								<?php
								$thumbnail = get_the_post_thumbnail_url( $post->ID, 'single_povezan' );
								get_webp_image_type( [ $thumbnail => '' ] );
								?>
								<img src="" data-srcset="{{ $thumbnail  }}" alt="">
							</picture>
						</div>
					@endif
					<h3 class="custom-card-title">{!! get_the_title($post->ID) !!}</h3>
					<div class="custom-card-info">
						<p class="date">{{ get_the_date('d.m.Y.', $post->ID) }}</p>
					</div>
				</a>
			</div>
		@endforeach
	</div>
@endif
@php(wp_reset_postdata())
