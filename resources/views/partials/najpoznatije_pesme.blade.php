<?php
$pesme = get_field( 'pesme' );
$naslov = get_field( 'naslov_sekcije' ); ?>
<div class="custom_cards related-text custom-flex-row custom-mb-xs">
  <div class="similar_songs">
    <h2>Najpopularnije pesme</h2>
  </div>
  <div class="related_songs p-0 row">
    @php $args = array(
                          'post_type' => 'tekst_pesme',
                          'posts_per_page' => -1,
                          'post__in' => $pesme
                          ); @endphp

    @php $query = new WP_Query( $args ); @endphp
    @if ( $query->have_posts() )
      @while ( $query->have_posts() )
        @php $query->the_post(); @endphp
        <div class="song col-lg-12 p-0">
          <div class="song_content">
            <h3 class="title"><a href="{{ the_permalink() }}">{{ the_title() }}</a></h3>
            <span class="song_author">
              <?php
              $terms = get_the_terms( $query->ID, 'izvodjaci' );
              foreach ( $terms as $term ) { ?>
                <a href="{{ get_term_link($term) }}"><?php  echo $term->name; ?></a>
          <?php    } ?>
            </span>
          </div>
        </div>
      @endwhile
    @endif
    @php wp_reset_postdata(); @endphp
  </div>
</div>
