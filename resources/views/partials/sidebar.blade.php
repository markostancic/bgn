<aside class="col-12 col-lg-4 sidebar custom-mb-big">
  @php dynamic_sidebar('custom-sidebar') @endphp
  <?php if (have_rows('preporuka_dana', 'option')) :
  while (have_rows('preporuka_dana', 'option')) : the_row();
  $image = get_sub_field('slika');
  $link = get_sub_field('url');
  ?>
  <div class="recomend sidebar--custom-mb">
    <p class="custom-heading-2"><?php _e('Preporučujemo za danas', 'beogradnocu'); ?></p>
    <a href="{{ $link }}">
      <img src="{{ $image['sizes']['preporuka_sidebar']  }}" alt="" class="img-fluid">
    </a>
  </div>
  <?php
  endwhile;
  endif;
  ?>
  <div class="app-links custom-flex-row sidebar--custom-mb">
    <p class="custom-heading-2"><?php _e('Preuzmi aplikaciju', 'beogradnocu'); ?></p>
    @include('partials.app-links')
  </div>
  <!-- Lokali acf global  -->
  <?php
  $kafane_link = get_field('najpopularnije_kafane_link', 'option');
  $kafane_text = get_field('najpopularnije_kafane_tekst', 'option');
  $kafane = get_field('najpopularnije_kafane', 'option', false);

  $klubovi_link = get_field('najpopularniji_klubovi_link', 'option');
  $klubovi_text = get_field('najpopularniji_klubovi_tekst', 'option');
  $klubovi = get_field('najpopularniji_klubovi', 'option', false);

  $splavovi_link = get_field('najpopularniji_splavovi_link', 'option');
  $splavovi_text = get_field('najpopularniji_splavovi_tekst', 'option');
  $splavovi = get_field('najpopularniji_splavovi', 'option', false);

  $restorani_link = get_field('najpopularniji_restorani_link', 'option');
  $restorani_text = get_field('najpopularniji_restorani_tekst', 'option');
  $restorani = get_field('najpopularniji_restorani', 'option', false);

  $barovi_link = get_field('najpopularniji_barovi_link', 'option');
  $barovi_text = get_field('najpopularniji_barovi_tekst', 'option');
  $barovi = get_field('najpopularniji_barovi', 'option', false);

  $striptizklubovi_link = get_field('najpopularniji_striptizklubovi_link', 'option');
  $striptizklubovi_text = get_field('najpopularniji_striptizklubovi_tekst', 'option');
  $striptizklubovi = get_field('najpopularniji_striptiz_klubovi', 'option', false);

  $najcitaniji_link = get_field('najcitaniji_tekstovi_link', 'option');
  $najcitaniji_text = get_field('najcitaniji_tekstovi_tekst', 'option');
  $tekstovi = get_field('najcitaniji_tekstovi', 'option', false);
  ?>
  @if($splavovi)
    <div class="sidebar_cards sidebar--custom-mb">
      <h3 class="custom-heading-2"><a href="{{ $splavovi_link }}">{{ $splavovi_text }}</a></h3>
      <?php
      $query = new WP_Query( [
        'post_type'   => 'splavovi_beograd',
        'post__in'    => $splavovi,
        'post_status' => 'any',
        'orderby'     => 'post__in',
      ] );
      ?>
      @if ( $query->have_posts() )
        @while ( $query->have_posts() )
          @php $query->the_post(); @endphp
          <?php
          $permalink = get_permalink();
          $title = get_the_title();
          $featured = get_the_post_thumbnail_url( $query->ID, 'lokali_sidebar' );
          $grad_deo = get_field( 'grad_deo_grada' );
          $ulica = get_field( 'ulica_i_broj' );
          ?>
          <div class="sidebar_card {{ $title }}">
            <a href="{{ $permalink }}" class="sidebar_card_link custom-flex-row">
              <div class="sidebar_card_image">
                <img src="{{ $featured }}" alt="" class="img-fluid">
              </div>
              <div class="sidebar_card_text custom-flex-row">
                <h4 class="sidebar_card_title">{{ $title  }}</h4>
                @if ($grad_deo || $ulica)
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 25" width="10" height="14.7">
                    <path fill="#ccc"
                          d="M8.5,0A8.41,8.41,0,0,0,0,8.29c0,3.4,4.7,11.76,7.18,16a1.53,1.53,0,0,0,2.63,0C12.3,20.05,17,11.69,17,8.29A8.41,8.41,0,0,0,8.5,0Zm0,13.85A5.38,5.38,0,0,1,3.06,8.54,5.23,5.23,0,0,1,4.65,4.78a5.56,5.56,0,0,1,7.7,0,5.23,5.23,0,0,1,1.59,3.76A5.38,5.38,0,0,1,8.5,13.85Z"/>
                  </svg>
                  <div class="sidebar_card_info">
                    <p>{!! $ulica !!}</p>
                    <p>{!! $grad_deo !!}</p>
                  </div>
                @endif
              </div>
            </a>
          </div>
        @endwhile
      @endif
      @php wp_reset_postdata(); @endphp
    </div>
  @endif
  @if($kafane)
    <div class="sidebar_cards sidebar--custom-mb">
      <h3 class="custom-heading-2"><a href="{{ $kafane_link }}">{{ $kafane_text }}</a></h3>
      <?php
      $query = new WP_Query( [
        'post_type'   => 'kafane_beograd',
        'post__in'    => $kafane,
        'post_status' => 'any',
        'orderby'     => 'post__in',
      ] );
      ?>
      @if ( $query->have_posts() )
        @while ( $query->have_posts() )
          @php $query->the_post(); @endphp
          <?php
          $permalink = get_permalink();
          $title = get_the_title();
          $featured = get_the_post_thumbnail_url( $query->ID, 'lokali_sidebar' );
          $grad_deo = get_field( 'grad_deo_grada' );
          $ulica = get_field( 'ulica_i_broj' );
          ?>
          <div class="sidebar_card {{ $title }}">
            <a href="{{ $permalink }}" class="sidebar_card_link custom-flex-row">
              <div class="sidebar_card_image">
                <img src="{{ $featured }}" alt="" class="img-fluid">
              </div>
              <div class="sidebar_card_text custom-flex-row">
                <h4 class="sidebar_card_title">{{ $title  }}</h4>
                @if ($grad_deo || $ulica)
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 25" width="10" height="14.7">
                    <path fill="#ccc"
                          d="M8.5,0A8.41,8.41,0,0,0,0,8.29c0,3.4,4.7,11.76,7.18,16a1.53,1.53,0,0,0,2.63,0C12.3,20.05,17,11.69,17,8.29A8.41,8.41,0,0,0,8.5,0Zm0,13.85A5.38,5.38,0,0,1,3.06,8.54,5.23,5.23,0,0,1,4.65,4.78a5.56,5.56,0,0,1,7.7,0,5.23,5.23,0,0,1,1.59,3.76A5.38,5.38,0,0,1,8.5,13.85Z"/>
                  </svg>
                  <div class="sidebar_card_info">
                    <p>{!! $ulica !!}</p>
                    <p>{!! $grad_deo !!}</p>
                  </div>
                @endif
              </div>
            </a>
          </div>
        @endwhile
      @endif
      @php wp_reset_postdata(); @endphp
    </div>
  @endif
  @if($klubovi)
    <div class="sidebar_cards sidebar--custom-mb">
      <h3 class="custom-heading-2"><a href="{{ $klubovi_link }}">{{ $klubovi_text }}</a></h3>
      <?php
      $query = new WP_Query( [
        'post_type'   => 'splavovi_beograd',
        'post__in'    => $splavovi,
        'post_status' => 'any',
        'orderby'     => 'post__in',
      ] );
      ?>
      @if ( $query->have_posts() )
        @while ( $query->have_posts() )
          @php $query->the_post(); @endphp
          <?php
          $permalink = get_permalink();
          $title = get_the_title();
          $featured = get_the_post_thumbnail_url( $query->ID, 'lokali_sidebar' );
          $grad_deo = get_field( 'grad_deo_grada' );
          $ulica = get_field( 'ulica_i_broj' );
          ?>
          <div class="sidebar_card {{ $title }}">
            <a href="{{ $permalink }}" class="sidebar_card_link custom-flex-row">
              <div class="sidebar_card_image">
                <img src="{{ $featured }}" alt="" class="img-fluid">
              </div>
              <div class="sidebar_card_text custom-flex-row">
                <h4 class="sidebar_card_title">{{ $title  }}</h4>
                @if ($grad_deo || $ulica)
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 25" width="10" height="14.7">
                    <path fill="#ccc"
                          d="M8.5,0A8.41,8.41,0,0,0,0,8.29c0,3.4,4.7,11.76,7.18,16a1.53,1.53,0,0,0,2.63,0C12.3,20.05,17,11.69,17,8.29A8.41,8.41,0,0,0,8.5,0Zm0,13.85A5.38,5.38,0,0,1,3.06,8.54,5.23,5.23,0,0,1,4.65,4.78a5.56,5.56,0,0,1,7.7,0,5.23,5.23,0,0,1,1.59,3.76A5.38,5.38,0,0,1,8.5,13.85Z"/>
                  </svg>
                  <div class="sidebar_card_info">
                    <p>{!! $ulica !!}</p>
                    <p>{!! $grad_deo !!}</p>
                  </div>
                @endif
              </div>
            </a>
          </div>
        @endwhile
      @endif
      @php wp_reset_postdata(); @endphp
    </div>
  @endif
  @if($barovi)
    <div class="sidebar_cards sidebar--custom-mb">
      <h3 class="custom-heading-2"><a href="{{ $barovi_link }}">{{ $barovi_text }}</a></h3>      <?php
      $query = new WP_Query( [
        'post_type'   => 'barovi_beograd',
        'post__in'    => $barovi,
        'post_status' => 'any',
        'orderby'     => 'post__in',
      ] );
      ?>
      @if ( $query->have_posts() )
        @while ( $query->have_posts() )
          @php $query->the_post(); @endphp
          <?php
          $permalink = get_permalink();
          $title = get_the_title();
          $featured = get_the_post_thumbnail_url( $query->ID, 'lokali_sidebar' );
          $grad_deo = get_field( 'grad_deo_grada' );
          $ulica = get_field( 'ulica_i_broj' );
          ?>
          <div class="sidebar_card {{ $title }}">
            <a href="{{ $permalink }}" class="sidebar_card_link custom-flex-row">
              <div class="sidebar_card_image">
                <img src="{{ $featured }}" alt="" class="img-fluid">
              </div>
              <div class="sidebar_card_text custom-flex-row">
                <h4 class="sidebar_card_title">{{ $title }}</h4>
                @if ($grad_deo || $ulica)
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 25" width="10" height="14.7">
                    <path fill="#ccc"
                          d="M8.5,0A8.41,8.41,0,0,0,0,8.29c0,3.4,4.7,11.76,7.18,16a1.53,1.53,0,0,0,2.63,0C12.3,20.05,17,11.69,17,8.29A8.41,8.41,0,0,0,8.5,0Zm0,13.85A5.38,5.38,0,0,1,3.06,8.54,5.23,5.23,0,0,1,4.65,4.78a5.56,5.56,0,0,1,7.7,0,5.23,5.23,0,0,1,1.59,3.76A5.38,5.38,0,0,1,8.5,13.85Z"/>
                  </svg>
                  <div class="sidebar_card_info">
                    <p>{!! $ulica !!}</p>
                    <p>{!! $grad_deo !!}</p>
                  </div>
                @endif
              </div>
            </a>
          </div>
        @endwhile
      @endif
      @php wp_reset_postdata(); @endphp
    </div>
  @endif
  @if($restorani)
    <div class="sidebar_cards sidebar--custom-mb">
      <h3 class="custom-heading-2"><a href="{{ $restorani_link }}">{{ $restorani_text }}</a></h3>
      <?php
      $query = new WP_Query( [
        'post_type'   => 'restorani_beograd',
        'post__in'    => $restorani,
        'post_status' => 'any',
        'orderby'     => 'post__in',
      ] );
      ?>
      @if ( $query->have_posts() )
        @while ( $query->have_posts() )
          @php $query->the_post(); @endphp
          <?php
          $permalink = get_permalink();
          $title = get_the_title();
          $featured = get_the_post_thumbnail_url( $query->ID, 'lokali_sidebar' );
          $grad_deo = get_field( 'grad_deo_grada' );
          $ulica = get_field( 'ulica_i_broj' );
          ?>
          <div class="sidebar_card {{ $title }}">
            <a href="{{ $permalink }}" class="sidebar_card_link custom-flex-row">
              <div class="sidebar_card_image">
                <img src="{{ $featured }}" alt="" class="img-fluid">
              </div>
              <div class="sidebar_card_text custom-flex-row">
                <h4 class="sidebar_card_title">{{ $title }}</h4>
                @if ($grad_deo || $ulica)
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 25" width="10" height="14.7">
                    <path fill="#ccc"
                          d="M8.5,0A8.41,8.41,0,0,0,0,8.29c0,3.4,4.7,11.76,7.18,16a1.53,1.53,0,0,0,2.63,0C12.3,20.05,17,11.69,17,8.29A8.41,8.41,0,0,0,8.5,0Zm0,13.85A5.38,5.38,0,0,1,3.06,8.54,5.23,5.23,0,0,1,4.65,4.78a5.56,5.56,0,0,1,7.7,0,5.23,5.23,0,0,1,1.59,3.76A5.38,5.38,0,0,1,8.5,13.85Z"/>
                  </svg>
                  <div class="sidebar_card_info">
                    <p>{!! $ulica !!}</p>
                    <p>{!! $grad_deo !!}</p>
                  </div>
                @endif
              </div>
            </a>
          </div>
        @endwhile
      @endif
      @php wp_reset_postdata(); @endphp
    </div>
  @endif
  @if($striptizklubovi)
    <div class="sidebar_cards sidebar--custom-mb">
      <h3 class="custom-heading-2"><a href="{{ $striptizklubovi_link }}">{{ $striptizklubovi_text }}</a></h3>
      <?php
      $query = new WP_Query( [
        'post_type'   => 'restorani_beograd',
        'post__in'    => $restorani,
        'post_status' => 'any',
        'orderby'     => 'post__in',
      ] );
      ?>
      @if ( $query->have_posts() )
        @while ( $query->have_posts() )
          @php $query->the_post(); @endphp
          <?php
          $permalink = get_permalink();
          $title = get_the_title();
          $featured = get_the_post_thumbnail_url( $query->ID, 'lokali_sidebar' );
          $grad_deo = get_field( 'grad_deo_grada' );
          $ulica = get_field( 'ulica_i_broj' );
          ?>
          <div class="sidebar_card {{ $title }}">
            <a href="{{ $permalink }}" class="sidebar_card_link custom-flex-row">
              <div class="sidebar_card_image">
                <img src="{{ $featured }}" alt="" class="img-fluid">
              </div>
              <div class="sidebar_card_text custom-flex-row">
                <h4 class="sidebar_card_title">{{ $title }}</h4>
                @if ($grad_deo || $ulica)
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 25" width="10" height="14.7">
                    <path fill="#ccc"
                          d="M8.5,0A8.41,8.41,0,0,0,0,8.29c0,3.4,4.7,11.76,7.18,16a1.53,1.53,0,0,0,2.63,0C12.3,20.05,17,11.69,17,8.29A8.41,8.41,0,0,0,8.5,0Zm0,13.85A5.38,5.38,0,0,1,3.06,8.54,5.23,5.23,0,0,1,4.65,4.78a5.56,5.56,0,0,1,7.7,0,5.23,5.23,0,0,1,1.59,3.76A5.38,5.38,0,0,1,8.5,13.85Z"/>
                  </svg>
                  <div class="sidebar_card_info">
                    <p>{!! $ulica !!}</p>
                    <p>{!! $grad_deo !!}</p>
                  </div>
                @endif
              </div>
            </a>
          </div>
        @endwhile
      @endif
      @php wp_reset_postdata(); @endphp
    </div>
  @endif
<!-- End of Lokali acf global -->
  @if(!is_single())
  <!-- Tekstovi acf global  -->
    @if($tekstovi)
      <div class="featured_texts">
        <h3 class="custom-heading-2"><a href="{{ $najcitaniji_link }}">{{ $najcitaniji_text }}</a></h3>
        <?php
        $query = new WP_Query( [
          'post_type'   => 'post',
          'post__in'    => $tekstovi,
          'post_status' => 'any',
          'orderby'     => 'post__in',
        ] );
        ?>
        @if ( $query->have_posts() )
          @while ( $query->have_posts() )
            @php $query->the_post(); @endphp
            <?php
            $permalink = get_permalink();
            $title = get_the_title();
            $featured = get_the_post_thumbnail_url( $query->ID, 'tekstovi_sidebar' );
            $date = get_the_date();
            ?>
            <div class="featured_text tekst-{{ get_the_ID()  }}">
              <a href="{{ $permalink }}" class="featured_text_link"></a>
              <img src="" data-srcset="{{ $featured }}" alt="">
              <div class="featured_text_info_container custom-flex-row">
                <h4 class="sidebar_text_title">{!! $title !!}</h4>
                <div class="featured_text_info custom-flex-row">
                  <a href="#" class="featured_text_tag button">Najave</a>
                  <p>{{ $date  }}</p>
                </div>
              </div>
            </div>
          @endwhile
        @endif
        @php wp_reset_postdata(); @endphp
      </div>
    @endif
  <!-- Tekstovi acf global  -->
  @endif
</aside>
