<div class="price-list custom-mb-big">
  <div class="price-list-heading custom-flex-row">
    <p class="custom-heading-2">Cross cenovnik</p>
    <a class="price-link" href="@asset('images/image-2.jpg')" data-lightbox="image-1">
      <img src="@asset('images/beograd_nocu__klub_meni_jelovnik.svg')" alt="" class="img-fluid">
    </a>
    <a class="price-link" href="#">
      <img src="@asset('images/beograd_nocu__klub_meni_karta_pica.svg')" alt="" class="img-fluid">
    </a>
    <p>U ponudi se uvek nalazi i posni Cross meni!</p>
  </div>
  <div class="price-list-content">
    <div class="custom-flex-row">
      <div class="column left-col">
        <p class="tab-heading">Restoran Cross cene (hrana) / Cross restoran meni:</p>
        <p>doručak od 320 do 490 dinara</p>
        <p>doručak se služi do 12:30h</p>
        <p>snedviči od 550 do 750 dinara</p>
        <p>supe, čorbe, potaži 250 dinara</p>
        <p>ručak glavno jelo od 650 do 1690 dinara</p>
        <p>salate od 280 do 550 dinara</p>
        <p>obrok salate od 790 do 990 dinara</p>
        <p>deserti od 290 do 520 dinara</p>
        <p>večera od 650 do 1690 dinara</p>
      </div>
      <div class="column right-col">
        <p class="tab-heading">Restoran Cross cene (piće) / Cenovnik:</p>
        <p>flaša žestine oko 5000 dinara</p>
        <p>flaša vina oko 2500 dinara</p>
        <p>kokteli oko 450 dinara</p>
      </div>
    </div>
  </div>
</div>
