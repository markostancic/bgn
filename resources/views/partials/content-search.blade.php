<div class="lokali_card">
  <div class="lokali_card_image">
    <a href="{{ $result['link'] }}">
      <?php if ($result['image']) : ?>
      <picture>
        <?php
        get_webp_image_type([$result['image'] => '']);
        ?>
        <img src="" data-srcset="{{ $result['image']  }}" alt="" class="img-fluid">
      </picture>
      <?php endif; ?>
    </a>
  </div>
  <div class="lokali_card_text">
    <a href="<?php the_permalink() ?>">
      <h2 class="lokali_card_heading">{!!  $result['post_title']   !!}</h2>
      <p class="info"><?php _e('Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h', 'beogradnocu'); ?></p>
    </a>
    <a href="#" class="button yellow"><?php _e('Rezerviši online', 'beogradnocu'); ?></a>
    @if($result['phone'])
      <a href="tel:{{ $result['phone'] }}" class="button blue">
        <img src="@asset('images/beograd_nocu__general_header_rezervacije.svg')" alt=""
             class="img-fluid">{{ $result['phone_preview'] }}</a>
    @endif
  </div>
</div>
