<div class="reservation custom-mb-big">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-11 mx-auto custom-flex-row">
                <div class="column">
                    <p>
                    <?php _e('Rezervacije su obavezne. Putem korisničkog Call centra ili online možete potpuno besplatno da napravite sigurnu rezervaciju.', 'beogradnocu')?>
                    </p>
                </div>
                <div class="column custom-flex-column">
                    <a href="#">
                        <span><?php _e('Rezerviši ovde', 'beogradnocu')?></span>
                        <div class="image">
                            <img src="" data-srcset="@asset('images/beograd_nocu__general_cta_rezervisi.svg')" alt="">
                        </div>
                    </a>
                    @if(get_field('telefon_kod', 'option'))
                        <p>Ili pozovi: <a href="tel:+{{ get_field('telefon_kod', 'option') }}">{{ get_field('telefon_prikaz', 'option') }}</a></p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>