<div class="col-12 map custom-mb-big">
  <div class="partials-heading">
    @if($term)
      <p class="custom-heading-4 text-center">Mapa - {{ $term->name  }} {!! get_the_title() !!}</p>
    @endif
  </div>
  <div id="map">
    <img src="@asset('images/map.png')" alt="">
  </div>
</div>
