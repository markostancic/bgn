<?php
$terms_izvodjaci = get_field('izvodjaci');
if( $terms_izvodjaci ): ?>
<div class="similar_songs">
  <h2>Najpopularniji IZVOĐAČI</h2>
</div>

  <?php foreach( $terms_izvodjaci as $term_i ): ?>
  <h3 class="similar_content_songs"> <a class="text_category post-category" href="<?php echo esc_url( get_term_link( $term_i ) ); ?>"><?php echo esc_html( $term_i->name ); ?></a></h3>
  <?php endforeach; ?>

<?php endif; ?>
