<div class="col-12 related-text custom_cards custom-flex-row custom-mb-big">
  <div class="custom-card-heading custom-flex-row partials-heading">
    <h4 class="custom-heading-4">Povezani tekstovi</h4>
    <a href="{{ site_url() }}/magazin" class="button">Svi tekstovi</a>
  </div>

  <?php $category = get_the_category()[0] ?>
  @php $args = [
  "post_type"=>"post",
  "posts_per_page"=> 4,
   'tax_query'      => [
                [
                  'taxonomy' => 'category',
                  'terms'    =>  $category->term_id,
                  'field'    => 'term_id',
                ],
              ]
  ]; @endphp

  @php $query = new WP_Query( $args ); @endphp
  @if ( $query->have_posts() )
    @while ( $query->have_posts() )
      @php $query->the_post(); @endphp
      <?php
      $title_post = get_the_title();
      $thumbnail = get_the_post_thumbnail_url($query->ID, 'single_povezan');
      $permalink = get_permalink();
      ?>
      <div class="custom-card">
        <a href="{{ $permalink  }}" class="custom-card-link custom-flex-row">
          <div class="custom-card-image">
            <picture>
              <?php
              get_webp_image_type([$thumbnail => '']);
              ?>
              <img src="" data-srcset="{{ $thumbnail  }}" alt="">
            </picture>
          </div>
          <h3 class="custom-card-title">{!! $title_post !!}</h3>
          <div class="custom-card-info">
            <p>{!! get_the_date() !!}</p>
          </div>
        </a>
      </div>
    @endwhile
  @endif
  @php wp_reset_postdata(); @endphp
</div>
