<?php
$thumbnail = get_the_post_thumbnail_url(get_the_ID(), 'lokali_logo');
$radno_vreme = get_field('radno_vreme');
$ulica = get_field('ulica_i_broj');
$grad = get_field('grad_deo_grada');
$kapacitet = get_field('kapacitet');
$parking = get_field('parking');
$pozicije = get_field('tipovi_pozicija');
$najave = get_field('najave');
$program = get_field('program');
$ssnp = get_field('ssnp_google_lokacija');
$gmap_location = explode(',', $ssnp);
?>
<div class="col-12 col-lg-8 custom-flex-row main-col">
  <div class="heading custom-flex-row custom-mb-small">
    <h1>{!! get_the_title() !!}</h1>
    <button type="button" class="button yellow">Rezerviši</button>
  </div>
  <div class="tags custom-mb-small">
    <?php getSpecificTaxTerms('vrsta_listinga') ?>
  </div>
  <div class="d-lg-none reserve-mobile custom-mb-big">
    <button type="button" class="button yellow">Rezerviši</button>
  </div>
  <div class="work-status custom-flex-row custom-mb-small">
    <div class="column">
      <img src="@asset('images/beograd_nocu__klub_otvoren.svg')" alt="" class="img-fluid">
      @if($term)
        <p class="custom-heading-2">{{ $term->name  }} {!! get_the_title() !!} je otvoren</p>
      @endif
    </div>
    @if($radno_vreme)
      <div class="column">
        <p class="regular-text">{{ $radno_vreme  }}</p>
      </div>
    @endif
  </div>
  <div class="telephone custom-flex-row custom-mb-small">
    <div class="column">
      <p class="custom-heading-2">Telefoni za rezervacije</p>
    </div>
    @if(get_field('telefon_kod', 'option'))
      <div class="column">
        <a href="tel:+{{ get_field('telefon_kod', 'option') }}"
           class="blue-link">{{ get_field('telefon_prikaz', 'option') }}</a>
      </div>
    @endif
  </div>
  @if($thumbnail)
    <div class="local-info-image">
      <img src="{{ $thumbnail }}" alt="" class="img-fluid">
    </div>
  @endif
  <div class="local-info">
    <div class="info">
      <img src="@asset('images/beograd_nocu__klub_adresa.svg')" alt="" class="img-fluid">
      <a href="#">{{ $ulica }}</a>
    </div>
    <div class="info">
      <img src="@asset('images/beograd_nocu__klub_podrucje.svg')" alt="" class="img-fluid">
      <a href="#">{{ $grad  }}</a>
    </div>
    <div class="info">
      <img src="@asset('images/beograd_nocu__klub_radno_vreme.svg')" alt="" class="img-fluid">
      <p>{{ $radno_vreme  }}</p>
    </div>
    <div class="info">
      <img src="@asset('images/beograd_nocu__klub_vrsta_muzike.svg')" alt="" class="img-fluid">
      <p>House</p>
    </div>
    <div class="info">
      <img src="@asset('images/beograd_nocu__klub_broj_mesta.png')" alt="" class="img-fluid">
      <p>{{ $kapacitet  }} ljudi</p>
    </div>
    <div class="info">
      <img src="@asset('images/beograd_nocu__klub_tip_sedenja.svg')" alt="" class="img-fluid">
      <p>
        <?php
        if (have_rows('tipovi_pozicija')) :
          while (have_rows('tipovi_pozicija')) : the_row();
            $naziv_pozicije = get_sub_field('naziv_pozicije');
            if (get_row_index() != 1) {
              echo ', ';
            }
            echo $naziv_pozicije;
          endwhile;
        endif;
        ?>
      </p>

    </div>
    <?php if ($parking) : ?>
    <div class="info">
      <img src="@asset('images/beograd_nocu__klub_parking.svg')" alt="" class="img-fluid">
      <p>Dostupan parking</p>
    </div>
    <?php endif; ?>
  </div>
  <div class="upcomming-events events">
    <p class="custom-heading-3">Najave</p>
    <?php
    if (have_rows('najave')) :
    while (have_rows('najave')) : the_row();
    $tekst_najave = get_sub_field('tekst_najave');
    $date = get_sub_field('datum');
    $today = date('d.m.Y.');
    $nameOfDay = date('D', strtotime($date));
    $nameOfMonth = date("F", strtotime($date));
    $numDay = date('d', strtotime($date));
    $loc_day = day($nameOfDay);
    ?>
    @if($date >= $today)
      <div class="custom-flex-row">
        <button type="button" class="button yellow">Rezerviši</button>
        <div class="event-type">
          <p class="custom-heading-2">{{ $tekst_najave  }}</p>
          {{ $date  }}
          <p>{{ $loc_day  }} {{ $numDay }}. {{ $nameOfMonth }}</p>
        </div>
      </div>
    @endif
    <?php endwhile; endif; ?>
  </div>
  @if($term)
    <div class="local-itinerary events custom-mb-big">
      <p class="custom-heading-3">Program {{ $term->name }}a</p>
      <?php
      if (have_rows('program')) :
      while (have_rows('program')) : the_row();
      $tekst_najave = get_sub_field('tekst_programa');
      $date = get_sub_field('datum');
      $today = date('d.m.Y.');
      $nameOfDay = date('D', strtotime($date));
      $nameOfMonth = date("F", strtotime($date));
      $numDay = date('d', strtotime($date));
      $loc_day = day($nameOfDay);
      ?>
      @if($date >= $today)
        <div class="custom-flex-row">
          <button type="button" class="button yellow">Rezerviši</button>
          <div class="event-type">
            <p class="custom-heading-2">{{ $tekst_najave  }}</p>
            {{ $date  }}
            <p>{{ $loc_day  }} {{ $numDay }}. {{ $nameOfMonth }}</p>
          </div>
        </div>
      @endif
      <?php endwhile; endif; ?>
    </div>
  @endif
  <section class="local-content main-content custom-mb-big">
    @include('partials.content-page')
  </section>
  @include('partials.price-list')
</div>
