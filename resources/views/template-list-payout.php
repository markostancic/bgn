<?php

/*
Template Name: App List Payout
*/

?>

<!DOCTYPE HTML>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>Pregled unovčavanja</title>
    <style>
    div {
        margin-left: 100px;
        margin-right: 40px;
    }
    table, th, td {
        border: 1px solid black;
    }
    </style>
</head>
<body>
    <div>
        <h2>Pregled mesta za unovčavanje</h2>
        <table id='table1' style="width:100%">
            <tr>
                <th>Mesto</th>
                <th>Opis</th>
                <th>Akcija</th>
            </tr>
            <?php
      $servername = "127.0.0.1";
      $username = "root";
      $password = "";
      $dbname = "nocu_app";

                $sql = "SELECT * FROM payout";
                try {
                    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();

                    $rows = $stmt->fetchAll();
                    foreach ($rows as $row) {
                        $promotion = array();
                        $promotion['placeId'] = $row['place_id'];
                        $promotion['desc'] = $row['description'];
                        $promotion['uuid'] = $row['uuid'];

                        $postdata = get_post($promotion['placeId'], ARRAY_A);
                        if ($postdata != null) {
                            $promotion['name'] = htmlspecialchars($postdata['post_title']);
                        }

                        echo '<tr>';
                        echo '<td>'.$promotion['name'].'</td>';
                        echo '<td>'.$promotion['desc'].'</td>';
                        echo "<td><a href='https://www.beogradnocu.com/app-list-promo/'".$promotion['uuid']."'>Promeni</a></td>";
                        echo '</tr>';
                    }
                } catch (PDOException $e) {
                    echo "Error: " . $e->getMessage();
                }

            ?>
        </table>
    </div>
</body>
</html>
