{{--
  Template Name: Galerija grupna single lokal
--}}

@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      @include('partials.breadcrumb')
      <div class="col-12 col-lg-8 gallery_group">
        <div class="gallery_hero">
          <h1 class="gallery_group_lokal">{{ the_title() }}</h1>
          <div class="description">
            {!! the_content() !!}
          </div>
        </div>
        @include('partials.datepicker')
        <div class="gallery gallery_group_cards custom_cards custom-flex-row custom-mb-big">
          <?php
          $current_date = date( 'd-m-Y' );
          $date_2 = date( 'Ymd', strtotime( $current_date ) );
          $posts = new WP_Query( [
            'post_type'      => 'galerija',
            'status'         => 'published',
            'posts_per_page' => - 1,
            'meta_query'     => array(
              array(
                'key'     => 'datum_galerije',
                'value'   => $date_2,
                'compare' => '=',
                'type'    => 'DATE',
              ),
            ),
          ] );


          if ($posts->have_posts()) {
          while ($posts->have_posts()) {
          $posts->the_post();

          $lokal = get_field('lokal');
          $lokal_url = get_permalink($lokal->ID);
          $gal_date = get_field( 'datum_galerije' );
          $date = strtotime( $gal_date );
          $gal_date = date( 'd.m.Y', $date );
          $gallery = get_field( 'galerija' );
          $gal_featured = $gallery[0]['url_do_fotografije'];
          ?>
          <div class=" custom-card">
            <a href="{{ $lokal_url }}" class="custom-card-link custom-flex-row">
              <div class="custom-card-image">
                <img srcset=""
                     src="{{ $gal_featured }}"
                     alt="" class="img-fluid">
              </div>
              <h3 class="custom-card-title">{{ the_title() }}</h3>
              <div class="custom-card-info">
                <p>Datum: {{ $gal_date }}</p>
                <p>Broj foto: {{ count($gallery) }}</p>
              </div>
            </a>
          </div>
          <?php
          }
          }
          wp_reset_postdata();
          ?>
        </div>
      </div>
    </div>
      @include('partials.sidebar')
    </div>
  </div>
  @include('partials.reservation')
  <div class="container">
    <div class="row">
      @include('partials.featured')
    </div>
  </div>
@endsection
