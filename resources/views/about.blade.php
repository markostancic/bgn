{{--
  Template Name: O nama
--}}


@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      @include('partials.breadcrumb')
      <div class="col-12 col-lg-8">
        <h1>{!! get_the_title() !!}</h1>
        <div class="main-content custom-mb-big">
          <?php the_content(); ?>
        </div>
        @include('partials.form')
      </div>
      @include('partials.sidebar')
    </div>
  </div>
  @include('partials.reservation')
  <div class="container">
    <div class="row">
      @include('partials.featured')
    </div>
  </div>
@endsection
