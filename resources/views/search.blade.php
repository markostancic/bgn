@extends('layouts.app')

@section('content')
  <section class="search-results">
    <div class="container">
      <div class="row">
        @include('partials.breadcrumb')
        <div class="col-lg-8 search-wrapper">
          <h1 class="title"><?php _e('Rezultati pretrage:', 'bgn'); ?> {{ get_query_var('s') }}</h1>
          @if (!have_posts())
            <div class="alert alert-warning">
              {{ __('Sorry, no results were found.', 'sage') }}
            </div>
            {!! get_search_form(false) !!}
          @endif
          <div class="lokali_cards custom-flex-row">
            <?php
            $counter_bar = 0;
            $counter = 0;
            $results = [];
            $post_object = get_post_type_object(get_post_type(get_the_ID()));
            ?>
            @while(have_posts()) @php the_post() @endphp
            <?php
            global  $post;
            $featured_img_url = get_the_post_thumbnail_url($post->ID, 'lokali');
            $post_id = $post->ID;
            $link = get_the_permalink($post->ID);
            $phone = get_field('telefon_kod', 'option');
            $phone_view = get_field('telefon_prikaz', 'option');
            $local_type = $terms = get_terms([
              'taxonomy' => 'vrsta_listinga',
              'hide_empty' => false,
            ]);
            $results[] = [
              'lokal' => $post_object->name,
              'post_type' => $post_object->label,
              'post_title' => get_the_title($post->ID),
              'image' => $featured_img_url,
              'link' => $link,
              'phone' => $phone,
              'phone_preview' => $phone_view,
              'id' => $post_id,
              'category' => $local_type[0]->name

            ];
            ?>
            @endwhile
            <?php
            $results = array_group_by($results, "lokal");
            ?>
            @foreach($results[$post_object->name] as $result_key => $result)
              {!! $result_key == 0 ? '<h2>'. __($post_object->label, 'beogradnocu') .'</h2>' : '' !!}
              <div class="lokali_card">
                <div class="lokali_card_image">
                  <a href="{{ $result['link'] }}">
                    <?php if ($result['image']) : ?>
                    <picture>
                      <?php
                      get_webp_image_type([$result['image'] => '']);
                      ?>
                      <img src="" data-srcset="{{ $result['image']  }}" alt="" class="img-fluid">
                    </picture>
                    <?php endif; ?>
                  </a>
                </div>
                <div class="lokali_card_text">
                  <a href="{{ $result['link'] }}">
                    <h3 class="lokali_card_heading">{!!  $result['post_title']   !!}</h3>
                    <p
                      class="info"><?php _e('Strana lagana / Jazz / Soft rock / Pop vokal od 12h do 02h', 'beogradnocu'); ?></p>
                  </a>
                  <a href="#" class="button yellow"><?php _e('Rezerviši online', 'beogradnocu'); ?></a>
                  @if($result['phone'])
                    <a href="tel:{{ $result['phone'] }}" class="button blue">
                      <img src="@asset('images/beograd_nocu__general_header_rezervacije.svg')" alt=""
                           class="img-fluid">{{ $result['phone_preview'] }}</a>
                  @endif
                </div>
              </div>
            @endforeach
            @php(wp_reset_postdata())
          </div>
          <?php
          $args = [
            'post_type' => 'post',
            'posts_per_page' => 3,
            'orderby' => 'title',
            'order' => 'asc',
          ];

          $custom_query = new WP_Query($args);
          if (get_field('vesti_pretraga', 'option')):
            $posts = get_field('vesti_pretraga', 'option');
          else:
            $posts = $custom_query->posts;
          endif;

          if ($posts) :
          ?>
          <div class="col-12 custom_cards search-related related-text custom-flex-row custom-mb-big">
            <div class="custom-card-heading custom-flex-row partials-heading">
              <h4 class="custom-heading-4"><?php _e('Vesti', 'beogradnocu') ?></h4>
            </div>
            @foreach($posts as $post)
              <?php
              $post_image = get_the_post_thumbnail_url($post->ID, 'single_povezan') ? get_the_post_thumbnail_url($post->ID, 'single_povezan') : get_template_directory_uri() . '/assets/images/img_holder.jpg';
              ?>
              <div class="custom-card">
                <a href="{{ get_permalink() }}" class="custom-card-link custom-flex-row">
                  <div class="custom-card-image">
                    <img src="{{ $post_image }}" data-srcset="{{ $post_image }}" alt="" class="img-fluid">
                  </div>
                  <div class="custom-card-content">
                    <h3 class="custom-card-title">{!! get_the_title() !!}</h3>
                    <div class="custom-card-info">
                      <p>{{ get_the_date() }}</p>
                    </div>
                  </div>
                </a>
              </div>
            @endforeach
          </div>
          <?php endif; ?>
        </div>
        @include('partials.sidebar')
      </div>
    </div>
  </section>
@endsection

