<?php
/*
  Template Name: MRS
 */
get_header();
$slike = bg_mrsq();
// echo ABSPATH;
// print_r($slike);
?>
<?php get_template_part("partials-new/main-banner"); ?>
<div class="container">
    <?php beogradnocu_breadcrumbs(); ?>
    <div class="clearfix"></div>
    <div id="content" class="clearfix row">
        <div id="main" class="col-md-8 col-xs-12 first" role="main">
            <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
                <section class="lead">
                    <header class="colored-bg orange">
                        <h1>
                            <?php the_title(); ?>
                        </h1>
                    </header>
                    <?php
                    global $paged;
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; //For pagination

                    $brojalbuma = get_option('beogradnocu_brojalbuma_nastrani', '5');
                    $brojalbuma = $brojalbuma * 4;
                    $paged2 = $brojalbuma * ($paged - 1);

                    $tr = '1';
                    $minpic = $paged2;
                    $maxpic = $paged2 + $brojalbuma;

                    $total = intval(count($slike) / $brojalbuma);
// print_r($slike);
                    ?>

                    <div class="gallery-wrapper cf row-new">
                        <?php
                        $slike = array_reverse($slike);
                        foreach ($slike as $name) {
                            ?>
                            <div class="col-sm-3 col-xs-2 element gallery-item">
                                <?php
                                if ($tr > $minpic && $tr <= $maxpic) {
                                    ?>

                                    <a href="<?php echo $name['images']['0']['source']; ?>" title="" rel="prettyPhoto[gallery1]"><img src="<?php bloginfo('template_url'); ?>/php/timthumb.php?src=<?php echo $name['images']['0']['source']; ?>&amp;h=153&amp;w=215&amp;q=100&amp;a=tr" width="215" height="153" alt="" /></a> <?php
                        } else {
                                    ?>
                                    <a href="<?php echo $name['images']['0']['source']; ?>" style="display:none;" title="" rel="prettyPhoto[gallery1]"></a>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                            $tr++;
                        }//end foreach
                        ?>
                    </div>
                </section>
                <?php include(BEOGRADNOCU_INCLUDES . 'pagination-custom.php'); ?>
                <?php include(BEOGRADNOCU_INCLUDES . 'text-on-local-list.php'); ?>
            </article>
        </div>
        <?php get_sidebar(); // sidebar 2 ?>
    </div>
</div>
<?php
get_footer();
?>
