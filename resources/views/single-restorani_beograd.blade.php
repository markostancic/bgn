@extends('layouts.app')

@section('content')
  <section class="single-lokal">
    <div class="container">
      <div class="row">
      @include('partials.breadcrumb')
      <!-- TODO ubaciti linkove za rezervacije -->
        <!-- TODO ubaciti cenovnik -->
        <!-- TODO ubaciti muzicki zanr -->
        <?php $term = '';
        $terms = get_the_terms(get_the_ID(), 'vrsta_listinga');
        if ($terms) {
          $term = array_pop($terms);
        } ?>
        @include('partials.local', ['term' => $term])
        @include('partials.sidebar')
      </div>
    </div>
    @include('partials.reservation')
    <div class="container">
      <div class="row">
        @include('partials.map', ['term' => $term])
        @include('partials.gallery')
        @include('partials.featured')
      </div>
    </div>
  </section>
  @include('partials.modal')
@endsection
