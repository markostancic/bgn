{{--
  Template Name: Galerija grupna svi lokali
--}}

@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			@include('partials.breadcrumb')
			<div class="col-12 col-lg-8 gallery_group">
				<div class="gallery_hero">
					<h1>Slike iz noćnog provoda u Beogradu</h1>
				</div>
				<div class="gallery gallery_group_cards custom_cards custom-flex-row custom-mb-big">
					<?php
					$query = new WP_Query( [
						'post_type'      => 'galerija',
						'posts_per_page' => -1
					] );
					?>
					@if ( $query->have_posts() && false )
						<?php    $terms_vrste = get_the_terms( $query->ID, 'vrsta_listinga' ); ?>
						<div class="custom-card-heading custom-flex-row partials-heading">
							<h2 class="custom-heading-2">Galerije
								<?php if ($terms_vrste = 'Splav') { ?>
								Splava
								<?php } else { ?>
								restoran
								<?php } ?>
							</h2>
							<a href="#" class="button">Sve galerije</a>
						</div>
						@while ( $query->have_posts() )
							@php $query->the_post(); @endphp
							<div class=" custom-card">
								<?php
								$featured_img_url = get_the_post_thumbnail_url( $query->ID, 'galerija-group' );
								$terms_lokal = get_the_terms( $query->ID, 'lokali' );
								$terms_vrste = get_the_terms( $query->ID, 'vrsta_listinga' );
								$gal = get_field( 'galerija' );
								$count_gallery = ( count( $gal ) );
								$gal_date = get_field( 'datum_galerije' );
								?>
								<a href="<?php the_permalink()  ?>" class="custom-card-link custom-flex-row">
									<div class="custom-card-image">
										<img src="{{ $featured_img_url }}" data-srcset="" alt="" class="img-fluid ">
									</div>
									<h3 class="custom-card-title">{{ $terms_vrste[0]->name }} {{ $terms_lokal[0]->name  }}</h3>
									<div class="custom-card-info">
										<p>Datum: {{ $gal_date  }}</p>
										<p>Broj foto: {{ $count_gallery  }}</p>
									</div>
								</a>
							</div>
						@endwhile
					@endif
					@php wp_reset_postdata(); @endphp
				</div>

				<div class="gallery gallery_group_cards custom_cards custom-flex-row custom-mb-big">
					<div class="custom-card-heading custom-flex-row partials-heading">
						<h2 class="custom-heading-2">Galerije kafana</h2>
						<a href="#" class="button">Sve galerije</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="/wp-content/uploads/2020/04/Kafana-Krcma-logo-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Kafana Krčma</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="/wp-content/uploads/2020/04/Kafana-Tarapana-logo-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Kafana Tarapana</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="/wp-content/uploads/2020/04/Kafana-Sipaj-Ne-Pitaj-logo-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Kafana Sipaj Ne Pitaj</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="/wp-content/uploads/2019/07/Splav-Na-Vodi-Kafana-logo-1-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Na Vodi kafana</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
				</div>

				<div class="gallery gallery_group_cards custom_cards custom-flex-row custom-mb-big">
					<div class="custom-card-heading custom-flex-row partials-heading">
						<h2 class="custom-heading-2">Galerije klubova</h2>
						<a href="#" class="button">Sve galerije</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="" data-srcset="/wp-content/uploads/2020/03/Klub-Ellingtons-logo-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Ellington`s Bar</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="" data-srcset="/wp-content/uploads/2020/03/Klub-Kafana-Tarapana-logo-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Klub Kafana Tarapana</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="" data-srcset="/wp-content/uploads/2019/09/Klub-Magic-Diamond-logo-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Magic diamond</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="" data-srcset="/wp-content/uploads/2020/03/Cloud-logo-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Cloud Festivalska Platforma</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
				</div>

				<div class="gallery gallery_group_cards custom_cards custom-flex-row custom-mb-big">
					<div class="custom-card-heading custom-flex-row partials-heading">
						<h2 class="custom-heading-2">Galerije barova</h2>
						<a href="#" class="button">Sve galerije</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="" data-srcset="/wp-content/uploads/2019/10/Konzulat-Bar-Logo-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Konzulat Bar</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="" data-srcset="/wp-content/uploads/2019/05/Shisha-Bar-Dvojka-logo-1-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Shisha Bar Dvojka</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="" data-srcset="/wp-content/uploads/2019/04/Klub-Shisha-Hill-logo-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Shisha Hill</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
					<div class="custom-card">
						<a href="#" class="custom-card-link custom-flex-row">
							<div class="custom-card-image">
								<img src="" data-srcset="/wp-content/uploads/2019/02/Boho-Bar-logo-300x217.jpg" alt="" class="img-fluid ">
							</div>
							<h3 class="custom-card-title">Boho Bar</h3>
							<div class="custom-card-info">
								<p>Datum: 19.05.2019.</p>
								<p>Broj foto: 32</p>
							</div>
						</a>
					</div>
				</div>
			</div>
			@include('partials.sidebar')
		</div>
	</div>
	@include('partials.reservation')
	<div class="container">
		<div class="row">
			@include('partials.featured')
		</div>
	</div>
@endsection
