{{--
  Template Name: Nova godina zbirna
--}}

@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			@include('partials.breadcrumb')
			<div class="col-12 col-lg-8">
				<div class="banners">
					<a href="#">
						<img src="https://www.beogradnocu.com/wp-content/uploads/2019/09/Docek-Nove-godine-Beograd-2020-Hotel-Hyatt-Regency-baner.jpg" alt="">
					</a>
					<a href="#">
						<img src="https://www.beogradnocu.com/wp-content/uploads/2019/09/Docek-Nove-godine-2020-Inter-Hollywood-Ledine-baner.jpg" alt="">
					</a>
					<a href="#">
						<img src="https://www.beogradnocu.com/wp-content/uploads/2019/10/Docek-Nove-godine-2020-Beograd-Hotel-Hilton-baner.jpg" alt="">
					</a>
					<a href="#">
						<img src="https://www.beogradnocu.com/wp-content/uploads/2019/09/Docek-Nove-godine-Beograd-2020-Klub-Gotik-baner-1.jpg" alt="">
					</a>
				</div>

				<div class="event__filter">
					<div class="custom_checkbox_container">
						<input type="checkbox" id="hoteli" class="custom_checkbox">
						<label for="hoteli">Hoteli</label>
					</div>
					<div class="custom_checkbox_container">
						<input type="checkbox" id="restorani" class="custom_checkbox">
						<label for="restorani">Restorani</label>
					</div>
					<div class="custom_checkbox_container">
						<input type="checkbox" id="klubovi" class="custom_checkbox">
						<label for="klubovi">Klubovi/Splavovi</label>
					</div>
					<div class="custom_checkbox_container">
						<input type="checkbox" id="kafane" class="custom_checkbox">
						<label for="kafane">Kafane</label>
					</div>
					<div class="custom_checkbox_container">
						<input type="checkbox" id="pabovi" class="custom_checkbox">
						<label for="pabovi">Pabovi/Barovi</label>
					</div>
				</div>

				<h1>Docek Nove Godine 2021 – Nova Godina 2021 Beograd</h1>

				<div class="lokali_cards custom-flex-row">
					<div class="lokali_card">
						<div class="lokali_card_image">
							<a href="#">
								<picture>
									<img src="https://www.beogradnocu.com/wp-content/uploads/2019/10/Docek-Nove-godine-2020-Beograd-City-Hall-baner.jpg" alt="" class="img-fluid">
								</picture>
							</a>
						</div>
						<div class="lokali_card_text">
							<a href="#">
								<h2 class="lokali_card_heading">Cross Bar</h2>
								<p class="info">Maya Berović</p>
								<p class="info">30 € + rezervacija</p>
							</a>
						</div>
					</div>
					<div class="lokali_card">
						<div class="lokali_card_image">
							<a href="#">
								<picture>
									<img src="https://www.beogradnocu.com/wp-content/uploads/2019/09/Docek-Nove-godine-Beograd-2020-Restoran-Hollywood-Ledine-baner-1.jpg" alt="" class="img-fluid">
								</picture>
							</a>
						</div>
						<div class="lokali_card_text">
							<a href="#">
								<h2 class="lokali_card_heading">Toro Restoran</h2>
								<p class="info">Edita</p>
								<p class="info">od 140 € sa hranom</p>
							</a>
						</div>
					</div>
					<div class="lokali_card">
						<div class="lokali_card_image">
							<a href="#">
								<picture>
									<img src="https://www.beogradnocu.com/wp-content/uploads/2019/11/Docek-Nove-godine-Beograd-2020-Hotel-Majdan-Sala-3-baner.jpg" alt="" class="img-fluid">
								</picture>
							</a>
						</div>
						<div class="lokali_card_text">
							<a href="#">
								<h2 class="lokali_card_heading">Splav Leto</h2>
								<p class="info">Slađa Allegro</p>
								<p class="info">od 65 € sa hranom i pićem</p>
							</a>
						</div>
					</div>
					<div class="lokali_card">
						<div class="lokali_card_image">
							<a href="#">
								<picture>
									<img src="https://www.beogradnocu.com/wp-content/uploads/2019/09/Docek-Nove-godine-Beograd-2020-svecane-sale-Nera-baner.jpg" alt="" class="img-fluid">
								</picture>
							</a>
						</div>
						<div class="lokali_card_text">
							<a href="#">
								<h2 class="lokali_card_heading">Magic Diamond klub</h2>
								<p class="info">Darko Lazić i Dara Bubamara</p>
								<p class="info">od 65 € sa hranom i pićem</p>
							</a>
						</div>
					</div>
					<div class="lokali_card">
						<div class="lokali_card_image">
							<a href="#">
								<picture>
									<img src="https://www.beogradnocu.com/wp-content/uploads/2019/11/Docek-Nove-godine-2020-Beograd-Klub-Hype-baner.jpg" alt="" class="img-fluid">
								</picture>
							</a>
						</div>
						<div class="lokali_card_text">
							<a href="#">
								<h2 class="lokali_card_heading">Na Vodi kafana</h2>
								<p class="info">Rada Manojlović i Boban Rajović</p>
								<p class="info">od 80 € sa hranom i pićem</p>
							</a>
						</div>
					</div>
				</div>

				<div class="main-content">
					<?php the_content(); ?>
				</div>
			</div>
			@include('partials.sidebar')
		</div>
	</div>
	@include('partials.reservation')
	<div class="container">
		<div class="row">
			@include('partials.featured')
		</div>
	</div>
@endsection
