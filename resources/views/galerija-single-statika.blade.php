{{--
  Template Name: Galerija single
--}}

@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			@include('partials.breadcrumb')
			<div class="col-12 col-lg-8 single_gallery">
				<div class="gallery_hero">
					<p class="gallery_date">08.09.2019</p>
					<h1>Slike iz provoda - Tag</h1>
					<a href="/uncategorized-sr/tag">Tag - poseti stranicu</a>
				</div>

				<div class="gallery">
					<div class="gallery__item">
						<a href="/wp-content/uploads/2019/08/67902016_1657592817711339_8224617352545697792_o.jpg" data-lightbox="roadtrip">
							<img src="/wp-content/uploads/2019/08/67902016_1657592817711339_8224617352545697792_o-225x133.jpg" alt="">
						</a>
					</div>
					<div class="gallery__item">
						<a href="/wp-content/uploads/2019/08/67827648_1657591897711431_5821163691042668544_o.jpg" data-lightbox="roadtrip">
							<img src="/wp-content/uploads/2019/08/67827648_1657591897711431_5821163691042668544_o-225x133.jpg" alt="">
						</a>
					</div>
					<div class="gallery__item">
						<a href="/wp-content/uploads/2019/08/67799289_1657592027711418_8367198644591394816_o.jpg" data-lightbox="roadtrip">
							<img src="/wp-content/uploads/2019/08/67799289_1657592027711418_8367198644591394816_o-225x133.jpg" alt="">
						</a>
					</div>
					<div class="gallery__item">
						<a href="/wp-content/uploads/2019/08/67796641_1657591907711430_1615269811988725760_o.jpg" data-lightbox="roadtrip">
							<img src="/wp-content/uploads/2019/08/67796641_1657591907711430_1615269811988725760_o-225x133.jpg" alt="">
						</a>
					</div>
					<div class="gallery__item">
						<a href="/wp-content/uploads/2019/08/67783544_1657592314378056_1678200261220237312_o.jpg" data-lightbox="roadtrip">
							<img src="/wp-content/uploads/2019/08/67783544_1657592314378056_1678200261220237312_o-225x133.jpg" alt="">
						</a>
					</div>
				</div>
				<nav class="pagination">

					<span aria-current="page" class="page-numbers current">1</span>
					<a class="page-numbers" href="#">2</a>
					<span class="page-numbers dots">…</span>
					<a class="page-numbers" href="#">5</a>
					<a class="next page-numbers" href="#"></a>
				</nav>
				<div class="datepicker custom-mb-big">
					<div class="datepicker_header custom-flex-row">
						<button type="button" class="datepicker_arrow button left"></button>
						<p class="datepicker_month"> Maj 2020. </p>
						<button type="button" class="datepicker_arrow button right"></button>
					</div>
					<div class="datepicker_days custom-flex-row">
						<span>Pon</span>
						<span>Uto</span>
						<span>Sre</span>
						<span>Čet</span>
						<span>Pet</span>
						<span>Sub</span>
						<span>Ned</span>
					</div>
					<div class="datepicker_dates">
						<div class="datepicker_dates_row custom-flex-row">
							<button type="button" class="button datepicker_dates_button" disabled=""></button>
							<button type="button" class="button datepicker_dates_button" disabled=""></button>
							<button type="button" class="button datepicker_dates_button" disabled=""></button>
							<button type="button" class="button datepicker_dates_button" disabled=""></button>
							<button type="button" class="button datepicker_dates_button" disabled=""></button>
							<button type="button" class="button datepicker_dates_button has_result">1.</button>
							<button type="button" class="button datepicker_dates_button has_result">2.</button>
						</div>
						<div class="datepicker_dates_row custom-flex-row">
							<button type="button" class="button datepicker_dates_button has_result">3.</button>
							<button type="button" class="button datepicker_dates_button has_result">4.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">5.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">6.</button>
							<button type="button" class="button datepicker_dates_button active">7.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">8.</button>
							<button type="button" class="button datepicker_dates_button has_result">9.</button>
						</div>
						<div class="datepicker_dates_row custom-flex-row">
							<button type="button" class="button datepicker_dates_button" disabled="">10.</button>
							<button type="button" class="button datepicker_dates_button has_result">11.</button>
							<button type="button" class="button datepicker_dates_button has_result">12.</button>
							<button type="button" class="button datepicker_dates_button has_result">13.</button>
							<button type="button" class="button datepicker_dates_button has_result">14.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">15.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">16.</button>
						</div>
						<div class="datepicker_dates_row custom-flex-row">
							<button type="button" class="button datepicker_dates_button" disabled="">17.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">18.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">19.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">20.</button>
							<button type="button" class="button datepicker_dates_button has_result">21.</button>
							<button type="button" class="button datepicker_dates_button has_result">22.</button>
							<button type="button" class="button datepicker_dates_button has_result">23.</button>
						</div>
						<div class="datepicker_dates_row custom-flex-row">
							<button type="button" class="button datepicker_dates_button has_result">24.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">25.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">26.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">27.</button>
							<button type="button" class="button datepicker_dates_button" disabled="">28.</button>
							<button type="button" class="button datepicker_dates_button has_result">29.</button>
							<button type="button" class="button datepicker_dates_button has_result">30.</button>
						</div>
					</div>
				</div>
			</div>
			@include('partials.sidebar')
		</div>
	</div>
	@include('partials.reservation')
	<div class="container">
		<div class="row">
			@include('partials.featured')
		</div>
	</div>
@endsection
