<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

add_action('wp_enqueue_scripts', function () {
    // Deferred main.css for optimisation
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_localize_script('sage/main.js', 'ajax_object', [
        'ajaxurl' => admin_url('admin-ajax.php'),
    ]);

    if (is_front_page() || is_page_template('views/front-page-statika.blade.php')) {
        wp_dequeue_style('sage/main.css');
        wp_dequeue_script('sage/main.js');
        wp_enqueue_script('front-page', asset_path('scripts/front-page.js'), ['jquery'], null, true);
        $css_path = asset_path('styles/front-page.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_page_template('views/about.blade.php') || is_page()) {
        wp_dequeue_style('sage/main.css');
        //wp_dequeue_script('sage/main.js');
        //wp_enqueue_script( 'about', asset_path( 'scripts/about.js' ), [ 'jquery' ], null, true );

        $css_path_galerija = asset_path('styles/gallery-single.css');
        $css_path = asset_path('styles/about.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <link rel="preload" href="<?php echo $css_path_galerija; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path_galerija; ?>">
        <?php
    }

    if (is_page_template('views/magazin.blade.php') || is_category()) {
        wp_dequeue_style('sage/main.css');
        //wp_dequeue_script('sage/main.js');
        //wp_enqueue_script( 'about', asset_path( 'scripts/about.js' ), [ 'jquery' ], null, true );
        $css_path = asset_path('styles/magazin.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_singular('tekst_pesme')) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/lyrics.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_singular(['striptiz_beograd', 'barovi_beograd', 'restorani_beograd', 'splavovi_beograd', 'klubovi_beograd', 'kafane_beograd'])) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/local-single.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_page_template('views/pricing.blade.php')) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/pricing.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_search()) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/search.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_single('post')) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/single.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_page_template('views/lokali-zbirna.blade.php') || is_archive()) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/local-group.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_home()) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/blog-group.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_page_template('views/app-landing-page.blade.php')) {
        wp_dequeue_style('sage/main.css');
        wp_dequeue_script('sage/main.js');
        wp_enqueue_script('lp', asset_path('scripts/lp.js'), ['jquery'], null, true);
        $css_path = asset_path('styles/lp.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_page_template('views/galerija-grupna.blade.php') || is_page_template('views/galerija-grupna-svi-lokali.blade.php')) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/gallery-group.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_page_template('views/galerija-single-statika.blade.php') || is_singular('galerija')) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/gallery-single.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_page_template('views/nova-godina-single.blade.php')) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/new-year-single.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }

    if (is_page_template('views/nova-godina-zbirna.blade.php')) {
        wp_dequeue_style('sage/main.css');
        $css_path = asset_path('styles/new-year-group.css');
        ?>
        <link rel="preload" href="<?php echo $css_path; ?>" as="style">
        <link rel="stylesheet" href="<?php echo $css_path; ?>">
        <?php
    }
}, 100);

// Soil plugin cleanup for Sage Theme
add_action('after_setup_theme', function () {
    add_theme_support('soil-clean-up');
    add_theme_support('soil-disable-asset-versioning');
    add_theme_support('soil-disable-trackbacks');
    add_theme_support('soil-js-to-footer');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);
    add_theme_support('customize-selective-refresh-widgets');
}, 20);

add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

add_action('after_setup_theme', function () {
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();

        return new Blade($app['view']);
    });

    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});
